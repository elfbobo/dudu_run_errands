// 支付方式切换
$('.duigou').click(function() {
    $('.duigou').removeClass('duigou-active');
    $(this).addClass('duigou-active');
});


function showPay() {
     var payType = $(".payType:checked").val();
     if (payType == null || payType == '' || payType == 'undefined') {
        layer.msg('请选择付款方式');
         return;
     }
     //余额支付
    if(payType==3){
         $(".ftc_wzsf").show();
    }else if(payType==1){
        layer.msg('支付宝支付正在建设中...');
        return;
    }else if(payType==2){
        callpay();
    }
}
$(function() {
    $('.wxzf_price').text('￥{$needPay}');

    //出现浮动层
    $('.blank_yh').on('click', function() {
        $(".ftc_wzsf").hide();
    });

    $(".ljzf_but").on('click', function() {
        $(".ftc_wzsf").show();
    });
    //关闭浮动
    $(".close").on('click', function() {
        $(".ftc_wzsf").hide();
    });
    //数字显示隐藏
    $(".xiaq_tb").on('click', function() {
        $(".numb_box").slideUp(500);
    });
    $(".mm_box").on('click', function() {
        $(".numb_box").slideDown(500);
    });
    //----
    var i = 0;
    var payPwd = '';
    $(".nub_ggg li a").on('click', function() {
        var payStatus = $('#payStatus').val();
        //判断已经已经是6位密码
        if (payStatus == 1) {
            layer.msg('正在处理中');
            return;
        }
        i++;
        if (i < 6) {
            payPwd = payPwd + $(this).text();
            $(".mm_box li").eq(i - 1).addClass("mmdd");
        } else {
            $('#payStatus').val(1);
            payPwd = payPwd + $(this).text();
            $(".mm_box li").eq(i - 1).addClass("mmdd");
            if (payPwd == '' || payPwd == null || payPwd.length != 6) {
                //alert(payPwd);
                return;
            }
            layer.msg('正在付款...',{icon:16});
            var orderids=$('#orderids').val();
           $.ajax({
               type: "POST",
               url: "/Wx/Confirm/balancePay",
               data: {pwd:payPwd,orderids:orderids},
               dataType: "json",
               success: function(data){
                   layer.closeAll();
                   if(data.status==0){
                       layer.msg(data.msg);
                       setTimeout(function(){
                           location.href="/Wx/Orders/orders";
                       },1000);
                   }else if(data.status==-1||data.status==-3||data.status==-2){
                       $(".mm_box li").removeClass("mmdd");
                       i=0;
                       payPwd='';
                       $('#payStatus').val(0);
                       layer.msg(data.msg);
                       layer.msg(data.msg);
                       return;
                   }else if(data.status==-4){
                       layer.msg(data.msg);
                       setTimeout(function(){
                           location.href="/Wx/Orders/orders";
                       },1000);
                   }
                }
           });

            //密码错误处理、/*
            //
            // $(".mm_box li").removeClass("mmdd");
            // $('#payStatus').val(0);
            // payPwd = '';
            // i = 0;
            //*/
        }
    });

    $(".nub_ggg li .del").on('click', function() {
        if (i > 0) {
            i--;
            $(".mm_box li").eq(i).removeClass("mmdd");
            payPwd = payPwd.substring(0, payPwd.length - 1);
            i == 0;
            $('#payStatus').val(0);
        }
    });

});

//设置支付密码
function setpayPwd(){
        var paypwd=$('#payPwd').val();
        var paypwds=$('#payPwds').val();
        if(paypwd.length!=6||paypwds.length!=6){
            layer.msg('密码长度不对');
            return;
        }
        if(paypwd!=paypwds){
            layer.msg('重复密码不正确');
            return;
        }
    var re = /^\d+$/;


    if(!re.test(paypwd)){
        layer.msg('密码只能是数字');
        return;
    }

    if(isNaN(paypwd)||isNaN(paypwds)){
            layer.msg('密码只能是6位数字');
            return;
        }
        layer.msg('正在保存...',{icon:16});
           $.ajax({
               type: "POST",
               url: "/Wx/Confirm/setPayPwd",
               data: {pwd:paypwds},
               dataType: "json",
               success: function(data){
                   layer.closeAll();
                        if(data.status==0){
                            layer.msg(data.msg);
                            $('#setpayPwd').hide();
                            $('#pay').show();
                        }else if(data.status==-1||data.status==-2){
                            layer.msg(data.msg);
                        }
                }
           });
}
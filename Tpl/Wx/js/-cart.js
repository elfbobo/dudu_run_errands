/*
* @Author: liTengZhen
* @Date:   2017-06-30 11:43:51
*/
var BuyCart={

    key : 'o2oShop'+$('#uId').val(),
    sId : $('#sId').val(),


    /**
     * 初始化结算价格、购物车数量
     */
    initCart: function(){
        var car = localStorage.getItem(this.key);

        var init={
                'price' : 0,
                'number': 0
        };
        //console.log(car);
        //localStorage.removeItem(this.key);
        if( car ){
          car = JSON.parse(car);
          var type = typeof(car[this.sId]);
          if( type != "undefined" ){
            car = car[this.sId];
            for(var i in car){
                init['price']+=car[i]['goodsPri']*car[i]['goodsNum'];
                init['number']+=parseInt(car[i]['goodsNum']);
            }
          }
        }

        //console.log(init);
        return init;
    },


    /**
     * 购物车商品
     * @return 购物车商品
     */
    cartInfo: function(){
        var car = localStorage.getItem(this.key);

        if(car){
          car = JSON.parse(car);
          var type = typeof(car[this.sId]);
          if( type != "undefined"){
            car = car[this.sId];
          }else{
            car = false;
          }
        }
        return car;
    },


    /**
     * 添加到购物车
     * @param obj obj [description]
     */
    addToCart: function(obj){
      var info         = $(obj).data('info');
      var gId          = info['goodsId'];

      //info['goodsNum'] = $(obj).val();
      var data = {
        'goodsId' : gId,
        'gcount'  : $(obj).val()
      };
      this.sendAddToCart(data);


      //localStorage.removeItem(this.key)
      //已存在购物车
      // if(localStorage.getItem(this.key)){
      //   var cart = JSON.parse(localStorage.getItem(this.key));
      //   //不存在商铺购物车需创建
      //   if (typeof(cart[this.sId]) == "undefined") {
      //     cart[this.sId]={};
      //   }

      // }else{
      //   var cart  = {}; //用户购物车
      //   cart[this.sId] = {}; //该商店购物车
      // }
      // cart[this.sId][gId] = info; //该商店商品信息
      // localStorage.setItem(this.key,JSON.stringify(cart));
    },

    /**
     * 商品详情添加到购物车
     * @param obj obj [description]
     */
    addToCart1: function(obj,num){

      var info         = $(obj).data('info');
      var gId          = info['goodsId'];

      //info['goodsNum'] = num;
      var data = {
        'goodsId' : gId,
        'gcount'  : num
      };
      this.sendAddToCart(data);
      // localStorage.removeItem(this.key)
      // //已存在购物车
      // if(localStorage.getItem(this.key)){
      //   var cart = JSON.parse(localStorage.getItem(this.key));
      //   //不存在商铺购物车需创建
      //   if (typeof(cart[this.sId]) == "undefined") {
      //     cart[this.sId]={};
      //   }

      // }else{
      //   var cart  = {}; //用户购物车
      //   cart[this.sId] = {}; //该商店购物车
      // }
      // cart[this.sId][gId] = info; //该商店商品信息

      // localStorage.setItem(this.key,JSON.stringify(cart));

    },



    delFromCart: function(obj){
      var info         = $(obj).data('info');
      var gId          = info['goodsId'];
      var num = $(obj).val();
      if(num==0){
        var data={
          ids : gId+'_'+(info['id'] || 0)+'_'+0+'_'+0;
        };
        this.sendDelCart(data);
      }else{
        var data={
          goodsId : gId,
          num     : num
        };
        this.changeCartGoodsNum(data);
      }
      // var cart = JSON.parse(localStorage.getItem(this.key));
      // //为空需删除
      // if($(obj).val() == ''){
      //   delete  cart[this.sId][gId];
      //   if(this.isEmpty(cart[this.sId])){
      //       delete cart[this.sId];
      //   }
      // }
      // localStorage.setItem(this.key,JSON.stringify(cart));

    },

    /**
     * 添加购物车商品请求
     */
    sendAddToCart: function(data){
      $.post('/Wx/Cart/addCart', data, function(data){
          if(data['status']<0){
            mui.toast(data['msg']);
            return false;
          }
          mui.toast('成功添加');
        })
    }

    /**
     * 删除购物车商品请求
     */
    sendDelCart: function(data){
      $.post('/Wx/Orders/delCartGoods', data, function(data){
          if(data['status']==-1){
            mui.toast('操作失败');
            return false;
          }
          mui.toast('成功删除该商品');
        })
    }

    /**
     * 修改购物车商品数量请求
     */
    changeCartGoodsNum: function(data){
      $.post('/Wx/Cart/changeCartGoodsNum', data, function(data){
          if(data['status']==-1){
            mui.toast('操作失败');
            return false;
          }
          //mui.toast('成功删除该商品');
        })
    }

    /**
     * 检测对象是否为空
     * @param  {[type]}  obj [description]
     * @return Boolean
     */
    isEmpty: function(obj){
        for(var i in obj){
            return false;
        }
        return true;
    }
}



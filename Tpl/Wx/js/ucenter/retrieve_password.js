/**
 * 短信找回密码js
 **/

// 验证手机号正则
function checkPhone() {
    var phone = document.getElementById('phone').value;
    if (!(/^1(3|4|5|7|8)\d{9}$/.test(phone))) {
        mui.toast('手机号码有误',{ duration:'long', type:'div' });
        return false;
    }
}

// 按钮禁用功能
function enable(){
    var phone = document.getElementById('phone').value;
    if(phone!=0) {
       document.getElementsByTagName('button')[0].disabled = false;
    }else if(phone==0){
       document.getElementsByTagName('button')[0].disabled = true; 
    }
}




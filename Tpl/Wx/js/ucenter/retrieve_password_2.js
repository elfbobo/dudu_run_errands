/**
 * 短信找回密码js
 **/


// 验证手机号正则
function checkPhone() {
    var code = document.getElementById('secCode').value;
    if (code!='123456') {
        mui.toast('验证码错误',{ duration:'long', type:'div' });
        return false;
    }
}

// 按钮禁用功能
function enable(){
    var code = document.getElementById('secCode').value;
    if(code!=0) {
       document.getElementsByTagName('button')[0].disabled = false;
    }else if(code==0){
       document.getElementsByTagName('button')[0].disabled = true;
    }
}


function countTime(){
    var time = document.getElementById('time').innerHTML;
        time--;
    var countDown=setInterval(function(){
        document.getElementById('time').innerHTML = time;
        if(time<0){
            clearInterval(countDown);
            document.getElementsByClassName('get_num')[0].style.background = '#fe8141';
            document.getElementsByClassName('get_num')[0].innerHTML = '重新获取';
        }
    },1000)
}


// 验证码倒计时
var second=120;
function settime(val) {
    if(val==0){
        second=0;
        return;
    }
    var code=document.getElementById('yanzhenma');
    if (second == 0) {
        document.getElementById('sendStatus').value=0;
        code.innerHTML='重新获取';
        document.getElementById('yanzhenma').classList.add('able');
        second = 60;
        return;
    } else {
        document.getElementById('sendStatus').value=1;
        code.innerHTML="重新发送(" + second + ")";
        second--;
    }
    setTimeout(function() {
        settime(val)
    }, 1000)
}

//获取验证码
function getCode() {
    if (document.getElementById('sendStatus').value == 1) {
        mui.toast('请求过于频繁',{ duration:'long', type:'div' });
        return;
    }
    var phone = document.getElementById('phone').value;
    var telReg = !!phone.match(/^1[3|7|4|5|8]{1}\d{9}$/);
    if (telReg == false) {
        mui.toast('手机格式不正确',{ duration:'long', type:'div' });
        return;
    }
    document.getElementById('yanzhenma').classList.remove('able');
    settime();
    mui.post('/Wx/Login/getCode',{
            phone : phone,
            type : 2
        },function(data){
            if (data.status == -2) {
                settime(0);
                mui.toast('操作过于频繁',{ duration:'long', type:'div' });
                return;
            }else if(data.status==-5){
                mui.toast('手机号不存在',{ duration:'long', type:'div' });
                return;
            }
            var reg = /^(\d{3})\d{4}(\d{4})$/;
            document.getElementById('userPhone').innerText = phone.replace(reg, "$1****$2");
            document.getElementById('phoneMsg').style.display = 'block';
        },'json'
    );
}

function checkForm() {
    var isrepeat=document.getElementById('isrepeat');
    var phone=document.getElementById('phone').value;
    var secCode=document.getElementById('secCode').value;
    var pwd=document.getElementById('pwd').value;
    var telReg = !!phone.match(/^1[3|7|4|5|8]{1}\d{9}$/);
    if(isrepeat.value==1){
        mui.toast('请勿重复提交',{ duration:'long', type:'div' });
        return;
    }
    if (telReg == false) {
        mui.toast('手机格式不正确',{ duration:'long', type:'div' });
        return;
    }
    if (secCode.length != 6) {
        mui.toast('请输入6位验证码',{ duration:'long', type:'div' });
        return;
    }
    if (pwd.length < 6) {
        mui.toast('请输入6位以上密码',{ duration:'long', type:'div' });
        return;
    }
    isrepeat.value=1;
    mui.post('/Wx/Login/resetLoginPwdHandle',{
        phone : phone,
        pwd : pwd,
        code:secCode
    },function(data){
        isrepeat.value=0;
            switch(data.status){
                case -1:
                    isrepeat.value=0;
                    mui.toast('手机号和密码必填',{ duration:'long', type:'div' });
                    break;
                case -3:
                    isrepeat.value=0;
                    mui.toast('验证码错误',{ duration:'long', type:'div' });
                    break;
                case -2:
                    isrepeat.value=0;
                    mui.toast('用户不存在',{ duration:'long', type:'div' });
                    break;
                case -4:
                    isrepeat.value=1;
                    mui.toast('修改失败',{ duration:'long', type:'div' });
                    break;
                case 0:
                    isrepeat.value=1;
                    mui.toast('修改成功',{ duration:'long', type:'div' });
                    setTimeout(function(){
                        location.href="/Wx/Index/index/r/my";
                    },1000)
                }
        },'json'
    );
            //console.log(data);
}


function checkFormPay() {
    var isrepeat=document.getElementById('isrepeat');
    var phone=document.getElementById('phone').value;
    var secCode=document.getElementById('secCode').value;
    var pwd=document.getElementById('pwd').value;
    var telReg = !!phone.match(/^1[3|7|4|5|8]{1}\d{9}$/);
    if(isrepeat.value==1){
        mui.toast('请勿重复提交',{ duration:'long', type:'div' });
        return;
    }
    if (telReg == false) {
        mui.toast('手机格式不正确',{ duration:'long', type:'div' });
        return;
    }
    if (secCode.length != 6) {
        mui.toast('请输入6位验证码',{ duration:'long', type:'div' });
        return;
    }
    if (pwd.length < 6) {
        mui.toast('请输入6位以上密码',{ duration:'long', type:'div' });
        return;
    }
    isrepeat.value=1;
    mui.post('/Wx/Login/resetPayPwdHandle',{
            phone : phone,
            pwd : pwd,
            code:secCode
        },function(data){
        isrepeat.value=0;
            switch(data.status){
                case -1:
                    isrepeat.value=0;
                    mui.toast('手机号和密码必填',{ duration:'long', type:'div' });
                    break;
                case -3:
                    isrepeat.value=0;
                    mui.toast('验证码错误',{ duration:'long', type:'div' });
                    break;
                case -2:
                    isrepeat.value=0;
                    mui.toast('用户不存在',{ duration:'long', type:'div' });
                    break;
                case -4:
                    isrepeat.value=1;
                    mui.toast('修改失败',{ duration:'long', type:'div' });
                    break;
                case 0:
                    isrepeat.value=1;
                    mui.toast('修改成功',{ duration:'long', type:'div' });
                    setTimeout(function(){
                        location.href="/Wx/Index/index/r/my";
                    },1000)
            }
        },'json'
    );
    //console.log(data);
}

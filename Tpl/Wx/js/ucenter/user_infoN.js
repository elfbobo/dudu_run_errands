document.getElementById('photoUpload').onclick = function() {
  mui('#sheet1').popover('toggle');
}

var $upFile = $('#photo_upload');
//调用手机摄像头并拍照
function getImage() {
  $upFile.attr({'accept':'image/*','capture':'camera'});
  $upFile.click();
}

//从相册选择照片
function galleryImgs() {
  $upFile.attr({'accept':'image/*'})
  $upFile.removeAttr('capture');
  $upFile.click();
}

function confirm() {
  mui.confirm('确定退出当前账户', '提示', ['取消', '确定'], function(data) {
    if (data['index'] == 1) loginOut();
  })
}

function loginOut() {
  mui.post('/Wx/Login/logOut', {}, function(data) {
    if (data.status == 0) {
      mui.toast('成功退出', {
        duration: 'long',
        type: 'div'
      });
      setTimeout(function() {
        location.href = "/Wx/Index/Index"
      });
    }
  }, 'json')
}

$('#photo_upload').change(function(){
   var data = new FormData();

   data.append('file', $('#photo_upload')[0].files[0]);
  var index = layer.msg('上传中...', {icon: 16,scrollbar: false, time:100000}) ;
     $.ajax({
      url:'/Wx/Login/uploadImg',
      data:data,
      contentType: false,
      processData: false,
      dataType:'json',
      type:'POST',
      success:function(data){
        layer.close(index);
        mui.toast(data['msg'], {duration: 'long',type: 'div'});
        if(data['status']==1){
          var img ='/'+data['path'];
          $('#userPhoto').css('background-image','url('+img+')');
        }
        mui('#sheet1').popover('toggle');
      }
     })

})

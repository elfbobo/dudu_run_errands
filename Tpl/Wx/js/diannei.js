


/**
 * 获取分类商品
 * @param int id 商品分类id
 * @param obj obj 分类商品容器
 */
function getCateGoods(id,obj){

  var data={
    get_num: 1,
    yiji_id: id,
    shop_id: $('#main').data('shopid')
  };
  $.getJSON('/Wx/Shop/getShopGoods', data, function(data){
    var html='';
    if(!data) obj.html('暂无商品');
    $.each(data,function(i,n){
      html+='  <div class="main-goods-section-r-lb">';
      html+='      <div class="main-goods-section-r-tp" onclick="location.href=\'/Wx/Goods/index/goodsId/'+n['goodsId']+'\'" style="background-image:url(/'+n['goodsThums']+')"></div>';
      html+='      <div class="main-goods-section-r-ypz">';
      html+='        <div class="yichu">'+n['goodsName']+'</div>';
      html+='        <div class="ysj">月售'+n['yue_xiaoliang']+'&nbsp;评价'+n['goods_appraises']+'</div>';
      html+='        <div class="dj">¥'+n['shopPrice']+'</div>';
      html+='      </div>';
      html+='      <div class="sfq">';
      html+='          <div class="jian">-</div>';
      html+='          <input class="v-jg" type="number" max="'+n['goodsStock']+'" readonly unselectable="on" value="" />';
      html+='          <div class="jia">+</div>';
      html+='      </div>';
      html+='  </div>';
    })
    obj.html(html);
  })
}


$('#search').on('click',function(){
  if( !$.trim( $('#serachValue').val() ) ){
    mui.toast('请输入搜索关键词');
  }else{
    $('#form').submit();
  }
})

//点击加号
$(document).on('click', '.jia', function(){
    var v = $(this).parent().find('.v-jg').val();
    $(this).parent().find('.v-jg').val(++v);
    if(v >= 1){
       $(this).parent().find('.jian').show();
    }
    var $num  = $(this).parent().find('.v-jg');
    var v = $num.val();
    if(v*1+1>$(this).attr('max')){
      mui.toast('库存不足');
      return;
    }

    BuyCart.addToCart($num[0],2);
    //fadd_cart($(this));
})

// $('.jia').click(function() {
// });
//点击减号
$(document).on('click', '.jian', function(){
    var $input = $(this).parent().find('.v-jg');
    var v = $input.val();
    $(this).parent().find('.v-jg').val(--v);

    if(v < 1){
        $(this).parent().find('.jian').hide();
        $(this).parent().find('.v-jg').val('');

    }

    BuyCart.delFromCart($input[0],2);


   var txt =  $('.gwc-circle').text();
   $('.gwc-circle').text(--txt);
    var txt2 =  $(this).parent().parent().find('.dj').text();

    txt2 = parseFloat(txt2.substr(1,txt2.length-1));
    var jg = $('.js-jg').text();
    jg =  parseFloat(jg.substr(1,jg.length-1));
    var sb = Math.round((jg-txt2)*100)/100;
    $('.js-jg').text('¥'+sb);
    if(sb==0){
      $('.js-jg').text('¥'+sb+'.00');
    }
})
// $('.jian').click(function() {

// });

function fadd_cart(obj){
    fadd_move(obj, $('.gwc-circle'), fcallback)
}

var i = 0, dian_y, dian_x, end_y, end_x, g, x_v, y_v, t;
function fadd_move(start_obj, end_obj, callback) {
    i++;
    // 起始位置
    dian_x = start_obj.offset().left + start_obj.width()/2 - 7;
    dian_y = start_obj.offset().top + start_obj.height()/2 - 7;
    // 终点位置
    end_x = end_obj.offset().left;
    end_y = end_obj.offset().top;
    // 速度 时间 加速度
    x_v = -11;
    y_v = 0;
    t = (end_x - dian_x)/x_v;
    g = 2*(end_y - dian_y)/t/t;
    // console.log(dian_y+','+dian_x+','+g)
    // 创建点
    var html_ = "<div class='dian dian"+i+"'></div>";
    $('body').append(html_);
    $('.dian'+i+'').css({
      'left': dian_x,
      'top': dian_y
    });
    fto_move(start_obj, $('.dian'+i+''), dian_y, dian_x, end_x, end_y, x_v, y_v, g, callback)
}

function fto_move(wlg, obj, m_dian_y, m_dian_x, m_end_x, m_end_y, m_x_v, m_y_v, m_g, callback){

    var m_time = setInterval(function(){
      m_dian_x += m_x_v;
      m_dian_y += m_y_v;
      m_y_v += m_g;

      if(m_dian_y>=m_end_y){
        obj.css({
          'left': m_end_x,
          'top': m_end_y
        });
        callback(wlg);
        obj.fadeOut('fast',function(){
          obj.remove();
        });
        clearInterval(m_time);
      }
      else{
        obj.css({
          'left': m_dian_x,
          'top': m_dian_y
        });
      }
    },20)
}
function fcallback(obj){

    var txt =  $('.gwc-circle').text();
    $('.gwc-circle').text(++txt);
    var txt2 =  obj.parent().parent().find('.dj').text();
    txt2 = parseFloat(txt2.substr(1,txt2.length-1));
    var jg = $('.js-jg').text();
    jg =  parseFloat(jg.substr(1,jg.length-1));
    var sb = Math.round((jg+txt2)*100)/100;
    $('.js-jg').text('¥'+sb);
}

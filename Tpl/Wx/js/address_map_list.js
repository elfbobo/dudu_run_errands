    var point = new BMap.Point(localStorage.mllng, localStorage.mllat); //定义一个中心点坐标  

    $(function() {
        getlocation();
    })

    function getlocation() {
        var geoc = new BMap.Geocoder();
        showload();
        geoc.getLocation(point, function(rs) {
            makelist(rs['surroundingPois']);
        });
    }

    $(".topbar-title").find("input").keyup(function() {
        var v = $(this).val();
        if (v == '') {
            getlocation();
        }
    });

    function search_key() {
        var msg = $(".topbar-title").find("input").val();
        if (msg == '') {
            showmsg("请填写搜索词");
            return;
        }
        showload();
        var map = new BMap.Map("map");
        map.centerAndZoom(point, 15);
        var options = {
            onSearchComplete: function(results) {
                // 判断状态是否正确
                if (local.getStatus() == BMAP_STATUS_SUCCESS) {
                    var s = [];
                    for (var i = 0; i < results.getCurrentNumPois(); i++) {
                        s.push(results.getPoi(i));
                    }
                }
                makelist(s);
            }
        };
        var local = new BMap.LocalSearch(map, options);
        local.search(msg);
    }

    function makelist(data) {
        console.log(data);
        if (data && data.length > 0) {
            var html = "";
            for (var k in data) {
                var t = "";
                if (data[k]['type'] == 0) {
                    html += '<li class="mui-table-view-cell mui-media" data-lng="' + data[k]['point']['lng'] + '" data-lat="' + data[k]['point']['lat'] + '"><div class="mui-media-body"><span>' + data[k]['title'] + '</span><p class="mui-ellipsis">' + data[k]['address'] + '</p></div></li>';
                }
            }
            if (html == "") { showmsg("搜索不到内容"); }
            $(".mui-table-view").html(html);
            killload();
        } else {
            killload();
            showmsg("搜索不到内容");
        }
    }

    var isload = 0;

    function showload() {
        mui.toast("数据加载中...", { duration: 999999, type: 'div' });
        $("html").css("pointer-events", "none");
        isload = 1;
        setTimeout("killload();", 30000);
    }

    function killload(type) {
        if (isload == 0) { return; }
        $(".mui-toast-container").remove();
        $("html").css("pointer-events", "auto");
        isload = 0;
    }

    function showmsg(msg) {
        mui.toast(msg, { duration: 2500, type: 'div' });
    }

    $(document).on("click", ".mui-table-view-cell", function() {
        var lng = $(this).attr("data-lng");
        var lat = $(this).attr("data-lat");
        var address = $(this).find("span").text();
        var number = $(this).find("p").text();
        localStorage.address = address;
        localStorage.number = number;
        localStorage.lat = lat;
        localStorage.lng = lng;
        window.location.href = localStorage.ret;
    });
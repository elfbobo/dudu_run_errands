var starttime = new Date(2017, 7, 1, 18, 0, 0);
  setInterval(function () {
    var nowtime = new Date();  
    var time = starttime - nowtime;
    // alert(time);
    // var day = parseInt(time / 1000 / 60 / 60 / 24);
    var hour = parseInt(time / 1000 / 60 / 60 % 24);
    hour = hour>9?hour:'0'+hour;
    var minute = parseInt(time / 1000 / 60 % 60);
    minute = minute>9?minute:'0'+minute;
    var seconds = parseInt(time / 1000 % 60);
    seconds = seconds>9?seconds:'0'+seconds;
    $('.timespan').html("本场还剩 " + hour + " : " + minute + " : " + seconds );
  }, 1000);

var w = $('.top-tab ul li').outerWidth(true);
var l = $('.top-tab ul li').length;
$('.top-tab ul').width(w*l);

$('.top-tab ul li').click(function() {
    $(this).addClass('top-tab-active').siblings().removeClass('top-tab-active');
    $('.top-tab-icon').removeClass('top-tab-icon-active');
    $('.top-tab-icon').eq($(this).index()).addClass('top-tab-icon-active');
    $('.f-muii-content').eq($(this).index()).show().siblings().hide();
});
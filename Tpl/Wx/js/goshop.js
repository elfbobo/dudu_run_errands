

// var car  = BuyCart.cartInfo();
// var init = BuyCart.initCart();
// $('.gwc-circle').html(init['number']);
// $('.js-jg').html('¥'+init['price']);
$('.main-goods-section-cnt').eq(0).addClass('cnt-show');

$(function(){
  $('.main-goods-section-l ul li').eq(0).click();
  setScoreImg($('#zhpf')[0]);
  setScoreImg($('#pspf')[0]);
  $('.pflb').each(function(i,n){
    setScoreImg(n);
  })
})


function shopClose(){
  mui.toast('本店已打烊');
}
/**
 * 点击遮罩层隐藏购物车
 */
 $('.zhezhao').click(function(){
    $('.gwc-tan').stop().slideToggle();
    $('.zhezhao').removeClass('zhezhao-active');
 })

$('#delGoods').click(function(){
  mui.confirm('确定清空购物车吗?', '清空购物车', ['否','是'], function(e){
    if(e.index==1){
      BuyCart.destroyCart();
      //location.reload();
    }
  })
})

/**
 * 跳转到登录
 */
function toLogin(){
  mui.toast('请先登录');
  location.href='/Wx/Login/login';
}


/**
 * 收藏/取消收藏
 */
function favorites(){
  //已收藏
  if($('#collection').hasClass('top-right-follow-sel')){
    sendFavorites(-1);
  }else{
    sendFavorites(1);
  }
}


/**
 * 结算
 * @param flaot money 起送价
 */
function goShop(money){
  var jg = $('.js-jg').text();
  jg =  parseFloat(jg.substr(1,jg.length-1));
  var sb = Math.round((jg)*100)/100;
  if(sb == 0){
    mui.toast('购物车中没有商品');
    return false;
  }
  if(money>sb){
    mui.toast('还差'+(money-sb)+'元到达起送价');
    }else{
    location.href='/Wx/Confirm/conFirmOrder/shopId/'+$('#sId').val();
  }
}

/**
 * 发送收藏请求
 */
function sendFavorites(flag){
  var $obj=$('#collection');
  var data={
    'id': $obj.data('id'),
    'flag': flag
  };
  $.post('/Wx/Favorites/favorites', data, function(data){
      if(data==-2){
        mui.toast('请先登录');
        return;
      }
      if(data==-1){
        mui.toast('操作失败');
        return;
      }
      if(flag==1){
        mui.toast('成功收藏');
        $obj.addClass('top-right-follow-sel');
        return;
      }else{
        mui.toast('已取消收藏');
        $obj.removeClass('top-right-follow-sel');
        return;
      }
    })
}




/**
 * 服务星级评分
 */
function setScoreImg(obj){
  var temp = $(obj).data('score');
  var score = Math.floor(temp);
  var html='';
  for(var i=1;i<=5;i++){
      if(i<=score){
          html+='<img src="/Tpl/Wx/image/icon_star_1@2x.png" width="100%" height="100%">';
      }else if(temp*1+0.5>=i){
          html+='<img src="/Tpl/Wx/image/icon_star_05@2x.png" width="100%" height="100%">';
      }else{
          html+='<img src="/Tpl/Wx/image/icon_star_0@2x.png" width="100%" height="100%">';

      }
  }
  $(obj).find('.setStart').html(html);
}



/**
 * 获取分类商品
 * @param int id 商品分类id
 * @param obj obj 分类商品容器
 */
function getCateGoods(id,obj){

  var data={
    get_num: 1,
    yiji_id: id,
    shop_id: $('#main').data('shopid')
  };

  $.getJSON('/Wx/Shop/getShopGoods', data, function(data){
    var html='';
    if(!data) obj.html('暂无商品');
    $.each(data,function(i,n){
      html+='  <div class="main-goods-section-r-lb">';
      html+='      <div class="main-goods-section-r-tp" onclick="location.href=\'/Wx/Goods/index?goodsId='+n['goodsId']+'&ref=/Wx/Shop/shop/id/'+$('#sId').val()+'\'" style="background-image:url(/'+n['goodsThums']+')"></div>';
      html+='      <div class="main-goods-section-r-ypz">';
      html+='        <div class="yichu">'+n['goodsName']+'</div>';
      html+='        <div class="ysj">月售'+n['yue_xiaoliang']+'&nbsp;评价'+n['goods_appraises']+'</div>';
      html+='        <div class="dj">¥'+n['shopPrice']+'</div>';
      html+='      </div>';
      if(n['is_shuxing']==1){
        html+='      <a class="guige" href="/Wx/Goods/index?goodsId='+n['goodsId']+'&ref=/Wx/Shop/shop/id/'+$('#sId').val()+'">选规格</a>';
      }else{

         var info = {
           'goodsId'  : n['goodsId'],
        //   'goodsName': n['goodsName'],
        //   'goodsPic' : '/'+n['goodsThums'],
        //   'goodsPri' : n['shopPrice']
         };
        //数量判断
        var number = '';
        var reduce = '';
        //info = JSON.stringify(info);
        if(car  && typeof(car[n['goodsId']])!='undefined'){
          number   = car[n['goodsId']]['cnt'];
          reduce   = 'style="display:block"';
        }

        html+='      <div class="sfq">';
        html+='          <div class="jian" '+reduce+'>-</div>';
        html+='          <input class="v-jg" readonly unselectable="on" disabled data-info=\''+JSON.stringify(info)+'\'  type="number"  max="'+n['goodsStock']+'" value="'+number+'" />';

        html+='          <div class="jia" >+</div>';

        html+='      </div>';

      }
      html+='  </div>';
    })
    obj.html(html);
  })
}

$('.main-comment-section-tab-top div').click(function() {
    $(this).addClass('main-comment-section-tab-top-active').siblings().removeClass('main-comment-section-tab-top-active');

    if($(this).hasClass('manyi')){
      $('.satisfied').show();
      $('.unSatisfied').hide();
    }else if($(this).hasClass('bumanyi')){
      $('.satisfied').hide();
      $('.unSatisfied').show();
    }else{
      $('.satisfied').show();
      $('.unSatisfied').show();
    }
    //$('.hys-pflb').eq($(this).index()).show().siblings().hide();
});



$('.main-tab-box div').click(function() {

    $(this).addClass('main-tab-sel').siblings().removeClass('main-tab-sel');
    $('.main-section').eq($(this).index()).show().siblings().hide();

});
/**
 * [description]
 * @param  {[type]} ) {             $('.gwc-tan').stop().slideToggle();  $('.zhezhao').toggleClass('zhezhao-active');} [description]
 * @return {[type]}   [description]
 */
$('.gwc-dj').click(function() {
  $('.gwc-tan').stop().slideToggle();
  $('.zhezhao').toggleClass('zhezhao-active');
});

//点击分类
$('.main-goods-section-l ul li').click(function() {
    var index=$(this).index();
    $(this).addClass('main-goods-section-l-active').siblings().removeClass('main-goods-section-l-active');

    $('.main-goods-section-cnt').removeClass('cnt-show');
    $('.main-goods-section-cnt').eq($(this).index()).addClass('cnt-show');

    //$('.main-goods-section-cnt').eq($(this).index()).show().siblings().hide();


    //已有内容不加载
    var $cnt = $('.main-goods-section-cnt').eq(index);
    if( !$.trim( $cnt.html() ) ){
      getCateGoods($(this).data('id'),$cnt);
    }
});

//点击商品列表中的加号
$('.main-goods-section-cnt').on('click', '.jia', function(){


   if($('#isLogin').length<1){

       mui.toast('请先登录');
       return;
    }
    var $num  = $(this).parent().find('.v-jg');
    var v =$num.val();
    if(v*1+1>$(this).attr('max')){
      mui.toast('库存不足');
      return;
    }

    $(this).parent().find('.v-jg').val(++v);
    BuyCart.addToCart($num[0],2);


    if(v >= 1){
       $(this).parent().find('.jian').show();
    }
    fadd_cart($(this));
})

//点击购物车中的加号
$('#carGoods').on('click', '.jia', function(){

    if($('#isLogin').length<1){
       mui.toast('请先登录');
       return;
    }
    var $num  = $(this).parent().find('.v-jg');
    var v = $num.val();
    if(v*1+1>$(this).attr('max')){
      mui.toast('库存不足');
      return;
    }

    $(this).parent().find('.v-jg').val(++v);
    BuyCart.addToCart($num[0],1);


    if(v >= 1){
       $(this).parent().find('.jian').show();
    }
    fadd_cart($(this));
})



//点击商品列表中的减号
$('.main-goods-section-cnt').on('click', '.jian', function(){
    var $input = $(this).parent().find('.v-jg');
    var v = $input.val();
    $(this).parent().find('.v-jg').val(--v);

    if(v < 1){
        $(this).parent().find('.jian').hide();
        $(this).parent().find('.v-jg').val('');

    }

    BuyCart.delFromCart($input[0],2);


   var txt =  $('.gwc-circle').text();
   $('.gwc-circle').text(--txt);
    var txt2 =  $(this).parent().parent().find('.dj').text();

    txt2 = parseFloat(txt2.substr(1,txt2.length-1));
    var jg = $('.js-jg').text();
    jg =  parseFloat(jg.substr(1,jg.length-1));
    var sb = Math.round((jg-txt2)*100)/100;
    $('.js-jg').text('¥'+sb);
    if(sb==0){
      $('.js-jg').text('¥'+sb+'.00');
    }
})


//点击购物车中的减号
$('#carGoods').on('click', '.jian', function(){
    var $input = $(this).parent().find('.v-jg');
    var v = $input.val();
    $(this).parent().find('.v-jg').val(--v);

    if(v < 1){
        $(this).parent().find('.jian').hide();
        $(this).parent().find('.v-jg').val('');
    }

    BuyCart.delFromCart($input[0],1);


   var txt =  $('.gwc-circle').text();
   $('.gwc-circle').text(--txt);
    var txt2 =  $(this).parent().parent().find('.dj').text();
    console.log($(this).parent().parent().find('.dj'));
    txt2 = parseFloat(txt2.substr(1,txt2.length-1));

    console.log(txt2);
    var jg = $('.js-jg').text();
    jg =  parseFloat(jg.substr(1,jg.length-1));
    var sb = Math.round((jg-txt2)*100)/100;
    $('.js-jg').text('¥'+sb);
    if(sb==0){
      $('.js-jg').text('¥'+sb+'.00');
    }
})
// $('.jian').click(function() {

// });

function fadd_cart(obj){
    fadd_move(obj, $('.gwc-circle'), fcallback)
}

var i = 0, dian_y, dian_x, end_y, end_x, g, x_v, y_v, t;
function fadd_move(start_obj, end_obj, callback) {
    i++;
    // 起始位置
    dian_x = start_obj.offset().left + start_obj.width()/2 - 7;
    dian_y = start_obj.offset().top + start_obj.height()/2 - 7;
    // 终点位置
    end_x = end_obj.offset().left;
    end_y = end_obj.offset().top;
    // 速度 时间 加速度
    x_v = -11;
    y_v = 0;
    t = (end_x - dian_x)/x_v;
    g = 2*(end_y - dian_y)/t/t;
    // console.log(dian_y+','+dian_x+','+g)
    // 创建点
    var html_ = "<div class='dian dian"+i+"'></div>";
    $('body').append(html_);
    $('.dian'+i+'').css({
      'left': dian_x,
      'top': dian_y
    });
    fto_move(start_obj, $('.dian'+i+''), dian_y, dian_x, end_x, end_y, x_v, y_v, g, callback)
}


function fto_move(wlg, obj, m_dian_y, m_dian_x, m_end_x, m_end_y, m_x_v, m_y_v, m_g, callback){
    var m_time = setInterval(function(){
      m_dian_x += m_x_v;
      m_dian_y += m_y_v;
      m_y_v += m_g;

      if(m_dian_y>=m_end_y){
        obj.css({
          'left': m_end_x,
          'top': m_end_y
        });
        callback(wlg);
        obj.fadeOut('fast',function(){
          obj.remove();
        });
        clearInterval(m_time);
      }
      else{
        obj.css({
          'left': m_dian_x,
          'top': m_dian_y
        });
      }
    },20)
}


function fcallback(obj){
    var txt =  $('.gwc-circle').text();
    $('.gwc-circle').text(++txt);
    var txt2 =  obj.parent().parent().find('.dj').text();
    txt2 = parseFloat(txt2.substr(1,txt2.length-1));
    var jg = $('.js-jg').text();
    jg =  parseFloat(jg.substr(1,jg.length-1));
    var sb = Math.round((jg+txt2)*100)/100;
    $('.js-jg').text('¥'+sb);
}

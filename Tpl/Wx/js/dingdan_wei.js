function showzz() {
    $('.zhezhao').show();
}

$('.quxiao').click(function() {
    $('.zhezhao').hide();
});

/**
 * 取消订单
 * @param  int id 订单id
 */
function cancelOrder(id){
    mui.confirm('取消该订单？', '取消订单', ['否','是'],
        function(e) {
            if (e.index == 1) {
                $.post('/Wx/Orders/cancelOrder', {id: id}, function(data) {
                    if(data['status'] == -1) {
                        mui.toast('操作失败');
                        return;
                    }
                    mui.toast('已取消订单');
                    window.location.reload();
                },'json');
            }
    })
}

/**
 * 确认订单
 * @param  int id 订单id
 */
function conFirmOrder(id){

    mui.confirm('确认收货？', '确认收货', ['否','是'],
        function(e) {
            if (e.index == 1) {
                $.post('/Wx/Orders/conFirmOrder', {oid: id}, function(data) {
                    if(data['status'] == 0) {
                        mui.toast('已确认收货');
                        window.location.reload();
                        return;
                    }
                    mui.toast('操作失败');


                },'json');
            }
    })
}


/**
 * 投诉
 * @param  int shopId 商家id
 * @param  string name 商家名称
 */
function touSu(orderId,shopId,name){
    // console.log(orderId);
    // console.log(shopId);
    // console.log(name);
    // console.log('/Wx/Shop/complaint/shopId/'+shopId+'/orderId/'+orderId+'/name/'+name);
    // return;
    mui.confirm('投诉该商家？', '投诉商家', ['否','是'],
        function(e) {
            if (e.index == 1) {
                window.location.href='/Wx/Shop/complaint/shopId/'+shopId+'/orderId/'+orderId+'/name/'+name;
            }
    })
}
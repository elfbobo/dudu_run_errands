$(document).ready(function(){

    // tab1 功能模块
    $('#classify-l li').click(function(){
        $('#classify-l li').removeClass('active');
        $('#classify-l li span').removeClass('circle-active');
        $(this).addClass('active');
        $(this).children('span').addClass('circle-active');
        $('.classify-r').eq($(this).index()).show().siblings().hide();
    });

    $('.classify-r li').click(function(){
        lei = $(this).data('id');
        $('.classify-r li').removeClass('active');
        $('.classify-r li span').removeClass('circle-active');
        $(this).addClass('active');
        $(this).children('span').addClass('circle-active');
        $('#tab-classify-cnt').slideUp('fast');
        $('#tab-sj-1').toggleClass('tab-active')
        $('#tab-sj-1').children('span').toggleClass('tab-sj-active');
        getShops();
    });

    $('#tab-sj-1').click(function(){
        $('#tab-classify-cnt').stop().slideToggle();
        $(this).toggleClass('tab-active')
        $(this).children('span').toggleClass('tab-sj-active');
        $('#tab-sort-cnt').slideUp('fast');
        $('#tab-sj-2').removeClass('tab-active')
        $('#tab-sj-2').children('span').removeClass('tab-sj-active');
    });

    // tab2
    $('#tab-sort-cnt ul li').click(function(){
        paixu=$(this).data('type');
        $('#tab-sort-cnt ul li').removeClass('sort-active');
        $('#tab-sort-cnt ul li').children('span').removeClass('sort-active');
        $(this).children('span').addClass('sort-active');
        $(this).addClass('sort-active');
        $('#tab-sort-cnt').slideUp('fast');
        $('#tab-sj-2').toggleClass('tab-active')
        $('#tab-sj-2').children('span').toggleClass('tab-sj-active');
        getShops();
    });

    $('#tab-sj-2').click(function(){
        $('#tab-sort-cnt').stop().slideToggle();
        $(this).toggleClass('tab-active')
        $(this).children('span').toggleClass('tab-sj-active');
        $('#tab-classify-cnt').slideUp('fast');
        $('#tab-sj-1').removeClass('tab-active')
        $('#tab-sj-1').children('span').removeClass('tab-sj-active');
    });

    // 店铺滑动
    $('.gg-btm').each(function(index,el){
            var w = $(this).find('li').outerWidth(true);
            var s_w = w*($(this).find('li').length);
            $(this).find('ul').width(s_w);
    })

})
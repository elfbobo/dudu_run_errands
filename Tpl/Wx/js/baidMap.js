/*
* @Author: Marte
* @Date:   2017-06-29 10:17:40
* @Last Modified by:   Marte
* @Last Modified time: 2017-06-29 10:18:11
*/

function baiduMap(){
    navigator.geolocation.getCurrentPosition(function (position) {
        longitude = position.coords.longitude;
        latitude = position.coords.latitude;
        getCity(longitude,latitude); //定位地址
        getShops();//获取店铺列表
    },function(error){
    });
}

//获取地址
function getCity(longitude,latitude){
    var gpsPoint = new BMap.Point(longitude, latitude);
    BMap.Convertor.translate(gpsPoint, 0, function (point) {
        var geoc = new BMap.Geocoder();
        geoc.getLocation(point, function (rs) {
            var addComp = rs.addressComponents;
            document.getElementById('location').innerHTML=addComp['city'] ;
        });
    });
}
<include file="Common:header" />
</head>
<body>
		<div id="section_container">
			<section id="lazyload_section" data-role="section" class="active">
				<header class="bar bar-nav" id="header">
				  	<a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
					<h1 class="title"><?php echo $catename;?></h1>
				</header>
				<header class="bar bar-nav box_shadow head_a" style="margin-top:43px;padding-left:5px;padding-right:5px;">
					<a href="javascript:;" style="width:20%;" class="selected_bottom jx">即将揭晓</a>
					<a href="javascript:;" style="width:20%;" class="lastnew">最新</a>
					<a href="javascript:;" style="width:15%;" class="renqi">人气</a>
					<a href="javascript:;" style="width:15%;" class="price">价格</a>
					<a href="<?php echo U('Index/category');?>" style="width:30%;">商品分类</a>
				</header>
<div id="flat_article" style="top:65px;bottom:50px;background:#fff;width:100%;">
	<div class="scroller create_goods container" style="max-width: 640px;margin:0 auto;">
		<div class="goodslist">
		<div class="container jx_goods">
	<?php
		foreach ($shoplist as $k => $v){
	?>
	<figure <?php if(($k%2) != 0){echo 'style="border-right:none;"';}?>>
		<div class="goods_img">
		<a href="<?php echo U('Index/item',array('gid' => $v['id']))?>">
		<img data-original="<?php echo C('PIC_URL').$v['thumb'];?>" class="lazy" alt="<?php echo $v['title'];?>">
		</a>
		</div>
		<figcaption>
			<p><a href="<?php echo U('Index/item',array('gid' => $v['id']))?>"><?php echo $v['title'];?></a></p>
			<div class="info">
				<div class="left">
				<?php $baifebi = $v['canyurenshu']/$v['zongrenshu']*100;?>
				<progress max="100" value="<?php echo $baifebi;?>" class="css3">
					<div class="progress-bar"></div>
				</progress>
				<span class="bf_color">夺宝进度<em class="bf"><?php echo intval($baifebi);?>%</em></span>
				<div class="clear"></div>
				<button class="common-btn mygroup" goods_id="<?php echo $v['id'];?>"><a href="javascript:;">立即夺宝</a></button>
				<button class="common-btn oneget" flag="true" goods_id="<?php echo $v['id'];?>"><a href="javascript:;">加入清单</a></button>
				</div>
			</div>
		</figcaption>
	</figure>
	<?php } ?>
</div>
		</div>
		
		<ul class="search-ul load-more" style="display: none;">
		<li><button class="btn btn-block btn-more" id="loadmore">加载更加</button></li>
		</ul>
	</div>
</div>
<include file="Common:footer" />
<script type="text/javascript">
$(function(){
	var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
	var order_type = 'default'; //默认排序方式
	var page = 1;
	var clickVal={flag:1};
	var tag = 'list';
	$('.head_a a').click(function(){
		$('.head_a a').removeClass('selected_bottom');
		$(this).addClass('selected_bottom');
	});
	
	ajaxData(order_type,page,tag);
	
	function ajaxData(order_type,page,tag,orders){
		if(typeof orders == 'undefined'){
			orders = '';
		}
		if(clickVal.flag == 2){
			return;
		}else if(clickVal.flag == 3){
		return;}
		clickVal.flag = 2;
		$.ajax({
			url : '/index.php?m=Go&c=Index&a=lists<?php echo $catid;?>&sorts='+orders+'&type='+order_type+'&page='+page,
			type : 'GET',
			dataType : 'json',
			beforeSend : function(){
				$('.btn-more').css('background','#e4e4e4').text('加载中...');
			},
			success : function(jsonx){
				clickVal.flag = 1;
				if(jsonx['isNextPage'] == 0){
					clickVal.flag = 3;
				}
				var txt = '';
				var html = '';
				var json = jsonx['shoplist'];
				for(var i=0; i < json.length; i++){
                    html += '<ul class="search-ul"><li>';
                    html += '<div class="search_goods_img"><a href="/index.php?m=Go&c=Index&a=item&gid='+json[i]['id']+'"><img class="lazy" data-original="<?php echo C('PIC_URL');?>'+json[i]['thumb']+'" /></a></div>';
                    html += '<div class="search_goods_info">';
                    html += '<p class="title_shopname"><a href="/index.php?m=Go&c=Index&a=item&gid='+json[i]['id']+'">'+ json[i]['title'] +'</a></p>';
                    html += '<div class="goods_info_bottom"><div class="goods_con"><progress max="100" value="'+parseInt((json[i]['canyurenshu']/json[i]['zongrenshu'])*100)+'" class="css3">';
                    html += '<div class="progress-bar"></div></progress>';
                    html += '<p><span class="left">总需：'+json[i]['zongrenshu']+'</span><span class="right">剩余：'+json[i]['shenyurenshu']+'</span></p>';
                    html += '</div>';
                    html +=  '<div class="btn_buy addCart" goods_id="'+json[i]['id']+'"><span goods_id="'+json[i]['id']+'" class="clickaddcart" style="display:block;width:35px;height:35px;">购买</span></div>';
                    html += '</div></div></li></ul>';
				}
				setTimeout(function(){
					$('.goodslist').append(html);
					$('.btn-more').hide();
					if(jsonx['isNextPage'] == 0){
						clickVal.flag = 3;
						if(txt == ''){
							$('.btn-more').text('已经到底了').show();
						}else{
							$('.btn-more').text(txt);
							$('.container').width($(window).width());
						}
					}else{
						$('.container').width('auto');
						$('.btn-more').text('加载更多');
						$('.btn-more').css('background','#fff');
					}
					$('.load-more').show();
					layer.close(index);
					$('.layui-layer').remove();
				},1000);

				//飞入购物车动画
				var f_flag = true;
				$('body').on('click','.clickaddcart',function(){
					var _this = $(this);
					if(f_flag == false){
						return false;
					}
					f_flag = false;
					//_this.attr('flag','false');
					var goods_id = parseInt(_this.attr('goods_id'));
					$.ajax({
						type : 'POST',
						url : '?m=Go&c=Ajax&a=addShopCart&gid='+goods_id+'&num='+1,
						beforeSend : function(){
							var img = _this.parents('li').find('img');
							var flyElm = img.clone().css('opacity', 0.85);
							$('body').append(flyElm);
							flyElm.css({
								'z-index': 9000,
								'display': 'block',
								'position': 'absolute',
								'top': img.offset().top +'px',
								'left': img.offset().left +'px',
								'width': img.width() +'px',
								'height': img.height() +'px'
							});
							flyElm.stop().animate({
								top: $('.head-shopcart').offset().top,
								left: $('.head-shopcart').offset().left,
								width: 20,
								height: 20
							}, 'slow', function() {
								f_flag = true;
								flyElm.remove();
							});
						},
						success : function(res){
							var res = $.parseJSON(res);
							if (res['code'] == 0) {
								if(res['num'] > 0){
									$('nav.bar-tab .footer_goods_num').text(res['num']);
									$('nav.bar-tab .footer_goods_num').show();
								}
							}
						}
					});
				});
				
				setTimeout(function(){
					lyzimg();
					$.jqScroll('#flat_article');
				},1020);
			},
		});
	}
	
	//价格
	var price = 1;
	$('.price').click(function(){
		clickVal.flag = 1;
		var orders = '';
		if(price == 1){
			price = 2;
			page = 1;
			orders = 'desc';
			//$('.price').html('价格高至低');
		}else if(price == 2){
			orders = 'asc';
			price = 1;
			page = 1;
			//$('.price').html('价格低到高');
		}
		if(clickVal.flag == 2){
			return;
		}
		order_type = 'price';
		tag = 'list';
		$('.goodslist').html('');
		$('.load-more').hide();
		$('.btn-more').text('加载数据中');
		var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
		ajaxData(order_type,page,tag,orders);
	});
	
	//最新
	$('.lastnew').click(function(){
		order_type = 'lastnew';
		$('.goodslist').html('');
		$('.load-more').hide();
		page = 1;
		clickVal.flag = 1;
		if(clickVal.flag == 2){
			return;
		}
		tag = 'list';
		$('.btn-more').text('加载数据中');
		var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
		ajaxData(order_type,page,tag);
	});
	
	//人气
	$('.renqi').click(function(){
		order_type = 'renqi';
		$('.goodslist').html('');
		$('.load-more').hide();
		page = 1;
		clickVal.flag = 1;
		if(clickVal.flag == 2){
			return;
		}
		
		tag = 'list';
		$('.btn-more').text('加载数据中');
		var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
		ajaxData(order_type,page,tag);
	});
	
	//即将揭晓
	$('.jx').click(function(){
		order_type = 'default';
		$('.goodslist').html('');
		$('.load-more').hide();
		page = 1;
		clickVal.flag = 1;
		if(clickVal.flag == 2){
			return;
		}
		tag = 'list';
		$('.btn-more').text('加载数据中');
		$('.head_a .cate').html('商品分类');
		var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
		ajaxData(order_type,page,tag);
	});

	//加载更多
 	$('#loadmore').click(function(){
		if(clickVal.flag == 2){
			return;
		}else if(clickVal.flag == 3){
			return;
			}
		page += 1;
		var index = layer.load(2, {shade: false}); //0代表加载的风格，支持0-2
		ajaxData(order_type,page,tag);
	});
});

</script>
</body>
</html>
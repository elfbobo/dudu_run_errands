<include file="Common:header" />
<link rel="stylesheet" type="text/css" href="/Tpl/Wx/css/swiper.min.css">
<script src="/Tpl/Wx/js/swiper.min.js"></script>
<style type="text/css">
.one_swiper{height:140px;margin-top:-1px;}
.one_swiper img{width:100%;height:140px;}
</style>
</head>
<body>
	<div id="section_container">			
			<section id="lazyload_section" data-role="section" class="active">
				<header class="bar bar-nav" id="header">
					<div class="titlebar">
				  	<a href="<?php echo U('Index/category');?>" data-toggle="aside"><i class="iconfont iconline-menu"></i></a>
					<h1>1元云购</h1>
					</div>
				</header>
				
<article id="flat_article"  class="active" style="top:35px;bottom:50px;">
<div class="scroller">
<div class="container jx_goods autoLot">

	<div class="swiper-container one_swiper visible-xs-block">
		<div class="swiper-wrapper" id="imgList">
		<php></php>
			<volist name="adInfo" id="ad">
			<div class="swiper-slide">
				<a href="{$ad.adurl}"><img src="/{$ad.adfile}" /></a>
			</div>
			</volist>
		</div>
		<div class="swiper-pagination one-pagination"></div>
	</div>

	<div class="news_jx">
		<span class="jx-span">即将揭晓</span>
		<button class="btn btn-link btn-nav pull-right">
	    	<a href="###" style="display: none;"><span class="icon icon-right-nav"></span></a>
	  	</button>
	</div>
	<div id="autoLottery">
		<div id="afterlist"></div>
		<?php foreach ($okShow as $key => $value){ ?>
		<div class="autotime" item="<?php echo $value['id'];?>" <?php if($value['q_showtime'] == 'Y' && intval($value['q_end_time']-time()) > 1){echo 'tag="start"';}else{echo 'tag="end"';}?> data-endtime="<?php echo intval($value['q_end_time']-time());?>">
			<div class="autotime_left">
			<p><a href="<?php echo U('item',array('gid' => $value['id']));?>"><?php echo _htmtocode($value['title']);?></a></p>
			<div class="lot">
			<?php if($value['q_showtime'] == 'Y' && intval($value['q_end_time']-time()) > 1){ ?>){ ?>
			<span class="count_time"></span>
			<div class="LotteryTime"><b class="minute">00</b>:<b class="second">00</b>:<b class="millisecond">00</b></div>
			<?php }else{ ?>
			<p style="font-size:12px;" class="pz">恭喜<a href="<?php echo U('User/userindex',array('uid' => $value['q_uid']));?>" style="color:#3F5FD5;font-size:12px;"><?php echo get_user_name($value['q_uid']);?></a>获得xx</p>
			<?php } ?>
			</div>
			</div>
			<div class="autotime_right">
			<a href="<?php echo U('item',array('gid' => $value['id']));?>">
				<img alt="" class="lazy" data-original="<?php echo C('PIC_URL').$value['thumb'];?>" />
			</a>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<div class="container jx_goods">	
	<div class="news_jx">
		<span class="jx-span">人气推荐</span>
		<button class="btn btn-link btn-nav pull-right">
	    	<a href="<?php echo U('lists');?>" style="font-size:13px;">更多<span class="icon icon-right-nav"></span></a>
	  	</button>
	</div>
	<?php
		foreach ($hotRecommend as $k => $v){
	?>
	<figure <?php if(($k%2) != 0){echo 'style="border-right:none;"';}?>>
		<div class="goods_img">
		<a href="<?php echo U('Index/item',array('gid' => $v['id']))?>">
		<img data-original="<?php echo C('PIC_URL').$v['thumb'];?>" class="lazy" alt="<?php echo $v['title'];?>">
		</a>
		</div>
		<figcaption>
			<p><a href="<?php echo U('Index/item',array('gid' => $v['id']))?>"><?php echo $v['title'];?></a></p>
			<div class="info">
				<div class="left">
				<?php $baifebi = $v['canyurenshu']/$v['zongrenshu']*100;?>
				<progress max="100" value="<?php echo $baifebi;?>" class="css3">
					<div class="progress-bar"></div>
				</progress>
				<span class="bf_color">夺宝进度<em class="bf"><?php echo intval($baifebi);?>%</em></span>
				<div class="clear"></div>
				<button class="common-btn mygroup" goods_id="<?php echo $v['id'];?>"><a href="javascript:;">立即夺宝</a></button>
				<button class="common-btn oneget" flag="true" goods_id="<?php echo $v['id'];?>"><a href="javascript:;">加入清单</a></button>
				</div>
			</div>
		</figcaption>
	</figure>
	<?php } ?>
</div>


<div id="copyright">
<p>客户端 ｜ <a href="###">触屏版</a> ｜ <a href="http://lyz.***.com">电脑版</a></p>
<p>Copyright © 组团云购 | 京字</p>
</div>

</div>
</article>
<include file="Common:footer" />
<script type="text/javascript">
lyzimg();
$('.mygroup').click(function(){
	var goods_id = parseInt($(this).attr('goods_id'));
	$.ajax({
		type : 'POST',
		url : '?m=Go&c=Ajax&a=addShopCart&gid='+goods_id+'&num='+1,
		beforeSend : function(){
			//
		},
		success : function(res){
			var res = $.parseJSON(res);
			if (res['code'] == 0) {
				window.location.href='<?php echo U('Cart/cartlist');?>';
			}
		}
	});
});

//飞入购物车动画
var f_flag = true;
$('.oneget').click(function(){
	var _this = $(this);
	if(f_flag == false){
		return false;
	}
	f_flag = false;
	//_this.attr('flag','false');
	var goods_id = parseInt(_this.attr('goods_id'));
	$.ajax({
		type : 'POST',
		url : '/index.php?m=Go&c=Ajax&a=addShopCart&gid='+goods_id+'&num='+1,
		beforeSend : function(){
			var img = _this.parents('figure').find('img');
			var flyElm = img.clone().css('opacity', 0.85);
			$('body').append(flyElm);
			flyElm.css({
				'z-index': 9000,
				'display': 'block',
				'position': 'absolute',
				'top': img.offset().top +'px',
				'left': img.offset().left +'px',
				'width': img.width() +'px',
				'height': img.height() +'px'
			});
			flyElm.stop().animate({
				top: $('.head-shopcart').offset().top,
				left: $('.head-shopcart').offset().left,
				width: 20,
				height: 20
			}, 'slow', function() {
				f_flag = true;
				flyElm.remove();
			});
		},
		success : function(res){
			var res = $.parseJSON(res);
			if (res['code'] == 0) {
				if(res['num'] > 0){
					$('nav.bar-tab .footer_goods_num').text(res['num']);
					$('nav.bar-tab .footer_goods_num').show();
				}
			}
		}
	});
});

	//商品分类
	$('.goods_cate li a').click(function(){
		$('.head_a a').removeClass('selected_bottom');
		$('.head_a .cate').html($(this).text()).addClass('selected_bottom');
		$('.goodslist').html('');
		$('.load-more').hide();
		$('#left_overlay_aside').animate({
			'left' : 0,
		},'fast',function(){
			$('#section_container_mask').hide();
		});
		tag = $(this).attr('id');
		window.location.href = '/?/mobile/mobile/categoods/'+tag
	});

$(function(){

	var swiper = new Swiper('.one_swiper', {
		autoplay : 3000,
		paginationtouchendable : false,
		pagination : '.one-pagination',
		observer:true,//修改swiper自己或子元素时，自动初始化swiper
		observeParents:true,//修改swiper的父元素时，自动初始化swiper
		autoplayDisableOnInteraction : false,
	});
	
	$('#autoLottery div.autotime').each(function(index){
		if($(this).size() > 0){
		    if($(this).attr('tag') == 'start'){
		    	autoLottery($(this));
			}
		}
	});
	setInterval(function(){
		var i = 0;
		$('#autoLottery div.autotime').each(function(){
			if($(this).attr('tag') == 'start'){
				i++;
			}
		});
		if(i >= 4){
			return false;
		}
		show_get_lottery();
	},4500);
		
	function show_get_lottery(){
		var gids = '';
		$('#autoLottery div.autotime').each(function(){
			if($(this).attr('tag') == 'start'){
				gids += $(this).attr('item')+'_';
			}
		});
		
		$.ajax({
			url : '/index.php?m=Go&c=Ajax&a=show_get_lottery&ids='+gids,
			type : 'GET',
			success : function(data){
				var data = $.parseJSON(data);
				if(data['status'] == 1){
					var html = '';
					html += '<div id="autoTimeLottery'+data['id']+'" tag="start" class="autotime" item="'+data['id']+'" data-endtime="'+data['q_end_time']+'">';
					html += '<div class="autotime_left">';
					html += '<p><a href="/index.php?m=Go&c=Index&a=item&gid='+data['id']+'">'+data['title']+'</a></p>';
					html += '<div class="lot">';
					html += '<p style="font-size:12px;display:none;" class="pz">恭喜<a href="/index.php?m=Go&c=User&a=userindex&uid='+data['q_uid']+'" style="color:#3F5FD5;font-size:12px;">'+data['username']+'</a>获得</p>';
					html += '<span class="count_time"></span>';
					html += '<div class="LotteryTime"><b class="minute">00</b>:<b class="second">00</b>:<b class="millisecond">00</b></div>';
					html += '</div></div><div class="autotime_right">';
					html += '<a href="/index.php?m=Go&c=Index&a=item&gid='+data['id']+'">';
					html += '<img alt="" src="<?php echo C('PIC_URL');?>'+data['thumb']+'" />';
					html += '</a></div></div>';
					$('#afterlist').after(html);
					var obj = $('#autoTimeLottery'+data['id']);
					autoLottery(obj);
				}
			}
		});

	}

	function autoLottery(obj){
        var _this = $(obj);
        var minute = _this.find('b.minute');
        var second = _this.find('b.second');
        var millisecond = _this.find('b.millisecond');
        var times = (new Date().getTime()) + 1000 * _this.attr('data-endtime'); 
        var goods_id = _this.attr('item');
        var timer = setInterval(function(){
            var time = times - (new Date().getTime());
            if(time <= 1){
              minute.html('00');
              second.html('00');
              millisecond.html('00');
              clearInterval(timer);
              getGoodsInfo(_this,goods_id);
              return false;
            }
            i =  parseInt((time/1000)/60);
            s =  parseInt((time/1000)%60);
            ms =  String(Math.floor(time%1000));
            ms = parseInt(ms.substr(0,2));
            if(i<10)i='0'+i;
            if(s<10)s='0'+s;
            if(ms<10)ms='0'+ms;
            minute.html(i);
            second.html(s);
            millisecond.html(ms);
        },43);
	}

	//获取商品揭晓信息
	function getGoodsInfo(_this,goods_id){
          $.ajax({
              type : 'GET',
              url : '/index.php?m=Go&c=Ajax&a=get_shop_info&gid='+goods_id,
              timeout : 30,
              dataType : 'json',
              async : false,
              beforeSend : function(xhr){
                  var str = '<p style="font-size:12px;" class="pz">'+$(_this).find('div.lot p').html()+'</p>';
            	  $(_this).find('div.lot').html('<p style="font-size:12px;">正在揭晓...</p>');
            	  $(_this).attr('tag','end');
            	  setTimeout(function(){
            		  $(_this).find('div.lot').html(str);
                  },3000);
               },
              success : function(res){
                  if(res['status'] == 1){
                	 
                  }
              }
          });
	}
	
});
	
</script>
</body>
</html>
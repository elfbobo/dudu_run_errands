<include file="Common:public_header" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/lottery.css" rel="stylesheet" type="text/css" />
</head>
<body style="background:#fff;">
<div class="h5-1yyg-v1" id="loadingPicBlock">

<!-- 栏目页面顶部 -->
<include file="Common:top" />

<!-- 内页顶部 -->
	<div id="wrapper" style="background:#fff;">
    <section class="revealed" style="background:#fff;">
	    <div id="divLottery" class="revCon" style="background:#fff;">
	    	<?php foreach ($shoplist['listItems'] as $v){ ?>
			<ul id="<?php echo $v['id'];?>">
				<li class="revConL">
				<a href="<?php echo U('dataserver',array('gid' => $v['id']));?>">
				<img class="lazy" data-original="<?php echo C('PIC_URL').$v['thumb'];?>">
				</a>
				</li>
				<li class="revConR"><dl><dd>
					<a href="<?php echo U('User/userindex',array('uid' => $v['q_uid']));?>">
					<img name="uImg" uweb="<?php echo $v['q_uid'];?>" class="lazy" data-original="<?php echo C('PIC_URL').$v['userphoto'];?>">
					</a>
					</dd><dd>
						<span>获得者<strong>：</strong><a name="uName" href="<?php echo U('User/userindex',array('uid' => $v['q_uid']));?>" uweb="<?php echo $v['uid'];?>" class="rUserName blue"><?php echo $v['q_user'];?></a></span>本期抢购<strong>：</strong>
						<em class="orange arial"><?php echo $v['gonumber'];?></em>人次</dd></dl>
						<dt>幸运抢购码：<em class="orange arial"><?php echo $v['q_user_code'];?></em><br>揭晓时间：<em class="c9 arial"><?php echo $v['q_end_time'];?></em>
			</dt><b class="fr z-arrow"><a style="position:absolute;right:-6px;top:-6px;width:18px;height:18px;display:block;" href="<?php echo U('dataserver',array('gid' => $v['id']));?>"></a></b></li></ul>
			<?php } ?>
	    	<!--
            <div id="divLotteryLoading" class="loading"><b></b>正在加载</div>
            <a id="btnLoadMore" class="loading" href="javascript:;" style="display:none;">点击加载更多</a>
			-->
        </div>
    </section>
	</div>

<include file="Common:footer2" />
</div>
<style>
	.revCon ul{
		height:auto;
	}
</style>
</body>
</html>
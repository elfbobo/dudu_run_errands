<include file="Common:public_header" />
<link href="<?php echo MOBILE_TPL_PATH;?>/css/cartList.css" rel="stylesheet" type="text/css" />
<script id="pageJS" data="<?php echo MOBILE_TPL_PATH;?>/js/Payment.js" language="javascript" type="text/javascript"></script>
<script src="/Public/pingpp.js" type="text/javascript"></script>
<script src="/Public/jquery.form.js" type="text/javascript"></script>
</head>
<body>
<style>
.registerCon select{
background: #FFF none repeat scroll 0% 0%;
border: 5px solid #DDD;
color: #CCC;
border-radius: 5px;
padding: 0px 5px;
display: inline-block;
position: relative;
font-size: 16px;
height: 50px;
width: 101%;
}
.registerCon .loading{
	padding-top: 20px;
	color: #999;
}
.registerCon .form{
	display:none;
}


.regular-radio {
	display: none;
}

.regular-radio + label {
	-webkit-appearance: none;
	background-color: #fafafa;
	border: 1px solid #cacece;
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05);
	padding: 5px;
	border-radius: 50px;
	display: inline-block;
	position: relative;
}

.regular-radio:checked + label:after {
	content: ' ';
	width: 12px;
	height: 12px;
	border-radius: 50px;
	position: absolute;
	top: 0px;
	background: #99a1a7;
	box-shadow: inset 0px 0px 10px rgba(0,0,0,0.3);
	text-shadow: 0px;
	left: 0px;
	font-size: 32px;
}

.regular-radio:checked + label {
	background-color: #e9ecee;
	color: #99a1a7;
	border: 0px solid #ADB8C0;
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.1), inset 0px 0px 10px rgba(0,0,0,0.1);
}

.regular-radio + label:active, .regular-radio:checked + label:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}
</style>
<div class="h5-1yyg-v1">

<header class="bar bar-nav" id="header">
<a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
<h1 class="title">商品结算</h1> 
</header>

<form id="form_paysubmit" action="<?php echo U('Cart/paysubmit');?>" method="post">
    <input name="hidShopMoney" type="hidden" id="hidShopMoney" value="<?php echo $MoenyCount;?>" />
    <input name="hidBalance" type="hidden" id="hidBalance" value="<?php echo $Money;?>" />
    <input name="hidPoints" type="hidden" id="hidPoints" value="<?php echo $member['userScore'];?>" />
    <input name="shopnum" type="hidden" id="shopnum" value="<?php echo $shopnum;?>" />
    <input name="pointsbl" type="hidden" id="pointsbl" value="<?php echo $fufen_dikou;?>" />
     <input type="hidden" id="submitcode" name="submitcode" value="<?php echo $submitcode;?>">
    <div id="wrapper">
    <div>
		<section class="clearfix g-pay-lst">
			<ul>

			 <?php foreach ($shoplist as $key => $val){ ?>
	
				<li>
				    <a href="<?php echo U('Index/item',array('gid' => $val['id']));?>" class="gray6">(第<?php echo $val['qishu'];?>期)<?php echo $val['title'];?>  (<?php echo $val['title2'];?>)</a>
				    <span>
				        <em class="orange arial"><?php echo $val['cart_xiaoji'];?></em>人次
				    </span>
				</li>
			<?php } ?>
	
			</ul>
	
			<p class="g-pay-Total gray9">合计：<span class="orange arial Fb F16"><?php echo $MoenyCount;?></span> 元</p>
			<p class="g-pay-bline"></p>
	    </section>
		<div class="clear"></div>
    	<section class="clearfix g-Cart" style="width:90%;margin:0 auto;">
	   	 <article class="clearfix m-round g-pay-ment">
		    <ul id="ulPayway">
	

			<?php if ($Money >= $MoenyCount){ ?>
				<li class="gray9 z-pay-ye z-pay-grayC">
				<i id="spBalance" class="z-pay-ment" sel="0"></i>
				<span>您可以使用余额付款（账户余额：<?php echo $Money;?> 元）</span>
				</li>
			<?php }else{
					if(isset($ABC)){
				?>
			    <li class="gray6 z-pay-ye z-pay-grayC">
				<a href="<?php echo U('User/userrecharge')?>" class="z-pay-Recharge">去充值</a>
				<span>您的余额不足（账户余额：<?php echo $Money;?> 元）</span>
				</li>
			<?php }}?>
		    </ul>
	    </article>
	    <article id="bankList" class="clearfix mt10 m-round g-pay-ment g-bank-ct" style="display: none;">
		    <ul>
			    <li class="gray6 z-pay-grayC"><s class="z-arrow"></s>选择网银支付</li>
			    <li class="gray9" style="display:none;" umb='CMBCHINA'><i class="z-bank-Roundsel"><s></s></i>招商银行</li>
			    <li class="gray9" style="display:none;" umb='ICBC'><i class="z-bank-Round"><s></s></i>工商银行</li>
			    <li class="gray9" style="display:none;" umb='CCB'><i class="z-bank-Round"><s></s></i>建设银行</li>
		    </ul>
	    </article>
	    <div>
			<?php if ($Money >= $MoenyCount || $fufen_dikou >= $MoenyCount){ ?>
			<div class="block10"></div>
			   <a id="btnPay" href="javascript:;" class="orgBtn common-btn">确认支付</a>
			<?php }else{ ?>
				
<div class="clear"></div>
		 <section class="clearfix g-member">
	    <article class="clearfix mt10 m-round g-pay-ment g-bank-ct">
	        <ul id="ulBankList">
			<li class="gray6">选择平台支付<em class="orange"><?php echo $MoenyCount;?></em>元</li>

<?php foreach ($paylist as $key => $pay){ ?>
			<?php 
				if ($pay['pay_id'] != 1 && $pay['pay_id'] != 2 && $pay['pay_id'] != 3 && $pay['pay_id'] != 4 && $pay['pay_id'] != 8){ 
			?>
			     <li class="gray9" urm="<?php echo $pay['pay_id'];?>">
			         <i><s></s>
					 <input id="<?php echo $pay['pay_id'];?>" class="regular-radio" checked="checked" type="radio" value="<?php echo $pay['pay_id'];?>" name="account" id="Tenpay">
					<label for="<?php echo $pay['pay_id'];?>"></label>
					 </i><?php echo $pay['pay_name'];?>
			     </li>
			<?php } ?>
<?php } ?>

		    </ul>

	    </article>
		<div class="mt10 f-Recharge-btn">
					
		    <a id="btnSubmit" href="javascript:;" class="orgBtn">
		   
			<input id="submit_ok" class="shop_pay_but common-btn" type="submit" name="submit" value="马上支付" style="background-color: transparent;width: 100%;"></a>
	    </div>
			
	    </div>
<?php }?>
    </section>
	</div>
	</div>
</form>
<include file="Common:footer2"/>
<script type="text/javascript">
$(function(){
	$.jqScroll('#wrapper');
   // console.log();
    var alipay =$("#ulBankList >.gray9")[0];
    var weixinpay =$("#ulBankList >.gray9")[1];
    if(isWeiXin()){

      //  alipay.style.display = "none";
    }else{

      //  weixinpay.style.display = "none";

    }



<?php if ($flag){ ?>
    var cki = true;
	$('#form_paysubmit').submit(function(){
		$('#btnSubmit').css({'background':'#999999'});
		if(cki == false) return false;
		cki = false;
		$(this).ajaxSubmit({
			type : 'POST',
			url : '<?php echo $pay_path;?>',
			data:{
				channel : 'wx_pub',
				amount : '<?php echo $onlineMoenyCount;?>',
				code : '<?php echo $openid;?>'
			},success:function(responseText){
				if(typeof responseText == 'object'){
					if(responseText['status'] == '2'){
						window.location.href="<?php echo $gourl;?>";
						return;
					}
					cki = true;
					alert(responseText['msg']);
				}else if(typeof responseText == 'string'){
					var obj = $.parseJSON(responseText);
		            pingpp.createPayment(responseText, function(result, err) {
		               if(result == 'success'){
							window.location.href="<?php echo $gourl;?>";
					   		return;
					   }else if(result == 'cancel'){
						   $('#btnSubmit').css({'background':'#ff1b1b'});
					   		alert('取消支付');
							cki = true;
					   }else{
						   $('#btnSubmit').css({'background':'#ff1b1b'});
					   		alert('支付失败');
							cki = true;
					   }
					   $.post('<?php echo U('Cart/os');?>',{i:obj['id']},function(s){});
		            });
				}
			}
		});
		return false;
	});

<?php } ?>


});

        function isWeiXin() {
            var ua = window.navigator.userAgent.toLowerCase();
            if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                return true;
            } else {
                return false;
            }
        }
</script>
</div>
<style>
.g-pay-lst li{
	max-height:40px !important;
}
</style>
</body>
</html>
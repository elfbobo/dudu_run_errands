<include file="Common:header" />
<head>
<title><?php echo $msgTitle;?></title>
<style type="text/css">
body{
	padding:0;
	margin:0;
}
.info{
	width:80%;
	height:80px;
	margin:10px auto;
}
.text{
	height:38px;
	display:block;
	text-align:center;
	line-height:38px;
	font-size:14px;
	color:#ff1b1b;
}
.success{
	
}
.jump{
	text-align:center;
}
.error{
	
}
</style>
<body>
<div id="section_container">
	<section id="lazyload_section" data-role="section" class="active">
		<header class="bar bar-nav" id="header">
		  	<a class="icon icon-left-nav pull-left" href="javascript:;"  onclick="history.go(-1)"></a>
			<h1 class="title"><?php echo $msgTitle;?></h1>
		</header>

<div id="flat_article" style="top:65px;bottom:50px;background:#fff;width:100%;">

<?php 
if($status){
?>

<div class="info">
    <span class="text success"><?php echo $message;?></span>
<p class="jump">
页面自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a> 等待时间： <b id="wait"><?php echo($waitSecond); ?></b>
</p>
</div>

<?php
}else{
?>

<div class="info">
    <span class="text error"><?php echo $error;?></span>
<p class="jump">
页面自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a> 等待时间： <b id="wait"><?php echo($waitSecond); ?></b>
</p>
</div>

<?php 
} 
?>
</div>
</section>
</div>
<script type="text/javascript">
(function(){
	var href = document.getElementById('href');
	var wait = document.getElementById('wait');
	var setIntval = setInterval(function(){
		var time = --wait.innerHTML;
		if(time <= 0){
			location.href = href;
			clearInterval(setIntval);
		}
	},1000);
})();
</script>
</body>
</html>
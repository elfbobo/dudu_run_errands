<style>
.head-shopcart{
	position:relative;
}
.pPic{padding-bottom:5px !important;}
.footer_goods_num{
	display:none;
	font-size:12px;
	background:red;
	border-radius:50%;
	position:absolute;
	top:1px;
	font-style:normal;
	left:42px;
	width:20px;
	height:20px;
	line-height:20px;
	z-index:999;
	color:#fff;
}
nav.bar-tab span.tab-label{margin-top:-1px;}
nav.bar-tab span.icon{font-size:20px;margin-top:1px;}
nav.bar-tab{
	background-image: linear-gradient(to top,#f9f9f9,#ececec);
    background-image: -webkit-linear-gradient(to top,#f9f9f9,#ececec); /* Safari 5.1 - 6.0 */
	background-image: -o-linear-gradient(to top,#f9f9f9,#ececec); /* Opera 11.1 - 12.0 */
	background-image: -moz-linear-gradient(to top,#f9f9f9,#ececec); /* Firefox 3.6 - 15 */
	background-image: linear-gradient(to top,#f9f9f9,#ececec);
	border-color:#c0bfbf;
}
.bar-tab a{
	width:20%;
}
</style>
<footer class="footer">
<div style="bottom: 0px;">
<nav class="bar bar-tab" style="background:#fff;border-top:1px solid #ddd;">
  	<a class="tab-item" href="<?php echo U('Wx/Index/index');?>">
    	<span class="tab_menu_icon tab_menu_icon1"></span>
		<span class="tab-label">首页</span>
  	</a>
  	<a class="tab-item<?php if (isset($list_active)){ echo ' '.$list_active;}?>" href="<?php echo U('Index/index');?>">
    	<span class="tab_menu_icon tab_menu_icon2"></span>
		<span class="tab-label">1元云购</span>
  	</a>
  	<a class="tab-item<?php if (isset($lottery_active)){ echo ' '.$lottery_active;}?>" href="<?php echo U('Index/lottery');?>">
		<span class="tab_menu_icon <?php if(isset($lottery_active)){ echo ' tab_menu_icon3_selected';}else{?>tab_menu_icon3<?php }?>"></span>
    	<span class="tab-label">最新揭晓</span>
  	</a>
  	<a class="tab-item head-shopcart<?php if (isset($cart_active)){ echo ' '.$cart_active;}?>" href="<?php echo U('Cart/cartlist');?>">
    	<span class="tab_menu_icon <?php if(isset($cart_active)){ echo ' tab_menu_icon4_selected';}else{?>tab_menu_icon4<?php }?>"></span>
		<span class="tab-label">购物车</span>
		<em class="footer_goods_num" style="display:none;">0</em>
	</a>
  	<a class="tab-item <?php if (isset($re_active)){ echo ' '.$re_active;}?>" href="<?php echo U('User/userbuylist');?>">
		<span class="tab_menu_icon  <?php if(isset($re_active)){ echo ' tab_menu_icon5_selected';}else{?>tab_menu_icon5<?php }?>"></span>
    	<span class="tab-label">我的夺宝</span>
  	</a>
</nav>
</div>
</footer>
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/ratchet/css/ratchet.min.css">
<link rel="stylesheet" href="<?php echo MOBILE_TPL_PATH;?>/assets/agile/css/flat/iconline.css">
<script type="text/javascript" src="<?php echo MOBILE_TPL_PATH; ?>/layer2.1/layer.js"></script>
<script type="text/javascript">
var Path = new Object();
Path.Skin="<?php echo MOBILE_TPL_PATH;?>";
Path.M = "<?php echo MODULE_NAME;?>";
Path.Webpath = "<?php echo WEB_URL;?>";
var Base = {head: document.getElementsByTagName("head")[0] || document.documentElement,Myload: function(B, A) {this.done = false;B.onload = B.onreadystatechange = function() {if (!this.done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {this.done = true;A();B.onload = B.onreadystatechange = null;if (this.head && B.parentNode) {this.head.removeChild(B)}}}},getScript: function(A, C) {var B = function() {};if (C != undefined) {B = C}var D = document.createElement("script");D.setAttribute("language", "javascript");D.setAttribute("type", "text/javascript");D.setAttribute("src", A);this.head.appendChild(D);this.Myload(D, B)},getStyle: function(A, B) {var B = function() {};if (callBack != undefined) {B = callBack}var C = document.createElement("link");C.setAttribute("type", "text/css");C.setAttribute("rel", "stylesheet");C.setAttribute("href", A);this.head.appendChild(C);this.Myload(C, B)}};
function GetVerNum() {var D = new Date();return D.getFullYear().toString().substring(2, 4) + '.' + (D.getMonth() + 1) + '.' + D.getDate() + '.' + D.getHours() + '.' + (D.getMinutes() < 10 ? '0': D.getMinutes().toString().substring(0, 1))}
Base.getScript('<?php echo MOBILE_TPL_PATH;?>/js/Bottom.js');
lyzimg();
<?php if(!isset($onscroll)){?>
$(function(){
	$.jqScroll('#wrapper');
});
<?php } ?>
</script>
<?php echo $shareScript;?>
<include file="Common:header" />
<style type="text/css">
li b span {
  padding-top: 10px;
}
li b span {
  width:25%;
  text-align:center;
  float: left;
}
</style>
</head>
	<body>
		<div id="section_container">
			<section id="index_section" data-role="section" class="active">
				<header class="bar bar-nav" id="header">
				  	<a class="icon icon-left-nav pull-left"  href="javascript:;"  onclick="history.go(-1)"></a>
				  	<h1 class="title"><?php echo $title;?></h1>
				</header>
				
<article data-role="article" id="flat_article" data-scroll="verticle" class="active" style="top:44px;bottom:50px;">

	<div class="scroller create_goods">
	    <ul class="listitem count_goods" style="background:#f5f5f5">
	        <li style="border-bottom:1px solid #ddd;">
	            <a href="<?php echo U('Index/dataserver',array('gid' => $record['shopid']));?>"><img src="<?php echo C('PIC_URL').$record['thumb'];?>" width="25%" class="img"/></a>
	        	<div class="text">
	                <p><a href="<?php echo U('Index/dataserver',array('gid' => $record['shopid']));?>"><?php echo $record['shopname'];?></a></p>
					<?php $jiexiao = get_shop_if_jiexiao($record['shopid']);
						if($jiexiao['q_uid']){
					 ?>
						<p>获得者：<em class="blue" style="font-style: normal;"><?php echo get_user_name($jiexiao['q_user']);?></em></p>
						<p>揭晓时间：<em class="gray6" style="font-style: normal;"><?php echo $record['q_end_time'];?></em></p>
						<p class="orsu">订单状态：<?php echo $record['status'];?></p>
						<p>物流公司：<?php echo $record['company']?$record['company']:'---';?>&nbsp;&nbsp;运费：<?php echo $record['company_money']?$record['company_money']:'0.00';?></p>
						<p>物流单号：<?php echo $record['company_code']?$record['company_code']:'---';?></p>
						<p>收货地址：<?php echo $record['address'];?></p>
						<?php if ($record['status'] != '已付款,已发货,已完成' && $record['status'] != '已付款,未发货,未完成'){?>
						<p><span class="common-btn" style="width:73px;">确认收货</span></p>
					<?php } }?>
				</div>
	        </li>
	        <li style="border-bottom: 1px solid #ddd;">
	        	<span>本期商品您总共拥有<em class="orange" style="font-style: normal;"><?php echo $record['gonumber'];?> </em>个<?php echo _cfg('web_name_two');?>码</span>
	        	<p class="gray9"><?php echo microt($record['time']);?>&nbsp;&nbsp;<span><?php echo $record['gonumber'];?>人次</span></p>
	        </li>
	    </ul>
		<div class="block10"></div>
		    <ul>
			
			    <li><b><?php echo yunma($record['goucode']);?></b></li>
		
		    </ul>
	</div>


</article>
<include file="Common:footer" />
<script type="text/javascript">
$(function(){
	$('.common-btn').click(function(){
		var orderid = <?php echo $record['id'];?>;
		$.ajax({
			type:'get',
			url:'/index.php?m=Go&c=User&a=getgoods&orderid='+orderid,
			success:function(data){
				if(data['status']){
					$('.orsu').text("订单状态：已付款,已发货,已完成");
					$('.common-btn').hide();
				}else{
					alert('确认失败');}
			}});
	});
});
</script>
</body>
</html>
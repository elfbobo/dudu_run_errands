<?php
require 'config.php';

function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
    $lo = gcjBd($lat2, $lng2);
    $lat2 = $lo['lat'];
    $lng2 = $lo['lng'];
    $EARTH_RADIUS = 6378.137;
    $PI = 3.1415926;
    $radLat1 = $lat1 * $PI / 180.0;
    $radLat2 = $lat2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 1000);
    if ($len_type > 1) {
        $s /= 1000;
    }
    return round($s, $decimal);
}

$user_id = $_REQUEST['user_id'];
$address_id = $_REQUEST['address_id'];
//$user_id=1;
$sql = "select * from `" . $oto . "_user_address` where `userId`='" . $user_id . "' and addressId='{$address_id}'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $user_address[] = $row; //将取得的所有数据赋值给person_info数组
}

if(empty($_REQUEST['shopId'])){
    echo json_encode($user_address);
    exit();
}
$shopId = $_REQUEST['shopId'];
$sql = "select * from `" . $oto . "_shops` where `shopId`='{$shopId}'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $shops_info = $row; //将取得的所有数据赋值给person_info数组
}
$ptm = getPtDeliveryConfig();
if (isset($user_address)) {
    for ($i = 0; $i < count($user_address); $i++) {
        $dizhi = '';
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId1'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $sheng[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId2'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $shi[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId3'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $xian[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_communitys` where `communityId`='" . $user_address[$i]['communityId'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $jiedao[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // if (isset($jiedao)) {
        //     $jiedao_z = $jiedao[$i]['communityName'];
        // } else {
        //     $jiedao_z = '';
        // }

        // $dizhi = $sheng[$i]['areaName'] . $shi[$i]['areaName'] . $xian[$i]['areaName'] . $jiedao_z;
        // $user_address[$i]['areaId1_name'] = $sheng[$i]['areaName'];
        // $user_address[$i]['areaId2_name'] = $shi[$i]['areaName'];
        // $user_address[$i]['areaId3_name'] = $xian[$i]['areaName'];
        // $user_address[$i]['communityName'] = $jiedao_z;
        $user_address[$i]['dizhi'] = $dizhi . $user_address[$i]['address'];
        $user_address[$i]['deliveryMoney'] = $shops_info['deliveryMoney'];
        if ($shops_info['deliveryType'] == 1 && !empty($ptm)) {
            $lat = $user_address[$i]['lat'];
            $lon = $user_address[$i]['lng'];
            $lat2 = $shops_info['latitude'];
            $lon2 = $shops_info['longitude'];
            $shops_info['juli'] = getDistance($lat, $lon, $lat2, $lon2);

            if ($shops_info['juli']) {
                for ($j = 0; $j < count($ptm); $j++) {
                    if ($shops_info['juli'] <= $ptm[$j]['go_distance'] * 1000) {
                        $user_address[$i]['deliveryCostTime'] = $ptm[$j]['go_distance_time2'];
                        $user_address[$i]['deliveryMoney'] = $ptm[$j]['go_money'];
                        $user_address[$i]['juli'] = $shops_info['juli'];
                        break;
                    }
                    if ($j == count($ptm) - 1 && $shops_info['juli'] > $ptm[$j]['go_distance'] * 1000) {
                        $user_address[$i]['isCan'] = 0;
                    }
                }
            }
        }
    }
    echo json_encode($user_address);
} else {
    echo '[]';
}

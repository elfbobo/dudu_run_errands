<?php
require 'config.php';

function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
    $lo = gcjBd($lat2, $lng2);
    $lat2 = $lo['lat'];
    $lng2 = $lo['lng'];
    $EARTH_RADIUS = 6378.137;
    $PI = 3.1415926;
    $radLat1 = $lat1 * $PI / 180.0;
    $radLat2 = $lat2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 1000);
    if ($len_type > 1) {
        $s /= 1000;
    }
    return round($s, $decimal);
}

$shopId = $_REQUEST['shopId'];
$sql = "select * from `" . $oto . "_shops` where `shopId`='{$shopId}'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $shops_info[] = $row; //将取得的所有数据赋值给person_info数组
}
//计算平台配送费/配送时间
$ptm = getPtDeliveryConfig();
if ($shops_info[0]['deliveryType'] == 1 && !empty($ptm)) {
    $lat = $_REQUEST['lat'];
    $lon = $_REQUEST['lon'];
    $lat2 = $shops_info[0]['latitude'];
    $lon2 = $shops_info[0]['longitude'];
    $shops_info[0]['juli'] = getDistance($lat, $lon, $lat2, $lon2);

    if ($shops_info[0]['juli']) {
        for ($j = 0; $j < count($ptm); $j++) {
            if ($shops_info[0]['juli'] <= $ptm[$j]['go_distance'] * 1000) {
                $shops_info[0]['deliveryCostTime'] = $ptm[$j]['go_distance_time2'];
                $shops_info[0]['deliveryMoney'] = $ptm[$j]['go_money'];
                break;
            }
            if ($j == count($ptm) - 1 && $shops_info[0]['juli'] > $ptm[$j]['go_distance'] * 1000) {
                $shops_info = array();
            }
        }
    }
}

echo json_encode($shops_info);

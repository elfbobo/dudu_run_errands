<?php
require 'config.php';

$user_id = $_REQUEST['user_id'];
$orderId = $_REQUEST['orderId'];
//$user_id=59;
//$orderId=51;
$sql = "select o.*,g.*,s.shopName,s.shopImg,s.shopTel from `" . $oto . "_orders` as o,`" . $oto . "_order_goods` as g,`" . $oto . "_shops` as s where o.userId='" . $user_id . "' and `orderFlag`='1' and o.orderId=g.orderId and s.shopId=o.shopId and o.orderId='$orderId'";
//echo $sql;
//die();
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $orders[] = $row; //将取得的所有数据赋值给person_info数组
}

$ptconnect = ptconnect();

if (isset($orders)) {
    for ($i = 0; $i < count($orders); $i++) {
    	
    	/*
        if ($orders[$i]['staffId'] == null || is_null($orders[$i]['staffId'])) {
            $orders[$i]['psy'] = '未指定配送员';
            $orders[$i]['psy_phone'] = '';
        } else if ($orders[$i]['staffId'] == 0) {
            $orders[$i]['psy'] = '店主';
            $orders[$i]['psy_phone'] = $orders[$i]['shopTel'];
        } else {
            $staffId = $orders[$i]['staffId'];
            $sql = "select username,telephone from `" . $oto . "_staff` where id='{$staffId}'";
            $result = $db->query($sql);
            while ($row = $result->fetch_assoc()) {
                $psy[$i][] = $row; //将取得的所有数据赋值给person_info数组
            }
            if (isset($psy[$i])) {
                $orders[$i]['psy'] = $psy[$i][0]['username'];
                $orders[$i]['psy_phone'] = $psy[$i][0]['telephone'];
            } else {
                $orders[$i]['psy'] = '未指定配送员';
                $orders[$i]['psy_phone'] = '';
            }
        }
        */
    	
        $query = "select * from pt_delivery_order where orderId=? and user_send=0";
        $orders[$i]['psy'] = '暂时无配送员信息';
        $orders[$i]['psy_phone'] = '';
        if ($stmt = $ptconnect->prepare($query)) {
        	$stmt->bind_param('i', $orderId);
        	$stmt->execute();
        	$result = $stmt->get_result();
        	$district = $result->fetch_assoc();
        	if (!empty($district)){
        		$query2 = "select * from pt_user where id={$district['userId']}";
        		$stmt = $ptconnect->prepare($query2);
        		$stmt->execute();
        		$result = $stmt->get_result();
        		$user = $result->fetch_assoc();
        		if (!empty($user)){
        			$orders[$i]['psy'] = $user['truename'];
        			$orders[$i]['psy_phone'] = $user['mobile'];
        			$orders[$i]['face'] = $user['user_face'];
        		}
        	}
        	$stmt->close();
        }
        
        $sql = "SELECT * FROM " . $oto . "_goods_appraises where orderId='{$orderId}'";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $pingjia[$i][] = $row; //将取得的所有数据赋值给person_info数组
        }
        if (isset($pingjia[$i])) {
            $orders[$i]['isPingjia'] = 1;
        } else {
            $orders[$i]['isPingjia'] = 0;
        }

        $shopId = $orders[$i]['shopId'];
        $sql = "select * from `" . $oto . "_complain` where shopId='{$shopId}' and `userId`='{$user_id}' and isFlag='1' and orderId={$orderId}";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $suggest[$i][] = $row; //将取得的所有数据赋值给person_info数组
        }
        if (isset($suggest[$i])) {
            $orders[$i]['isTousu'] = 1;
        } else {
            $orders[$i]['isTousu'] = 0;
        }

    }
    
    $data[0]['orderId'] = $orders[0]['orderId'];
    $data[0]['orderNo'] = $orders[0]['orderNo'];
    $data[0]['shopTel'] = $orders[0]['shopTel'];
    $data[0]['shopName'] = $orders[0]['shopName'];
    $data[0]['deliverType'] = $orders[0]['deliverType'];
    $data[0]['orderStatus'] = $orders[0]['orderStatus'];
    $data[0]['psy'] = $orders[0]['psy'];
    $data[0]['psy_phone'] = $orders[0]['psy_phone'];
    $data[0]['isPingjia'] = $orders[0]['isPingjia'];
    $data[0]['isTousu'] = $orders[0]['isTousu'];
    $data[0]['userName'] = $orders[0]['userName'];
    $data[0]['userPhone'] = $orders[0]['userPhone'];
    $data[0]['userAddress'] = $orders[0]['userAddress'];
    $data[0]['payType'] = $orders[0]['payType'];
    $data[0]['createTime'] = $orders[0]['createTime'];
    $data[0]['requireTime'] = $orders[0]['requireTime'];
    $data[0]['shopId'] = $orders[0]['shopId'];
    $data[0]['shopImg'] = $orders[0]['shopImg'];
    $data[0]['deliverMoney'] = $orders[0]['deliverMoney'];
    $data[0]['needPay'] = $orders[0]['needPay'];
    $data[0]['couponMoney'] = $orders[0]['couponMoney'];
    $data[0]['orderRemarks'] = $orders[0]['orderRemarks'];
    $data[0]['face'] = isset($orders[0]['face']) ? $orders[0]['face'] : '';
    $data[0]['urgent'] = $orders[0]['urgent'];
    switch ($orders[0]['urgent']){
    	case 1:
    		$data[0]['urgent_text'] = '正常配送';
    		break;
    	case 2:
    		$data[0]['urgent_text'] = '加急配送';
    		break;
    }
    switch ($orders[0]['car']){
    	case 1:
    		$data[0]['car_text'] = '摩托车(电动车)';
    		break;
    	case 2:
    		$data[0]['car_text'] = '小汽车';
    		break;
    	case 3:
    		$data[0]['car_text'] = '火车';
    		break;
    }
    
    for ($i = 0; $i < count($orders); $i++) {
        $data[0]['list'][$i]['goodsName'] = $orders[$i]['goodsName'];
        $data[0]['list'][$i]['goodsNums'] = $orders[$i]['goodsNums'];
        $data[0]['list'][$i]['goodsPrice'] = $orders[$i]['goodsPrice'];
        $data[0]['list'][$i]['goodsAttrName'] = $orders[$i]['goodsAttrName'];
        $data[0]['list'][$i]['goodsThums'] = $orders[$i]['goodsThums'];
        $data[0]['list'][$i]['goodsId'] = $orders[$i]['goodsId'];
    }
}
echo json_encode($data);

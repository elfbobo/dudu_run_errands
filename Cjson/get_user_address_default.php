<?php
require 'config.php';

function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
	$lo = gcjBd($lat2, $lng2);
	$lat2 = $lo['lat'];
	$lng2 = $lo['lng'];
	$EARTH_RADIUS = 6378.137;
	$PI = 3.1415926;
	$radLat1 = $lat1 * $PI / 180.0;
	$radLat2 = $lat2 * $PI / 180.0;
	$a = $radLat1 - $radLat2;
	$b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
	$s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
	$s = $s * $EARTH_RADIUS;
	$s = round($s * 1000);
	if ($len_type > 1) {
		$s /= 1000;
	}
	return round($s, $decimal);
}

$user_id = $_REQUEST['user_id'];
$carType = $_REQUEST['carType'];
$urgent = $_REQUEST['urgent'];
//$user_id=1;
$sql = "select * from `" . $oto . "_user_address` where `userId`='" . $user_id . "' and `isDefault`='1'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
	$user_address[] = $row; //将取得的所有数据赋值给person_info数组
}

$shopId = $_REQUEST['shopId'];
$sql = "select * from `" . $oto . "_shops` where `shopId`='{$shopId}'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
	$shops_info = $row; //将取得的所有数据赋值给person_info数组
}
$ptm = getPtDeliveryConfig();

if (isset($user_address)) {
	for ($i = 0; $i < count($user_address); $i++) {
		$dizhi = '';
		$user_address[$i]['dizhi'] = $dizhi . $user_address[$i]['address'];
		$user_address[$i]['deliveryType'] = $shops_info['deliveryType'];
		//if ($shops_info['deliveryType'] == 1 && !empty($ptm)) {
		if (!empty($ptm)) {
			$lat = $user_address[$i]['lat'];
			$lon = $user_address[$i]['lng'];
			$lat2 = $shops_info['latitude'];
			$lon2 = $shops_info['longitude'];
			$shops_info['juli'] = getDistance($lat, $lon, $lat2, $lon2);
			$user_address[$i]['juli'] = $shops_info['juli'];
			if ($shops_info['juli']) {
				for ($j = 0; $j < count($ptm); $j++) {
					if ($shops_info['juli'] <= $ptm[$j]['go_distance'] * 1000) {
						$user_address[$i]['deliveryCostTime'] = $ptm[$j]['go_distance_time2'];
						/**
						 <option value="1">摩托车(电动车)</option>
						 <option value="2">小汽车</option>
						 <option value="3">火车</option>
						 1:正常配送，2加急
						 */
						$deliveryMoney = $ptm[$j]['go_money'];
						switch ($carType){
							case 1:
								$deliveryMoney += $ptm[$j]['go_money'];
								break;
							case 2:
								$deliveryMoney += $ptm[$j]['go_money2'];
								break;
							case 3:
								$deliveryMoney += $ptm[$j]['go_money3'];
								break;
						}
						if ($urgent == 2){
							$ptdb = ptconnect();
							$sql_x = "SELECT `value` FROM`pt_system` WHERE `name` = 'user_urgent' LIMIT 1";
							$result_x = $ptdb->query($sql_x);
							if ($result_x){
								$value = array();
								while ($row = $result_x->fetch_assoc()) {
									$value = $row;
								}
								if (!empty($value)){
									$user_urgent = (float)$value['value']/100;
									$deliveryMoney = $deliveryMoney + $deliveryMoney*$user_urgent;
								}
							}
						}
						$user_address[$i]['deliveryMoney'] = $deliveryMoney;
						if ($ptm[$j]['go_distance_time2'] <= 60){ //分钟
							$user_address[$i]['predict_time'] = $ptm[$j]['go_distance_time2'].'分钟';
						}elseif ($ptm[$j]['go_distance_time2'] <= 60*24){ //分钟
							$startTime = time();
							$endTime = time()+$ptm[$j]['go_distance_time2']*60;
							$hour=floor(($endTime-$startTime)%86400/3600);
							$minute=floor(($endTime-$startTime)%86400/60%60);
							$user_address[$i]['predict_time'] = $hour.'小时'.$minute.'分钟';
						}else{
							$user_address[$i]['predict_time'] = date('Y-m-d H:i',time()+$ptm[$j]['go_distance_time2']*60);
						}
						break;
					}
					if ($j == count($ptm) - 1 && $shops_info['juli'] > $ptm[$j]['go_distance'] * 1000) {
						$user_address = "";
					}
				}
			}
		}
	}
	
	echo json_encode($user_address);
} else {
	echo '[]';
}


/*
exit;
function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
    $lo = gcjBd($lat2, $lng2);
    $lat2 = $lo['lat'];
    $lng2 = $lo['lng'];
    $EARTH_RADIUS = 6378.137;
    $PI = 3.1415926;
    $radLat1 = $lat1 * $PI / 180.0;
    $radLat2 = $lat2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 1000);
    if ($len_type > 1) {
        $s /= 1000;
    }
    return round($s, $decimal);
}

$user_id = $_REQUEST['user_id'];
//$user_id=1;
$sql = "select * from `" . $oto . "_user_address` where `userId`='" . $user_id . "' and `isDefault`='1'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $user_address[] = $row; //将取得的所有数据赋值给person_info数组
}

$shopId = $_REQUEST['shopId'];
$sql = "select * from `" . $oto . "_shops` where `shopId`='{$shopId}'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $shops_info = $row; //将取得的所有数据赋值给person_info数组
}
$ptm = getPtDeliveryConfig();
if (isset($user_address)) {
    for ($i = 0; $i < count($user_address); $i++) {
        $dizhi = '';
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId1'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $sheng[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId2'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $shi[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_areas` where `areaId`='" . $user_address[$i]['areaId3'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $xian[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // $sql = "select * from `" . $oto . "_communitys` where `communityId`='" . $user_address[$i]['communityId'] . "'";
        // $result = $db->query($sql);
        // while ($row = $result->fetch_assoc()) {
        //     $jiedao[$i] = $row; //将取得的所有数据赋值给person_info数组
        // }
        // if (isset($jiedao)) {
        //     $jiedao_z = $jiedao[$i]['communityName'];
        // } else {
        //     $jiedao_z = '';
        // }

        // $dizhi = $sheng[$i]['areaName'] . $shi[$i]['areaName'] . $xian[$i]['areaName'] . $jiedao_z;
        $user_address[$i]['dizhi'] = $dizhi . $user_address[$i]['address'];
        if ($shops_info['deliveryType'] == 1 && !empty($ptm)) {
            $lat = $user_address[$i]['lat'];
            $lon = $user_address[$i]['lng'];
            $lat2 = $shops_info['latitude'];
            $lon2 = $shops_info['longitude'];
            $shops_info['juli'] = getDistance($lat, $lon, $lat2, $lon2);

            if ($shops_info['juli']) {
                for ($j = 0; $j < count($ptm); $j++) {
                    if ($shops_info['juli'] <= $ptm[$j]['go_distance'] * 1000) {
                        $user_address[$i]['deliveryCostTime'] = $ptm[$j]['go_distance_time2'];
                        $user_address[$i]['deliveryMoney'] = $ptm[$j]['go_money'];
                        break;
                    }
                    if ($j == count($ptm) - 1 && $shops_info['juli'] > $ptm[$j]['go_distance'] * 1000) {
                        $user_address = "";
                    }
                }
            }
        }
        // $user_address[$i]['areaId1_name'] = $sheng[$i]['areaName'];
        // $user_address[$i]['areaId2_name'] = $shi[$i]['areaName'];
        // $user_address[$i]['areaId3_name'] = $xian[$i]['areaName'];
        // $user_address[$i]['communityName'] = $jiedao_z;
    }

    echo json_encode($user_address);
} else {
    echo '[]';
}

//echo json_encode($user_address);
*/
<?php
		require('config.php');
		function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
		{ 
		 	$EARTH_RADIUS=6378.137; 
		 	$PI=3.1415926; 
		 	$radLat1 = $lat1 * $PI / 180.0;
		 	$radLat2 = $lat2 * $PI / 180.0;
		 	$a = $radLat1 - $radLat2; 
		 	$b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
		 	$s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
		 	$s = $s * $EARTH_RADIUS; 
		 	$s = round($s * 1000); 
		 	if ($len_type > 1) 
		 	{ 
		 		$s /= 1000; 
		 	} 
		 	return round($s,$decimal); 
		}
		$sql="select * from ".$oto."_sys_configs where fieldName='店铺附近范围'";
        $result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$sys_configs[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($sys_configs))
		{
			$fanwei=$sys_configs[0]['fieldCode'];
		}
        else $fanwei=200000;//搜索附近的范围，单位m

		$lat=$_REQUEST['lat'];
		$lon=$_REQUEST['lon'];
		$text=$_REQUEST['text'];
		$sousuo_way=$_REQUEST['sousuo_way'];
		if($sousuo_way==0 || $sousuo_way=='0')
		{
			$sql="select h.title,h.content,h.id,h.money,h.lat,h.lon,h.img,u.userName,u.userPhoto,u.grage,u.userBond,u.isBondShow from `".$oto."_help` as h,".$oto."_users as u where h.isShow='1' and h.isDel='0' and u.userId=h.userId and (h.title LIKE '%$text%' or h.content LIKE '%$text%')";
			$sql.=" order by h.createTime DESC ";
			$result=$db->query($sql);
			while($row=$result->fetch_assoc()){
				$help[]=$row;//将取得的所有数据赋值给person_info数组
			}
			if(isset($help))
			{
				for($i=0;$i<count($help);$i++)
				{
					$lat2=$help[$i]['lat'];
					$lon2=$help[$i]['lon'];
					$wb_id=$help[$i]['id'];
					$sql="select * from `".$oto."_help_order_user` where `help_id`='{$wb_id}' and isDel='0'";
					$result=$db->query($sql);
					while($row=$result->fetch_assoc()){
						$order_user[$i][]=$row;//将取得的所有数据赋值给person_info数组
					}
					if(isset($order_user[$i]))
					{
						$help[$i]['jiedan_num']=count($order_user[$i]);
					}
					else $help[$i]['jiedan_num']=0;
					$help[$i]['img_list']=explode("|",$help[$i]['img']);
					$help[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
				}
			}
		}
		else if($sousuo_way==1 || $sousuo_way=='1')
		{
			$sql="select * from `".$oto."_posts` where `status`='1' and isDel='0' and (title LIKE '%$text%' or content LIKE '%$text%') order by ftime DESC";
			$result=$db->query($sql);
			while($row=$result->fetch_assoc()){
				$help[]=$row;//将取得的所有数据赋值给person_info数组
			}
		}
		else if($sousuo_way==2 || $sousuo_way=='2')
		{
			$sql="select * from `".$oto."_shops` where `shopFlag`='1' and `shopStatus`='1' and `shopName` LIKE '%$text%' order by createTime DESC";
			$result=$db->query($sql);
			while($row=$result->fetch_assoc()){
				$shop_info[]=$row;//将取得的所有数据赋值给person_info数组
			}
			if(isset($shop_info))
			{
				for($i=0;$i<count($shop_info);$i++)
				{
					$lat2=$shop_info[$i]['latitude'];
					$lon2=$shop_info[$i]['longitude'];
					$shop_info[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
					if($shop_info[$i]['juli']>$fanwei)
					{
						array_splice($shop_info, $i, 1);
						$i--;
					}
				}
				for($i=0;$i<count($shop_info);$i++)
				{
					$shop_id=$shop_info[$i]['shopId'];
					$sql="select * from `".$oto."_goods` where `isSale`=1 and shopId='{$shop_id}' and goodsFlag='1'";
					$result=$db->query($sql);
					while($row=$result->fetch_assoc()){
						$shop_goods_list[$i][]=$row;//将取得的所有数据赋值给person_info数组
					}
					$time_star=time();
					$time_end=$time_star-3600*24*30;
					$time_star1=date('Y-m-d H:i:s',$time_end);
					$time_end1=date('Y-m-d H:i:s',$time_star);
					$shop_sum=0;
					if(isset($shop_goods_list[$i]))
					{
						for($j=0;$j<count($shop_goods_list[$i]);$j++)
						{
							$goods_id=$shop_goods_list[$i][$j]['goodsId'];
							$sql="select * from `".$oto."_orders`,`".$oto."_order_goods` where ".$oto."_orders.orderId=".$oto."_order_goods.orderId and ".$oto."_order_goods.goodsId='{$goods_id}' and createTime>='$time_star1' and createTime<='$time_end1' and orderStatus='4'";
							$result=$db->query($sql);
							while($row=$result->fetch_assoc()){
								$order_list[$i][$j][]=$row;//将取得的所有数据赋值给person_info数组
							}
							$sum=0;
							if(isset($order_list[$i][$j]))
							{
								for($k=0;$k<count($order_list[$i][$j]);$k++)
								{
									$sum+=$order_list[$i][$j][$k]['goodsNums'];
								}
							}
							else $sum=0;
							$shop_sum+=$sum;
						}
					}
					else $shop_sum=0;
					$shop_info[$i]['yue_xiaoliang']=$shop_sum;
					$sql="SELECT a.serviceScore,a.timeScore,a.content,u.userName,a.anonymity,a.createTime FROM ".$oto."_goods_appraises as a,".$oto."_users as u where a.shopId='{$shop_id}' and a.goodsId='0' and a.isDel='0' and a.isShow='1' and a.userId=u.userId order by a.createTime DESC ";
					$result=$db->query($sql);
					while($row=$result->fetch_assoc()){
						$appraises[$i][]=$row;//将取得的所有数据赋值给person_info数组
					}
					if(isset($appraises[$i]))
					{
						$ars1=0;
						$ars2=0;
						$ars_max=count($appraises[$i]);
						for($j=0;$j<$ars_max;$j++)
						{
							$ars1+=$appraises[$i][$j]['serviceScore'];
							$ars2+=$appraises[$i][$j]['timeScore'];
						}
						$shop_info[$i]['score']=round(($ars1/$ars_max+$ars2/$ars_max)/2,1);
					}
					else
					{
						$shop_info[$i]['score']=5;
					}
				}
				$help=$shop_info;
			}
		}
		else if($sousuo_way==3 || $sousuo_way=='3')
		{
			$sql="select * from `".$oto."_goods` where `isSale`=1 and goodsFlag='1' and goodsStock>0 and goodsStatus='1' and goodsName LIKE '%$text%' order by shopId DESC";
			$result=$db->query($sql);
			while($row=$result->fetch_assoc()){
				$goods[]=$row;//将取得的所有数据赋值给person_info数组
			}
			if(isset($goods))
			{
				$t=0;
				$lin_shopId=-1;
				for($i=0;$i<count($goods);$i++)
				{
					if($lin_shopId!=$goods[$i]['shopId'])
					{
						$shopId_list[$t]['shopId']=$goods[$i]['shopId'];
						$lin_shopId=$goods[$i]['shopId'];
						$t++;
					}
				}
				$t=0;
				for($i=0;$i<count($shopId_list);$i++)
				{
					$shop_id=$shopId_list[$i]['shopId'];
					$sql="select * from `".$oto."_shops` where `shopFlag`='1' and `shopStatus`='1' and shopId='$shop_id' order by createTime DESC";
					$result=$db->query($sql);
					while($row=$result->fetch_assoc()){
						$shop_info_lin[$i]=$row;//将取得的所有数据赋值给person_info数组
					}
					if(isset($shop_info_lin[$i]))
					{
						$shop_info[$t]=$shop_info_lin[$i];
						$t++;
					}
				}
				if(isset($shop_info))
				{
					for($i=0;$i<count($shop_info);$i++)
					{
						$lat2=$shop_info[$i]['latitude'];
						$lon2=$shop_info[$i]['longitude'];
						$shop_info[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
						if($shop_info[$i]['juli']>$fanwei)
						{
							array_splice($shop_info, $i, 1);
							$i--;
						}
					}
					for($i=0;$i<count($shop_info);$i++)
					{
						$shop_id=$shop_info[$i]['shopId'];
						$sql="select * from `".$oto."_goods` where `isSale`=1 and shopId='{$shop_id}' and goodsFlag='1'";
						$result=$db->query($sql);
						while($row=$result->fetch_assoc()){
							$shop_goods_list[$i][]=$row;//将取得的所有数据赋值给person_info数组
						}
						$time_star=time();
						$time_end=$time_star-3600*24*30;
						$time_star1=date('Y-m-d H:i:s',$time_end);
						$time_end1=date('Y-m-d H:i:s',$time_star);
						$shop_sum=0;
						if(isset($shop_goods_list[$i]))
						{
							for($j=0;$j<count($shop_goods_list[$i]);$j++)
							{
								$goods_id=$shop_goods_list[$i][$j]['goodsId'];
								$sql="select * from `".$oto."_orders`,`".$oto."_order_goods` where ".$oto."_orders.orderId=".$oto."_order_goods.orderId and ".$oto."_order_goods.goodsId='{$goods_id}' and createTime>='$time_star1' and createTime<='$time_end1' and orderStatus='4'";
								$result=$db->query($sql);
								while($row=$result->fetch_assoc()){
									$order_list[$i][$j][]=$row;//将取得的所有数据赋值给person_info数组
								}
								$sum=0;
								if(isset($order_list[$i][$j]))
								{
									for($k=0;$k<count($order_list[$i][$j]);$k++)
									{
										$sum+=$order_list[$i][$j][$k]['goodsNums'];
									}
								}
								else $sum=0;
								$shop_sum+=$sum;
							}
						}
						else $shop_sum=0;
						$shop_info[$i]['yue_xiaoliang']=$shop_sum;
						$sql="SELECT a.serviceScore,a.timeScore,a.content,u.userName,a.anonymity,a.createTime FROM ".$oto."_goods_appraises as a,".$oto."_users as u where a.shopId='{$shop_id}' and a.goodsId='0' and a.isDel='0' and a.isShow='1' and a.userId=u.userId order by a.createTime DESC ";
						$result=$db->query($sql);
						while($row=$result->fetch_assoc()){
							$appraises[$i][]=$row;//将取得的所有数据赋值给person_info数组
						}
						if(isset($appraises[$i]))
						{
							$ars1=0;
							$ars2=0;
							$ars_max=count($appraises[$i]);
							for($j=0;$j<$ars_max;$j++)
							{
								$ars1+=$appraises[$i][$j]['serviceScore'];
								$ars2+=$appraises[$i][$j]['timeScore'];
							}
							$shop_info[$i]['score']=($ars1/$ars_max+$ars2/$ars_max)/2;
						}
						else
						{
							$shop_info[$i]['score']=5;
						}
						$t=0;
						for($j=0;$j<count($goods);$j++)
						{
							if($shop_id==$goods[$j]['shopId'])
							{
								$shop_info[$i]['list'][$t]['goodsName']=$goods[$j]['goodsName'];
								$shop_info[$i]['list'][$t]['goodsId']=$goods[$j]['goodsId'];
								$t++;
							}
						}
						if($t==0) $shop_info[$i]['list']=[];
					}
					$help=$shop_info;
				}
			}
		}
		if(isset($help)) echo json_encode($help);
		else echo '[]';
		//else echo 'null';
?>
<?php
require('config.php');
function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
	{ 
	 	$EARTH_RADIUS=6378.137; 
	 	$PI=3.1415926; 
	 	$radLat1 = $lat1 * $PI / 180.0;
	 	$radLat2 = $lat2 * $PI / 180.0;
	 	$a = $radLat1 - $radLat2; 
	 	$b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
	 	$s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	 	$s = $s * $EARTH_RADIUS; 
	 	$s = round($s * 1000); 
	 	if ($len_type > 1) 
	 	{ 
	 		$s /= 1000; 
	 	} 
	 	return round($s,$decimal); 
	}
	$sql="select * from ".$oto."_sys_configs where fieldName='店铺附近范围'";
    $result=$db->query($sql);
	while($row=$result->fetch_assoc()){
		$sys_configs[]=$row;//将取得的所有数据赋值给person_info数组
	}
	if(isset($sys_configs))
	{
		$fanwei=$sys_configs[0]['fieldCode'];
	}
	else $fanwei=200000;//搜索附近的范围，单位m
	function fget_dianpu_num($lei)
	{
		global $oto,$db,$fanwei,$lon,$lat;
        	$sql="select * from ".$oto."_goods_cats where catName='{$lei}' and catFlag='1'";
        	$result=$db->query($sql);
			while($row=$result->fetch_assoc()){
				$cat_info[]=$row;//将取得的所有数据赋值给person_info数组
			}
			if(!isset($cat_info))
			{
				$cat_id=0;
			}
			else
			{
				$cat_id=$cat_info[0]['catId'];
			}
			$sql="select * from `".$oto."_shops` where `shopFlag`='1' and `shopStatus`='1' and (goodsCatId1='{$cat_id}' or goodsCatId2='{$cat_id}' or goodsCatId3='{$cat_id}') ";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$shop_info[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($shop_info))
		{
			for($i=0;$i<count($shop_info);$i++)
			{
				$lat2=$shop_info[$i]['latitude'];
				$lon2=$shop_info[$i]['longitude'];
				$shop_info[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
				if($shop_info[$i]['juli']>$fanwei)
				{
					array_splice($shop_info, $i, 1);
					$i--;
				}
			}
			return count($shop_info);
		}
		else
		{
			return 0;
		}
	}
		$n=$_REQUEST['n'];
		$lon='0';
		$lat='0';
		if(isset($_REQUEST['lon'])) $lon=$_REQUEST['lon'];
		if(isset($_REQUEST['lat'])) $lat=$_REQUEST['lat'];
		$sql="select * from `".$oto."_goods_cats` where catFlag=1";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$list[]=$row;//将取得的所有数据赋值给person_info数组
		}
		$yiji_num=0;
		for($i=0;$i<count($list);$i++)
		{
			if($list[$i]['parentId']==0 && $list[$i]['isFixed']==$n)
			{
				$data[$yiji_num]=$list[$i];
				if($lon!='0')
				{
					$data[$yiji_num]['shop_num']=fget_dianpu_num($data[$yiji_num]['catName']);
				}
				else $data[$yiji_num]['shop_num']=0;
				$erji_num=0;
				for($j=0;$j<count($list);$j++)
				{
					if($list[$i]['catId']==$list[$j]['parentId'])
					{
						$data[$yiji_num]['list'][$erji_num]=$list[$j];
						if($lon!='0')
						{
							$data[$yiji_num]['list'][$erji_num]['shop_num']=fget_dianpu_num($data[$yiji_num]['list'][$erji_num]['catName']);
						}
						else $data[$yiji_num]['list'][$erji_num]['shop_num']=0;
						$sanji_num=0;
						for($k=0;$k<count($list);$k++)
						{
							if($list[$j]['catId']==$list[$k]['parentId'])
							{
								$data[$yiji_num]['list'][$erji_num]['list'][$sanji_num]=$list[$k];
								$sanji_num++;
							}
						}
						if($sanji_num==0) $data[$yiji_num]['list'][$erji_num]['list']=[];
						$erji_num++;
					}
				}
				if($erji_num==0) $data[$yiji_num]['list']=[];
				$yiji_num++;
			}
		}
		//print_r($data);
		echo json_encode($data);
?>
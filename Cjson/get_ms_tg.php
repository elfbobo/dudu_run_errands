<?php
require 'config.php';
$time = time();
$type = $_REQUEST['type'];
$goodsId = $_REQUEST['goodsId'];
if ($type == 'tg') {
    $sql = "select  b.shopId,b.goodsId,b.groupPrice,b.groupMaxCount,b.startTime,b.endTime,a.goodsImg,a.goodsName,s.shopName,s.deliveryFreeMoney,s.deliveryMoney,s.deliveryCostTime,,s.deliveryType,s.serviceEndTime from `" . $oto . "_goods` as a,`" . $oto . "_goods_group` as b," . $oto . "_shops as s where a.isGroup='1' and a.goodsId=b.goodsId and a.shopId=b.shopId and b.goodsGroupStatus='1' and startTime<='{$time}' and endTime>='{$time}' and a.goodsFlag='1' and a.goodsStatus='1' and b.groupStatus='1' and b.goodsId='{$goodsId}' and b.shopId=s.shopId";
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()) {
        $goods_info[] = $row; //将取得的所有数据赋值给person_info数组
    }
    if (isset($goods_info)) {
        for ($i = 0; $i < count($goods_info); $i++) {
            $goodsId = $goods_info[$i]['goodsId'];
            $time_star = $goods_info[$i]['startTime'];
            $time_end = $goods_info[$i]['endTime'];
            $sql = "select o.orderId,g.goodsNums from `" . $oto . "_orders` as o,`" . $oto . "_order_goods` as g where o.orderId=g.orderId and o.orderType='3' and g.goodsId='{$goodsId}' and o.orderStatus in ('0','1','2','3','4') and o.paytime>='$time_star' and o.paytime<='$time_end' ";
            $result = $db->query($sql);
            while ($row = $result->fetch_assoc()) {
                $order[$i][] = $row; //将取得的所有数据赋值给person_info数组
            }
            $sum = 0;
            if (isset($order[$i])) {
                for ($j = 0; $j < count($order[$i]); $j++) {
                    $sum += $order[$i][$j]['goodsNums'];
                }
            }
            $goods_info[$i]['yimai'] = $sum;
            $goods_info[$i]['time'] = $time;
            $goods_info[$i]['shopPrice'] = $goods_info[$i]['groupPrice'];
        }
        $data[0]['goodsName'] = $goods_info[0]['goodsName'];
        $data[0]['shopPrice'] = $goods_info[0]['shopPrice'];
        $data[0]['shopName'] = $goods_info[0]['shopName'];
        $data[0]['shopId'] = $goods_info[0]['shopId'];
        $data[0]['deliveryFreeMoney'] = $goods_info[0]['deliveryFreeMoney'];
        $data[0]['deliveryMoney'] = $goods_info[0]['deliveryMoney'];
        $data[0]['deliveryCostTime'] = $goods_info[0]['deliveryCostTime'];
        $data[0]['serviceEndTime'] = $goods_info[0]['serviceEndTime'];
        $data[0]['time'] = $goods_info[0]['time'];
        $data[0]['endTime'] = $goods_info[0]['endTime'];
        echo json_encode($data);
    } else {
        echo '[]';
    }

} else if ($type == 'ms') {
    $t1 = strtotime(date('Y-m-d 00:00:00', $time));
    $t2 = strtotime(date('Y-m-d 08:00:00', $time));
    $t3 = strtotime(date('Y-m-d 12:00:00', $time));
    $t4 = strtotime(date('Y-m-d 16:00:00', $time));
    $t5 = strtotime(date('Y-m-d 20:00:00', $time));
    $t6 = strtotime(date('Y-m-d 24:00:00', $time));
    //$data[0]['pan']=-1;
    if ($time < $t2 && $time >= $t1) {
        $pan = 1;
        $stat_time = $t1;
        $end_time = $t2;
    }
    if ($time < $t3 && $time >= $t2) {
        $pan = 2;
        $stat_time = $t2;
        $end_time = $t3;
    }
    if ($time < $t4 && $time >= $t3) {
        $pan = 3;
        $stat_time = $t3;
        $end_time = $t4;
    }
    if ($time < $t5 && $time >= $t4) {
        $pan = 4;
        $stat_time = $t4;
        $end_time = $t5;
    }
    if ($time < $t6 && $time >= $t5) {
        $pan = 5;
        $stat_time = $t5;
        $end_time = $t6;
    }
    $user_id = $_REQUEST['user_id'];
    $pan2 = $pan - 1;
    //$sql="select * from ".$oto."_goods,".$oto."_goods_seckill where ".$oto."_goods.isSeckill='1' and ".$oto."_goods.goodsId=".$oto."_goods_seckill.goodsId and ".$oto."_goods.shopId=".$oto."_goods_seckill.shopId and ".$oto."_goods_seckill.goodsSeckillStatus='1' and seckillStartTime>='{$stat_time}' and seckillEndTime<='{$end_time}' order by id DESC";
    $sql = "select g.*,a.*,s.shopName,s.deliveryFreeMoney,s.deliveryMoney,s.deliveryCostTime,,s.deliveryType,s.serviceEndTime,s.shopId from " . $oto . "_goods as g," . $oto . "_goods_seckill as a," . $oto . "_shops as s where g.isSeckill='1' and g.goodsId=a.goodsId and g.shopId=a.shopId and g.shopId=s.shopId and a.goodsSeckillStatus='1' and seckillStartTime<='{$time}' and seckillEndTime>'{$time}' and seckillSetTime='{$pan2}' order by id DESC";
    //file_put_contents("tsxx.txt", "\r\n".$sql."\r\n", FILE_APPEND);
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()) {
        $goods_info[] = $row; //将取得的所有数据赋值给person_info数组
    }
    if (isset($goods_info)) {
        for ($i = 0; $i < count($goods_info); $i++) {
            $goods_id = $goods_info[$i]['goodsId'];
            $seckillStartTime = $goods_info[$i]['seckillStartTime'];
            $seckillEndTime = $goods_info[$i]['seckillEndTime'];
            $sql = "select * from " . $oto . "_orders," . $oto . "_order_goods where " . $oto . "_orders.orderId=" . $oto . "_order_goods.orderId and " . $oto . "_order_goods.goodsId='{$goods_id}' and " . $oto . "_orders.orderType='2' and payTime<'{$seckillEndTime}' and payTime>='{$seckillStartTime}'";
            $result = $db->query($sql);
            //file_put_contents("tsxx.txt", "\r\n1:".$sql."\r\n", FILE_APPEND);
            while ($row = $result->fetch_assoc()) {
                $xiaoliang_info[] = $row; //将取得的所有数据赋值给person_info数组
            }
            $sum = 0;
            $sum2 = 0;
            if (!isset($user_id) || $user_id == 0 || $user_id == '0') {
                $sum2 = 0;
            } else {
                $sql = "select * from " . $oto . "_orders," . $oto . "_order_goods where " . $oto . "_orders.orderId=" . $oto . "_order_goods.orderId and " . $oto . "_order_goods.goodsId='{$goods_id}' and " . $oto . "_orders.orderType='2' and userId='{$user_id}' and payTime<'{$seckillEndTime}' and payTime>='{$seckillStartTime}'";
                $result = $db->query($sql);
                //file_put_contents("tsxx.txt", "\r\n2:".$sql."\r\n", FILE_APPEND);
                while ($row = $result->fetch_assoc()) {
                    $user_xiaoliang_info[] = $row; //将取得的所有数据赋值给person_info数组
                }
                if (!isset($user_xiaoliang_info)) {
                    $sum2 = 0;
                } else {
                    for ($j = 0; $j < count($user_xiaoliang_info); $j++) {
                        $sum2 += $user_xiaoliang_info[$j]['goodsNums'];
                    }
                }
            }
            if (!isset($xiaoliang_info)) {
                $sum = 0;
            } else {
                for ($j = 0; $j < count($xiaoliang_info); $j++) {
                    $sum += $xiaoliang_info[$j]['goodsNums'];
                }
            }
            $goods_info[$i]['goods_yimai'] = $sum;
            $goods_info[$i]['user_yimai'] = $sum2;
            $goods_info[$i]['time'] = $time;
        }
        $data[0]['goodsName'] = $goods_info[0]['goodsName'];
        $data[0]['shopPrice'] = $goods_info[0]['seckillPrice'];
        $data[0]['shopName'] = $goods_info[0]['shopName'];
        $data[0]['shopId'] = $goods_info[0]['shopId'];
        $data[0]['deliveryFreeMoney'] = $goods_info[0]['deliveryFreeMoney'];
        $data[0]['deliveryMoney'] = $goods_info[0]['deliveryMoney'];
        $data[0]['deliveryCostTime'] = $goods_info[0]['deliveryCostTime'];
        $data[0]['serviceEndTime'] = $goods_info[0]['serviceEndTime'];
        $data[0]['time'] = $goods_info[0]['time'];
        $data[0]['endTime'] = $end_time;
        echo json_encode($data);
    } else {
        echo '[]';
    }

}

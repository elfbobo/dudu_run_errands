<?php
require 'config.php';

function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
    $lo = gcjBd($lat2, $lng2);
    $lat2 = $lo['lat'];
    $lng2 = $lo['lng'];
    $EARTH_RADIUS = 6378.137;
    $PI = 3.1415926;
    $radLat1 = $lat1 * $PI / 180.0;
    $radLat2 = $lat2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 1000);
    if ($len_type > 1) {
        $s /= 1000;
    }
    return round($s, $decimal);
}

$user_id = $_REQUEST['user_id'];
$shopId = $_REQUEST['shopId'];
//$user_id=1;
$sql = "select * from `" . $oto . "_car_session` where `userId`='" . $user_id . "' LIMIT 1";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $person_info[] = $row; //将取得的所有数据赋值给person_info数组
}

if (!isset($person_info)) {
    echo '[]';
    die();
}
$car_session = unserialize($person_info[0]['car_session']);
if (count($car_session) <= 0) {
    echo '[]';
    die();
}

if (!empty($shopId)) {
    $sql = "SELECT deliveryType,latitude,longitude FROM `{$oto}_shops` WHERE shopId = {$shopId} LIMIT 1";
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()) {
        $shopInfo = $row;
    }
    $ptm = getPtDeliveryConfig();
//计算平台配送费/配送时间
    if ($shopInfo['deliveryType'] == 1 && !empty($ptm)) {
        $sql = "SELECT lng,lat FROM `{$oto}_user_address` WHERE userId = {$user_id} AND isDefault = 1 LIMIT 1";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $addrInfo = $row;
        }
        if (!empty($addrInfo)) {
            $lat = $addrInfo['lat'];
            $lon = $addrInfo['lng'];
            $lat2 = $shopInfo['latitude'];
            $lon2 = $shopInfo['longitude'];
            $shopInfo['juli'] = getDistance($lat, $lon, $lat2, $lon2);
            if ($shopInfo['juli']) {
                for ($j = 0; $j < count($ptm); $j++) {
                    if ($shopInfo['juli'] <= $ptm[$j]['go_distance'] * 1000) {
                        $shopInfo['deliveryMoney'] = (float) $ptm[$j]['go_money'];
                        break;
                    }
                    if ($shopInfo['juli'] > $ptm[$j]['go_distance'] * 1000 && $j == count($ptm) - 1) {
                        $addrInfo = array();
                    }
                }
            }
        }
    }
}

$i = 0;
foreach ($car_session as $k => $v) {
    // if ($shopId != $v['shopId']) {continue;}
    //echo 'goodsName'.$v['goodsName']."<br/>";
    //echo 'goodsId'.$v['goodsId']."<br/>";
    $cart[$i]['goods_id'] = $v['goodsId'];
    $cart[$i]['goods_num'] = $v['cnt'];
    $cart[$i]['isSelected'] = $v['ischk'];
    $cart[$i]['goodsImg'] = $v['goodsThums'];
    $cart[$i]['goodsName'] = $v['goodsName'];
    $cart[$i]['shopPrice'] = $v['shopPrice'];
    $cart[$i]['isGroup'] = $v['isGroup'];
    $cart[$i]['isSeckill'] = $v['isSeckill'];
    $cart[$i]['shopId'] = $v['shopId'];
    $cart[$i]['shopName'] = $v['shopName'];
    $cart[$i]['goods_attr'] = 0;
    $cart[$i]['goodss_attr_id'] = $k;
    $cart[$i]['shopImg'] = 0;
    $cart[$i]['goodsStock'] = 0;
    $cart[$i]['shuxing_money'] = 0;
    $goodsId = $v['goodsId'];
    $sql = "SELECT " . $oto . "_goods.goodsStock," . $oto . "_shops.shopImg," . $oto . "_shops.deliveryFreeMoney," . $oto . "_shops.deliveryMoney," . $oto . "_shops.deliveryStartMoney," . $oto . "_goods.brandId," . $oto . "_goods.goodsCatId1," . $oto . "_goods.goodsCatId2," . $oto . "_goods.goodsCatId3," . $oto . "_goods.shopCatId1," . $oto . "_goods.shopCatId2 FROM " . $oto . "_goods," . $oto . "_shops where " . $oto . "_goods.shopId=" . $oto . "_shops.shopId and " . $oto . "_goods.goodsId='{$goodsId}' LIMIT 1";
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()) {
        $goods_info[$i] = $row; //将取得的所有数据赋值给person_info数组
    }

    $cart[$i]['deliveryFreeMoney'] = $goods_info[$i]['deliveryFreeMoney'];
    $cart[$i]['deliveryMoney'] = $goods_info[$i]['deliveryMoney'];
    $cart[$i]['deliveryStartMoney'] = $goods_info[$i]['deliveryStartMoney'];
    $cart[$i]['brandId'] = $goods_info[$i]['brandId'];
    $cart[$i]['goodsCatId1'] = $goods_info[$i]['goodsCatId1'];
    $cart[$i]['goodsCatId2'] = $goods_info[$i]['goodsCatId2'];
    $cart[$i]['goodsCatId3'] = $goods_info[$i]['goodsCatId3'];
    $cart[$i]['shopCatId1'] = $goods_info[$i]['shopCatId1'];
    $cart[$i]['shopCatId2'] = $goods_info[$i]['shopCatId2'];
    /*获取秒杀限购*/
    if ($cart[$i]['isSeckill'] == 1 || $cart[$i]['isSeckill'] == '1') {
        $time = time();
        $t1 = strtotime(date('Y-m-d 00:00:00', $time));
        $t2 = strtotime(date('Y-m-d 08:00:00', $time));
        $t3 = strtotime(date('Y-m-d 12:00:00', $time));
        $t4 = strtotime(date('Y-m-d 16:00:00', $time));
        $t5 = strtotime(date('Y-m-d 20:00:00', $time));
        $t6 = strtotime(date('Y-m-d 24:00:00', $time));
        $data[0]['pan'] = -1;
        $goods_id = $v['goodsId'];
        if ($time < $t2 && $time >= $t1) {
            $pan = 1;
            $stat_time = $t1;
            $end_time = $t2;
        }
        if ($time < $t3 && $time >= $t2) {
            $pan = 2;
            $stat_time = $t2;
            $end_time = $t3;
        }
        if ($time < $t4 && $time >= $t3) {
            $pan = 3;
            $stat_time = $t3;
            $end_time = $t4;
        }
        if ($time < $t5 && $time >= $t4) {
            $pan = 4;
            $stat_time = $t4;
            $end_time = $t5;
        }
        if ($time < $t6 && $time >= $t5) {
            $pan = 5;
            $stat_time = $t5;
            $end_time = $t6;
        }
        $pan2 = $pan - 1;
        //$sql="select * from ".$oto."_goods,".$oto."_goods_seckill where ".$oto."_goods.isSeckill='1' and ".$oto."_goods.goodsId=".$oto."_goods_seckill.goodsId and ".$oto."_goods.shopId=".$oto."_goods_seckill.shopId and seckillStartTime>='{$stat_time}' and seckillEndTime<='{$end_time}' and ".$oto."_goods.goodsId='{$goods_id}'";
        $sql = "select * from " . $oto . "_goods," . $oto . "_goods_seckill where " . $oto . "_goods.isSeckill='1' and " . $oto . "_goods.goodsId=" . $oto . "_goods_seckill.goodsId and " . $oto . "_goods.shopId=" . $oto . "_goods_seckill.shopId and seckillStartTime<='{$time}' and seckillEndTime>'{$time}' and seckillSetTime='{$pan2}' and " . $oto . "_goods.goodsId='{$goods_id}'";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $miaosha_info[$i][] = $row; //将取得的所有数据赋值给person_info数组
        }
        if (!isset($miaosha_info[$i])) {
            $goods_info[$i]['seckillMaxCount'] = 0;
        } else {
            $goods_info[$i]['shopPrice'] = $miaosha_info[$i][0]['seckillPrice'];
            $goods_info[$i]['seckillMaxCount'] = $miaosha_info[$i][0]['seckillMaxCount'];
        }
    }
    /**/
    $cart[$i]['shopImg'] = $goods_info[$i]['shopImg'];
    if ($cart[$i]['isSeckill'] == 1 || $cart[$i]['isSeckill'] == '1') {
        if ($goods_info[$i]['goodsStock'] < $goods_info[$i]['seckillMaxCount']) {
            $cart[$i]['goodsStock'] = $goods_info[$i]['goodsStock'];
        } else {
            $cart[$i]['goodsStock'] = $goods_info[$i]['seckillMaxCount'];
        }

    } else if ($cart[$i]['isGroup'] == 1 || $cart[$i]['isGroup'] == '1') {
        $goods_id = $v['goodsId'];
        $time = time();
        $sql = "select * from " . $oto . "_goods_group where goodsId='{$goods_id}' and startTime<='$time' and endTime>='$time' and goodsGroupStatus='1'";
        $result = $db->query($sql);
        //file_put_contents("tsxx.txt", "\r\n".$sql."\r\n", FILE_APPEND);
        while ($row = $result->fetch_assoc()) {
            $group_info[$i] = $row; //将取得的所有数据赋值给person_info数组
        }
        if (isset($group_info[$i])) {
            $cart[$i]['goodsStock'] = $group_info[$i]['groupMaxCount'];
        } else {
            $cart[$i]['goodsStock'] = $goods_info[$i]['goodsStock'];
        }

    } else {
        $cart[$i]['goodsStock'] = $goods_info[$i]['goodsStock'];
    }

    $money = 0;
    $goods_attr = '';
    $attr_id = $k;
    $attr_id = explode("_", $attr_id);
    // for($j=0;$j<count($attr_id);$j++)
    // {
    //     echo count($attr_id[$j])."<br/>";
    //     if($attr_id[$j]=="" || $attr_id[$j]==" " || $attr_id[$j]=="undefined") array_splice($attr_id, $j, 1);
    // }
    $lin = $attr_id;
    if (count($attr_id) - 3 == 0) {
        $attr_id = false;
    } else {
        $attr_id = '';
        for ($j = 1; $j < count($lin) - 2; $j++) {
            if ($j == 1) {
                $attr_id .= $lin[$j];
            } else {
                $attr_id .= ',' . $lin[$j];
            }
        }
    }
    if ($attr_id == false || $attr_id == 'false') {
        $money = 0;
        $goods_attr = '';
    } else {
        $z_attr_money;
        $attr_id_list = explode(',', $attr_id);
        $attr_max = count($attr_id_list);
        for ($j = 0; $j < $attr_max; $j++) {
            $z_attr_id = $attr_id_list[$j];
            $sql = "SELECT * FROM " . $oto . "_goods_attributes where id='$z_attr_id'";
            $result = $db->query($sql);
            while ($row = $result->fetch_assoc()) {
                $z_attr_money[$j] = $row; //将取得的所有数据赋值给person_info数组
            }
            $money += $z_attr_money[$j]['attrPrice'];
            if ($j == 0) {
                $goods_attr .= $z_attr_money[$j]['attrVal'];
            } else {
                $goods_attr .= ',' . $z_attr_money[$j]['attrVal'];
            }

            if ($z_attr_money[$j]['attrStock'] < $cart[$i]['goodsStock']) {
                $cart[$i]['goodsStock'] = $z_attr_money[$j]['attrStock'];
            }

        }
    }
    $cart[$i]['shuxing_money'] = $money;
    $cart[$i]['goods_attr'] = $goods_attr;
    $i++;
}
//echo count($car_session);
//print_r($car_session);
//print_r($cart);
echo json_encode($cart);

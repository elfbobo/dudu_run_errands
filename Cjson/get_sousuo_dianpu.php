<?php
require 'config.php';
function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
{
    $lo = gcjBd($lat2, $lng2);
    $lat2 = $lo['lat'];
    $lng2 = $lo['lng'];
    $EARTH_RADIUS = 6378.137;
    $PI = 3.1415926;
    $radLat1 = $lat1 * $PI / 180.0;
    $radLat2 = $lat2 * $PI / 180.0;
    $a = $radLat1 - $radLat2;
    $b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
    $s = $s * $EARTH_RADIUS;
    $s = round($s * 1000);
    if ($len_type > 1) {
        $s /= 1000;
    }
    return round($s, $decimal);
}

$lat = $_REQUEST['lat'];
$lon = $_REQUEST['lon'];
$pan = $_REQUEST['pan'];
$paixu = $_REQUEST['paixu'];
$shaixuan = $_REQUEST['shaixuan'];
$text = $_REQUEST['text'];
$sql = "select * from " . $oto . "_sys_configs where fieldName='店铺附近范围'";
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $sys_configs[] = $row; //将取得的所有数据赋值给person_info数组
}
if (isset($sys_configs)) {
    $fanwei = $sys_configs[0]['fieldCode'];
}
if ($pan == 'fenlei') {
    $sql = "select * from " . $oto . "_goods_cats where catName='{$text}' and catFlag='1'";
    $result = $db->query($sql);
    while ($row = $result->fetch_assoc()) {
        $cat_info[] = $row; //将取得的所有数据赋值给person_info数组
    }
    if (!isset($cat_info)) {
        $cat_id = 0;
    } else {
        $cat_id = $cat_info[0]['catId'];
    }
    $sql = "select * from `" . $oto . "_shops` where `shopFlag`='1' and (goodsCatId1='{$cat_id}' or goodsCatId2='{$cat_id}' or goodsCatId3='{$cat_id}')  and `shopStatus`='1'";
    if ($text == '所有店铺') {
        $sql = "select * from `" . $oto . "_shops` where `shopFlag`='1' and `shopStatus`='1'";
    }

} else if ($pan == 'sousuo') {
    $sql = "select * from `" . $oto . "_shops` where `shopFlag`='1' and `shopStatus`='1' and `shopName` LIKE '%$text%' ";
}

if ($paixu == 'zhineng') {
    $sql .= " order by deliveryCostTime ASC";
} else if ($paixu == 'qisongjia') {
    $sql .= " order by deliveryStartMoney ASC";
}
// else if($paixu=='xiaoliang')
// {
//     $sql.=" order by bizMoney DESC";
// }
$result = $db->query($sql);
while ($row = $result->fetch_assoc()) {
    $shop_info[] = $row; //将取得的所有数据赋值给person_info数组
}
if (isset($shop_info)) {
    //平台配送费
    $ptm = getPtDeliveryConfig();
    for ($i = 0; $i < count($shop_info); $i++) {
        $lat2 = $shop_info[$i]['latitude'];
        $lon2 = $shop_info[$i]['longitude'];
        $shop_info[$i]['juli'] = getDistance($lat, $lon, $lat2, $lon2);
        //计算平台配送费/配送时间
        if ($shop_info[$i]['deliveryType'] == 1 && !empty($ptm)) {
            if ($shop_info[$i]['juli']) {
                for ($j = 0; $j < count($ptm); $j++) {
                    if ($shop_info[$i]['juli'] <= $ptm[$j]['go_distance'] * 1000) {
                        $shop_info[$i]['deliveryCostTime'] = $ptm[$j]['go_distance_time2'];
                        $shop_info[$i]['deliveryMoney'] = $ptm[$j]['go_money'];
                        break;
                    }

                    if ($j == count($ptm) - 1 && $shop_info[$i]['juli'] > $ptm[$j]['go_distance'] * 1000) {
                        unset($shop_info[$i]);
                        break;
                    }
                }
            }
        }

        if ($shop_info[$i]['juli'] < 1000) {
            $shop_info[$i]['juli2'] = $shop_info[$i]['juli'] . "米";
        } else {
            $shop_info[$i]['juli2'] = round($shop_info[$i]['juli'] / 1000, 1) . "KM";
        }

        if ($shop_info[$i]['juli'] > $fanwei) {
            array_splice($shop_info, $i, 1);
            $i--;
        }
    }
    if ($paixu == 'juli') {
        for ($i = 0; $i < count($shop_info); $i++) {
            for ($j = $i; $j < count($shop_info); $j++) {
                if ($shop_info[$j]['juli'] < $shop_info[$i]['juli']) {
                    $t = $shop_info[$i];
                    $shop_info[$i] = $shop_info[$j];
                    $shop_info[$j] = $t;
                }
            }
        }
    }
    for ($i = 0; $i < count($shop_info); $i++) {
        $shop_id = $shop_info[$i]['shopId'];
        $sql = "select * from `" . $oto . "_goods` where `isSale`=1 and shopId='{$shop_id}' and goodsFlag='1'";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $shop_goods_list[$i][] = $row; //将取得的所有数据赋值给person_info数组
        }
        $time_star = time();
        $time_end = $time_star - 3600 * 24 * 30;
        $time_star1 = date('Y-m-d H:i:s', $time_end);
        $time_end1 = date('Y-m-d H:i:s', $time_star);
        $shop_sum = 0;
        if (isset($shop_goods_list[$i])) {
            for ($j = 0; $j < count($shop_goods_list[$i]); $j++) {
                $goods_id = $shop_goods_list[$i][$j]['goodsId'];
                $sql = "select * from `" . $oto . "_orders`,`" . $oto . "_order_goods` where " . $oto . "_orders.orderId=" . $oto . "_order_goods.orderId and " . $oto . "_order_goods.goodsId='{$goods_id}' and createTime>='$time_star1' and createTime<='$time_end1' and orderStatus='4'";
                $result = $db->query($sql);
                while ($row = $result->fetch_assoc()) {
                    $order_list[$i][$j][] = $row; //将取得的所有数据赋值给person_info数组
                }
                $sum = 0;
                if (isset($order_list[$i][$j])) {
                    for ($k = 0; $k < count($order_list[$i][$j]); $k++) {
                        $sum += $order_list[$i][$j][$k]['goodsNums'];
                    }
                } else {
                    $sum = 0;
                }

                $shop_sum += $sum;
            }
        } else {
            $shop_sum = 0;
        }

        $shop_info[$i]['yue_xiaoliang'] = $shop_sum;
        $sql = "SELECT a.serviceScore,a.timeScore,a.content,u.userName,a.anonymity,a.createTime FROM " . $oto . "_goods_appraises as a," . $oto . "_users as u where a.shopId='{$shop_id}' and a.goodsId='0' and a.isDel='0' and a.isShow='1' and a.userId=u.userId order by a.createTime DESC ";
        $result = $db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $appraises[$i][] = $row; //将取得的所有数据赋值给person_info数组
        }

        if (isset($appraises[$i])) {
            $ars1 = 0;
            $ars2 = 0;
            $ars_max = count($appraises[$i]);
            for ($j = 0; $j < $ars_max; $j++) {
                $ars1 += $appraises[$i][$j]['serviceScore'];
                $ars2 += $appraises[$i][$j]['timeScore'];
            }
            $shop_info[$i]['score'] = round(($ars1 / $ars_max + $ars2 / $ars_max) / 2, 1);
        } else {
            $shop_info[$i]['score'] = 5;
        }
    }
    if ($paixu == 'xiaoliang') {
        for ($i = 0; $i < count($shop_info); $i++) {
            for ($j = $i; $j < count($shop_info); $j++) {
                if ($shop_info[$j]['yue_xiaoliang'] > $shop_info[$i]['yue_xiaoliang']) {
                    $t = $shop_info[$i];
                    $shop_info[$i] = $shop_info[$j];
                    $shop_info[$j] = $t;
                }
            }
        }
    }
    echo json_encode($shop_info);
} else {
    echo 'null';
}

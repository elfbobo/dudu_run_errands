# 嘟嘟跑腿

#### 介绍
嘟嘟跑腿项目，是有电商，入驻商，代理商，app电商前端，app端跑腿等功能一整套电商平台。请大家加入QQ群交流：881437201



#### 后端管理结构目录介绍

Application 应用目录
Cjson         app端Api接口目录
Common     公共目录
Conf           配置目录
emotion      表情符号目录
Lang           核心语言包目录
Library        框架类库目录
Mode          框架应用模式目录
pay            支付目录
Public         资源目录
Runtime      运行时目录
Tpl             前端模板目录
Upload       文件上传目录
.htaccess    目录级别的修改配置的方式
admin.php  总管理后端入口
index.php   站点首页
shop.php    商家入口



#### 技术

后端技术
1. 基于Thinkphp框架
2. Json方式提供Api
3. kindeditor
4. bootstrap
5. layer


前端技术

1. bootstrap
2. H5


app电商端技术（如果需要代码，可加群沟通：881437201）
1. 基于第三方apicloud平台打造（www.apicloud.com）
2. 一键生成（苹果，安卓）两种版本
3. H5+源生底层接口

app跑腿端技术（如果需要代码，可加群沟通：881437201）
1. 基于第三方apicloud平台打造（www.apicloud.com）
2. 一键生成（苹果，安卓）两种版本
3. H5+源生底层接口

#### 总后端功能

1. 商品管理
2. 订单管理
3. 提现管理
4. 代理管理
5. 店铺管理
6. 会员管理
7. 优惠活动管理
8. 文章管理
9. 系统管理
10. 商城设置

#### 商家后端功能

1. 订单管理
2. 商品管理
3. 网店设置
4. 优惠活动

#### 代理后端功能
1. 区域代理

#### App电商前端功能
1. 商品
2. 分类
3. 购物车
4. 个人中心

#### App跑腿端功能
1. 待抢单、待取货、配送中、已完成、已取消
2. 个人中心  我的收入、帮助中心
3. 订单管理   配送倒计时、订单状态、收入


#### 系统安装
apache 2.4.9（nginx也可用,版本1.1以上）
mysql  5.5 以上
php    5.6
代码中设置到的***.com域名，替换成自己的域名
设置到微信登录的。 替换自己的微信参数

#### 系统后台截图
![总预览](https://images.gitee.com/uploads/images/2019/0413/230124_9b78160f_448924.png "1.png")
![商品列表](https://images.gitee.com/uploads/images/2019/0413/230159_75d55ca6_448924.png "2.png")
![商品信息](https://images.gitee.com/uploads/images/2019/0413/230217_653c1c10_448924.png "3.png")
![商品属性](https://images.gitee.com/uploads/images/2019/0413/230229_3e3869b1_448924.png "4.png")
![商品管理](https://images.gitee.com/uploads/images/2019/0413/230240_4ea788a2_448924.png "5.png")
![商家订单列表](https://images.gitee.com/uploads/images/2019/0413/230324_70d4ff99_448924.png "6.png")
![商家退款列表](https://images.gitee.com/uploads/images/2019/0413/230335_b08361a0_448924.png "7.png")
![提现列表](https://images.gitee.com/uploads/images/2019/0413/230350_9697f0ac_448924.png "8.png")
![区域代理](https://images.gitee.com/uploads/images/2019/0413/230401_be474546_448924.png "9.png")
![代理提现](https://images.gitee.com/uploads/images/2019/0413/230419_f9e84350_448924.png "10.png")
![店铺分类](https://images.gitee.com/uploads/images/2019/0413/230430_04164114_448924.png "11.png")
![店铺列表](https://images.gitee.com/uploads/images/2019/0413/230441_16351c6e_448924.png "12.png")
![会员列表](https://images.gitee.com/uploads/images/2019/0413/230454_f3cf90a3_448924.png "13.png")
![优惠券](https://images.gitee.com/uploads/images/2019/0413/230507_8d4347e5_448924.png "14.png")
![商城设置](https://images.gitee.com/uploads/images/2019/0413/230525_81fb0701_448924.png "15.png")

#### 跑腿端后台
![系统设置](https://images.gitee.com/uploads/images/2019/0614/121254_6732f8c0_448924.png "pt1.png")
![管理员](https://images.gitee.com/uploads/images/2019/0614/121356_b38fa506_448924.png "pt2.png")
![配送员管理](https://images.gitee.com/uploads/images/2019/0614/121420_40198358_448924.png "pt3.png")
![平台管理](https://images.gitee.com/uploads/images/2019/0614/121441_251fe828_448924.png "pt4.png")
![配送费设置](https://images.gitee.com/uploads/images/2019/0614/121502_9a32d37e_448924.png "pt5.png")
![提现管理](https://images.gitee.com/uploads/images/2019/0614/121534_19146d8a_448924.png "pt6.png")

#### 演示平台
一，超管后台
http://o2oup.dzdiy.cn:8080/admin   体验帐号：system   密码：bj2019

二，商家管理后台
http://o2oup.dzdiy.cn:8080/Shops/login.html  体验帐号：15877777002  密码：234567

三，商家管理跑腿骑手后台
http://ptup.dzdiy.cn:8080/admin   体验帐号：system   密码：bj2019

四，体验App

请大家加入QQ群：881437201。  在群文件当中有体验App

 **提醒：
供个人学习，作者授权后可商用。** 



#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
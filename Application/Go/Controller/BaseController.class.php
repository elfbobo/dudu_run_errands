<?php
namespace Go\Controller;
use Think\Controller;
use Go\Util\JSSDK;

class BaseController extends Controller {

	protected $userinfo = null;
	
	protected $field = array('id','sid','shop_id'
				,'title','title2','title_style',
				'money','yunjiage','zongrenshu','q_uid',
				'canyurenshu','shenyurenshu','qishu',
				'maxqishu','thumb','picarr','codes_table',
				'pos','renqi','isbest','time','q_showtime');
	
	protected function _initialize(){
		define('MOBILE_TPL_PATH', substr(C('VIEW_PATH'), 1).C('DEFAULT_THEME').'/Public');
		//微信分享jssdk
		$wechatConfig = C('WXPAYCONF_PUB');
		$jssdk = new JSSDK($wechatConfig['APPID'], $wechatConfig['APPSECRET']);
		$signPackage = $jssdk->getSignPackage();
		$script = '<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>';
		$script .= "<script type=\"text/javascript\">wx.config({debug: false,appId: '{$signPackage["appId"]}',timestamp: {$signPackage["timestamp"]},nonceStr: '{$signPackage["nonceStr"]}',signature: '{$signPackage["signature"]}',jsApiList: ['checkJsApi','onMenuShareTimeline','onMenuShareAppMessage','onMenuShareQQ','onMenuShareWeibo','onMenuShareQZone','hideMenuItems','showMenuItems','hideAllNonBaseMenuItem','showAllNonBaseMenuItem','translateVoice','startRecord','stopRecord','onVoiceRecordEnd','playVoice','onVoicePlayEnd','pauseVoice','stopVoice','uploadVoice','downloadVoice','chooseImage','previewImage','uploadImage','downloadImage','getNetworkType','openLocation','getLocation','hideOptionMenu','showOptionMenu','closeWindow','scanQRCode','chooseWXPay','openProductSpecificView','addCard','chooseCard','openCard']});</script>";
		$script .= '<script type="text/javascript" src="'.MOBILE_TPL_PATH.'/js/jssdk.js"></script>';
		$this->shareScript = $script;
		
		if (!!$userId = intval(session('oto_userId'))){
			$this->userinfo = M('Users')->where(array('userId' => $userId))->find();
			if (!$this->userinfo) $this->userinfo = null;
			$this->userinfo['money'] = $this->userinfo['userMoney'];
			$this->userinfo['uid'] = $this->userinfo['userId'];
		}
	}
	
}
<?php 
 namespace Admin\Controller;
/**
* 报表统计控制器		
*/
class StatsController extends BaseController{
	//客户统计
	public function tjkf(){
		$this->isLogin();
		$statsModel = D('Stats');
		$object = $statsModel->getUserOrder();
		$this->assign('object',$object);
		$this->display();
	}
	//订单统计
	public function tjdd(){
		$this->isLogin();
		$statsModel = D('Stats');
		$object = $statsModel->getOrders();
		$this->assign('object',$object);
		$this->assign('inpend',date('Y-m-d',time()));
		$this->display();
	}
	//获取订单其他数据
	public function getOrderChart(){
		$this->isAjaxLogin();
		$m = D('Stats');
		$arr = $m->getOrderChart();
		$rs = json_encode($arr);
		$this->assign('inpstart',I('inpstart'));
		$this->assign('inpend',I('inpend'));
		$this->assign('ys1',I('ys1'));
		$this->assign('ys2',I('ys2'));
		$this->assign('ys3',I('ys3'));
		$this->assign('ys4',I('ys4'));
		$this->assign('ys5',I('ys5'));
		$this->ajaxReturn($rs);
	}
	//销售概况
	public function tjgk(){
		$this->isLogin();
		$statsModel = D('Stats');
		$this->assign('inpend',date('Y',time()));
		$this->assign('inpstart',date('Y',time()));
		$this->display();
	}
	//获取订单和销售额走势
	public function getOrderTrend(){
		$this->isAjaxLogin();
		$m = D('Stats');
		$arr = $m->getOrderTrend();
		$rs = json_encode($arr);
		$this->assign('inpstart',I('inpstart'));
		$this->assign('inpend',I('inpend'));
		$this->assign('inpstartMonth',I('inpstartMonth'));
		$this->assign('inpendMonth',I('inpendMonth'));
		$this->ajaxReturn($rs);
	}
	//销售明细
	public function tjmx(){
		$this->isLogin();
		$order = D('Orders');
		$map = "orderStatus = 4 ";
		$page = $_REQUEST['p'] ? $_REQUEST['p'] : 1;
		$pageSize = 20;
		if($_REQUEST['startDate'] && is_array($_POST)){
			$startDate = $_REQUEST['startDate'];
			$endDate = $_REQUEST['endDate'];
			$this->assign('startDate',$startDate);
			$this->assign('endDate',$endDate);
			if($endDate)
			{
				$map.="AND (oto_orders.createTime >= '$startDate' AND oto_orders.createTime < '$endDate')";
			}else{
				$map.="AND (oto_orders.createTime >= '$startDate')";
			}
		}
		$list = $order->OrderDetails($page,$pageSize,$map);
		$count = $order->OrderDetailsCount($map);
		$res = new \Think\Page($count,$pageSize,I());   //调用think分页类
		$pageres = $res->show();

		$this->assign('lists',$list);
		$this->assign('pageres',$pageres);
		$this->display();
	}
	//销售排行
	public function tjph(){
		$this->isLogin();
		$order = D('Orders');
		$map = "orderStatus = 4 ";
		$page = $_REQUEST['p'] ? $_REQUEST['p'] : 1;
		$pageSize = 20;
		if($_REQUEST['startDate'] && is_array($_POST)){
			$startDate = $_REQUEST['startDate'];
			$endDate = $_REQUEST['endDate'];
			$this->assign('startDate',$startDate);
			$this->assign('endDate',$endDate);
			$map.="AND (oto_orders.createTime >= '$startDate' AND oto_orders.createTime < '$endDate')";
		}
		$list = $order->goodsOrder($page,$pageSize,$map);
		$count = $order->goodsOrderCount($map);
		$res = new \Think\Page($count,$pageSize,I());   //调用think分页类
		$pageres = $res->show();

		$this->assign('lists',$list);
		$this->assign('page',$page);
		$this->assign('pageSize',$pageSize);
		$this->assign('pageres',$pageres);
		$this->display();
	}
	//会员排行
	public function tjhy(){
		$this->isLogin();
		$order = D('Orders');
		$map = "orderStatus = 4 ";
		if($_POST['startDate'] && is_array($_POST)){
			$startDate = $_POST['startDate'];
			$endDate = $_POST['endDate'];
			$this->assign('startDate',$startDate);
			$this->assign('endDate',$endDate);
			$map.="AND (oto_orders.createTime >= '$startDate' AND oto_orders.createTime < '$endDate')";
		}
		$page = $_REQUEST['p'] ? $_REQUEST['p'] : 1;
		$pageSize = 20;
		$list = $order->getOrderCountDesc($page,$pageSize,$map);
		$count = $order->getOrderCount($map);
		$res = new \Think\Page($count,$pageSize,I());   //调用think分页类
		$pageres = $res->show();

		$this->assign('lists',$list);
		$this->assign('page',$page);
		$this->assign('pageSize',$pageSize);
		$this->assign('pageres',$pageres);
		$this->display();
	}
	
	//导出数据
	public function outExcel(){
		$this->isLogin();
		$statsModel = D('Stats');
		$data = json_decode($_GET['data'],true);
		$status = $data['status'];
		$startDate = $data['startDate'];
		$endDate = $data['endDate'];
		if(1 == $status){
			$array = array();
			$list = $statsModel->getUserOrder();
			$filename = '客户统计报表';
		}else if(2 == $status){
			$list = array();
			$list[] = unserialize(session('orderStatusModel'));
			$list[] = unserialize(session('deliveryTypeModel'));
			$list[] = unserialize(session('payTypeModel'));
			$filename = '订单统计报表';
		}else if(3 == $status){
			$list = array();
			$categories = array();
			$categories['name'] = '时间';
			$categories['data'] = unserialize(session('categoriesModel'));
			$list[] = $categories;
			$list[] = unserialize(session('orderModel'));
			$list[] = unserialize(session('moneyModel'));
			$filename = '销售概况报表';
		}
		if(4 == $status){
			$order = D('Orders');
			$array = ['商品名称','订单号','数量','售价','售出日期'];
			$list = $order->derivedDetail($startDate,$endDate);
			$filename = '销售明细统计表';
		}
		if(5 == $status){
			$order = D('Orders');
			$array = ['排行','商品名称','货号','销售量','销售额（单位：元）','均价（单位：元）'];
			$list = $order->derivedGoods($startDate,$endDate);
			$filename = '销售排行统计表';
		}
		if(6 == $status){
			$order = D('Orders');
			$array = ['排行','会员名称','订单数单位（个）','购物总金额'];
			$list = $order->derivedOrder($startDate,$endDate);
			$filename = '会员排行统计表';
		}

		//$list = $this->handle($list);
		$this->out($list,$array,$status,$filename);
	}

	/**
	 * 把数据表里的数据处理并返回
	 * @param $arr 从数据表里取得数据
	 * @return array
	 */
	public function handle($arr)
	{
		$list = array();
		foreach($arr as $key=>$val)
		{
			$list[$key] = $val;
			if($list[$key]['shopAtive']==1){
				$list[$key]['shopAtive'] = '营业中';
			}else{
				$list[$key]['shopAtive'] = '休息中';
			}
		}
		return $list;
	}
	/**
	 * 生成A-Z之间的所有英文字母
	 * @return array
	 */
	public function merge()
	{
		$arr = range('A','Z');
		return $arr;
	}
	/**
	 * 生成Excel导出数据
	 * @param $list 字段内容
	 * @param $array 字段名
	 * @param $txstatus 提现状态
	 */
	public function out($list,$array,$status,$filename){
		vendor('PHPExcel');
		vendor('PHPExcel.PHPExcel');
		$objPHPExcel = new \PHPExcel();
		$objProps = $objPHPExcel->getProperties();
		$objPHPExcel->setActiveSheetIndex(0);
		$objActSheet = $objPHPExcel->getActiveSheet();
		$objActSheet->setTitle('Sheet1');
		$objActSheet->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置excel文件默认水平垂直方向居中
		$objActSheet->getDefaultStyle()->getFont()->setSize(14)->setName("微软雅黑");//设置默认字体大小和格式
		$objActSheet->getDefaultRowDimension()->setRowHeight(30);//设置默认行高
		//$arr = $this->merge();
		if(1 == $status){
			$objActSheet->setCellValue('A1','会员购买率');
			$objActSheet->setCellValue('A2','会员总数');
			$objActSheet->setCellValue('B2','有订单会员数');
			$objActSheet->setCellValue('C2','会员订单总数');
			$objActSheet->setCellValue('D2','会员购买率');
			$objActSheet->setCellValue('A3',$list['sumUser']);
			$objActSheet->setCellValue('B3',$list['sumUsersOrder']);
			$objActSheet->setCellValue('C3',$list['sumUserOrders']);
			$objActSheet->setCellValue('D3',$list['orderRate'].'%');
			$objActSheet->setCellValue('A5','每会员平均订单数及购物额');
			$objActSheet->setCellValue('A6','会员购物总额');
			$objActSheet->setCellValue('B6','每会员订单数');
			$objActSheet->setCellValue('C6','每会员购物额');
			$objActSheet->setCellValue('A7','￥'.$list['sumOrdersMoney']);
			$objActSheet->setCellValue('B7',$list['everyUserOrders']);
			$objActSheet->setCellValue('C7','￥'.$list['everyUserMoney']);
			$objActSheet->getColumnDimension('A')->setWidth(25);
			$objActSheet->getColumnDimension('B')->setWidth(20);
			$objActSheet->getColumnDimension('C')->setWidth(20);
			$objActSheet->getColumnDimension('D')->setWidth(20);
		}else if(2 == $status){
			$objActSheet->setCellValue('A1','订单概况');
			$objActSheet->setCellValue('A5','配送方式');
			$objActSheet->setCellValue('A9','支付方式');
			foreach($list as $k=>$v){
				$little = 'A';
				$num1 = 0;$num2 = 0;
				foreach($v as $ks=>$vs){
					$font = $little++;
					if(0 == $k){
						$num1 = 2;$num2 = 3;
					}else if(1 == $k){
						$num1 = 6;$num2 = 7;
					}else if(2 == $k){
						$num1 = 10;$num2 = 11;
					}
					$objActSheet->setCellValue($font.$num1,$vs[0]);
					$objActSheet->setCellValue($font.$num2,$vs[1]);
				}
			}
			$objActSheet->getColumnDimension('A')->setWidth(15);
			$objActSheet->getColumnDimension('B')->setWidth(15);
			$objActSheet->getColumnDimension('C')->setWidth(15);
			$objActSheet->getColumnDimension('D')->setWidth(15);
		}else if(3 == $status){
			$little = 'A';
			foreach($list as $k=>$v){
				$font = $little++;
				$num = 1;
				$objActSheet->setCellValue($font.$num,$v['name']);
				foreach($v['data'] as $ks=>$vs){
					++$num;
					$objActSheet->setCellValue($font.$num,$vs);
				}
			}
			$objActSheet->getColumnDimension('A')->setWidth(15);
			$objActSheet->getColumnDimension('B')->setWidth(15);
			$objActSheet->getColumnDimension('C')->setWidth(15);
		}
		if(4 == $status){
			$az = $this->merge();
			$count = count($array);
			for($i=0;$i<$count;$i++)
			{
				$objActSheet->setCellValue($az[$i].'1',$array[$i]);
			}
			foreach($list as $k=>$v){
				$j=$k+2;
				$objActSheet->setCellValue('A'.$j,$v['goodsName']);
				$objActSheet->setCellValue('B'.$j,$v['orderNo']);
				$objActSheet->setCellValue('C'.$j,$v['goodsNums']);
				$objActSheet->setCellValue('D'.$j,$v['goodsPrice']);
				$objActSheet->setCellValue('E'.$j,$v['createTime']);
			}
		}

		if(5 == $status){
			$az = $this->merge();
			$count = count($array);
			for($i=0;$i<$count;$i++)
			{
				$objActSheet->setCellValue($az[$i].'1',$array[$i]);
			}
			foreach($list as $k=>$v){
				$j=$k+2;
				$objActSheet->setCellValue('A'.$j,$k+1);
				$objActSheet->setCellValue('B'.$j,$v['goodsName']);
				$objActSheet->setCellValue('C'.$j,$v['goodsId']);
				$objActSheet->setCellValue('D'.$j,$v['goodsCount']);
				$objActSheet->setCellValue('E'.$j,$v['sum']);
				$objActSheet->setCellValue('F'.$j,number_format($v['avg'],2));
			}
		}

		if(6 == $status){
			$az = $this->merge();
			$count = count($array);
			for($i=0;$i<$count;$i++)
			{
				$objActSheet->setCellValue($az[$i].'1',$array[$i]);
			}
			foreach ($list as $k=>$v){
				$j=$k+2;
				$objActSheet->setCellValue('A'.$j,$k+1);
				$objActSheet->setCellValue('B'.$j,$v['userName']);
				$objActSheet->setCellValue('C'.$j,$v['orderCount']);
				$objActSheet->setCellValue('D'.$j,$v['money']);
			}
		}

		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		ob_end_clean();
		header("Content-Type: application/vnd.ms-excel;");
		header("Content-Disposition:attachment;filename={$filename}".date('Y-m-d',mktime()).".xls");
		header("Pragma:no-cache");
		header("Expires:0");
		$objWriter->save('php://output');
	}
	
	public function water(){
		echo 'water';
	}
	

}
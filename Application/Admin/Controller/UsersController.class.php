<?php
 namespace Admin\Controller;
/**
*用户管理控制器
*/
class UsersController extends BaseController{
	/**
	 * 跳到新增/编辑页面
	 */
	public function toEdit(){
		$this->isLogin();
	    $m = D('Admin/Users');
    	$object = array();
    	if(I('id',0)>0){
    		$this->checkPrivelege('hylb_02');
    		$object = $m->get();
    		$this->assign('object',$object);
		    $this->view->display('users/edit');
    	}else{
    		$this->checkPrivelege('hylb_01');
    		$object = $m->getModel();
    		$object['userStatus'] = 1;
    		$this->assign('object',$object);
		    $this->view->display('/users/add');
    	}    	
	}
	/**
	 * 新增/修改操作
	 */
	public function edit(){
		$this->isAjaxLogin();
		$m = D('Admin/Users');
    	$rs = array();
    	if(I('id',0)>0){
    		$this->checkAjaxPrivelege('hylb_02');
    		$rs = $m->edit();
    	}else{
    		$this->checkAjaxPrivelege('hylb_01');
    		$rs = $m->insert();
    	}
    	$this->ajaxReturn($rs);
	}
	/**
	 * 删除操作
	 */
	public function del(){
		$this->isAjaxLogin();
		$this->checkAjaxPrivelege('hylb_03');
		$m = D('Admin/Users');
    	$rs = $m->del();
    	$this->ajaxReturn($rs);
	}
   /**
	 * 查看
	 */
	public function toView(){
		$this->isLogin();
		$this->checkPrivelege('hylb_00');
		$m = D('Admin/Users');
		if(I('id')>0){
			$object = $m->get();
			$this->assign('object',$object);
		}
		$this->view->display('/users/view');
	}
	/**
	 * 分页查询
	 */
	public function index(){
		$this->isLogin();
		$this->checkPrivelege('hylb_00');
		$m = D('Admin/Users');
    	$page = $m->queryByPage();
    	$pager = new \Think\Page($page['total'],$page['pageSize']);
    	$page['pager'] = $pager->show();
    	$this->assign('loginName',I('loginName'));
    	$this->assign('userPhone',I('userPhone'));
    	$this->assign('userEmail',I('userEmail'));
    	$this->assign('userType',I('userType',-1));
    	$this->assign('Page',$page);
        $this->display("/users/list");
	}
	/**
	 * 列表查询
	 */
    public function queryByList(){
    	$this->isAjaxLogin();
		$m = D('Admin/Users');
		$list = $m->queryByList();
		$rs = array();
		$rs['status'] = 1;
		$rs['list'] = $list;
		$this->ajaxReturn($rs);
	}
	/**
	 * 查询用户账号
	 */
	public function checkLoginKey(){
		$this->isAjaxLogin();
		$m = D('Admin/Users');
		$key = I('clientid');
	 	$id = I('id',0);
		$rs = $m->checkLoginKey(I($key),$id);
		$this->ajaxReturn($rs);
	}
	
	/**********************************************************************************************
	  *                                             账号管理                                                                                                                              *
	  **********************************************************************************************/
    /**
     * 获取账号分页列表
     */
    public function queryAccountByPage(){
    	$this->isLogin();
    	$this->checkPrivelege('hyzh_00');
		$m = D('Admin/Users');
    	$page = $m->queryAccountByPage();
    	$pager = new \Think\Page($page['total'],$page['pageSize']);
    	$page['pager'] = $pager->show();
    	$this->assign('loginName',I('loginName'));
    	$this->assign('userStatus',I('userStatus',-1));
    	$this->assign('userType',I('userType',-1));
    	$this->assign('Page',$page);
        $this->display("/users/account_list");
	}
	/**
	 * 编辑账号状态
	 */
	public function editUserStatus(){
		$this->isAjaxLogin();
		$this->checkAjaxPrivelege('hyzh_04');
		$m = D('Admin/Users');
		$rs = $m->editUserStatus();
		$this->ajaxReturn($rs);
	}
	/**
	 * 跳到账号编辑状态
	 */
	public function toEditAccount(){
		$this->isLogin();
		$this->checkPrivelege('hyzh_04');
		$m = D('Admin/Users');
		$object = $m->getAccountById();
		$this->assign('object',$object);
		$this->display("/users/edit_account");
	}
	/**
	 * 编辑账号信息
	 */
	public function editAccount(){
		$this->isAjaxLogin();
		$this->checkAjaxPrivelege('hyzh_04');
		$m = D('Admin/Users');
		$rs = $m->editAccount();
		$this->ajaxReturn($rs);
	}

	/**
     * 查询会员的余额
     */
    public function balance()
    {
        $this->isLogin();
        //$this->checkPrivelege('hycz_00');
        $users = D('Users');
        $info = array();
        $info = $users->findByUserId();
       /* if(!$info)
        {
            $this->error('该会员是商家!',U('Users/index'));
        }*/
        $this->assign('object',$info);
        $this->display('users/money');
    }
    /**
     * 操作余额
     */
    public function editMoney()
    {
        $this->isLogin();
        $users = D('Users');
        $info = array();
        $users->updateMoneyByUserId() ? $info['status'] = 1 : $info['status'] = 0;
        $this->ajaxReturn($info);
        exit;
    }

    public function save_upload(){
    	$url = I('imgUrl');
    	$rs = $this->get_image_byurl($url);
    	echo json_encode($rs);
    }

   	// 图片写入本地
	public function get_image_byurl($url,$filename="",$type="") {

		if ($url == "") { return false; }

		$ext = strrchr($url, ".");  //得到图片的扩展名
		$urlInfoArray = explode('/', $url);
		// 图片名称.格式 字符串
		$imgNameAndExt = $urlInfoArray[Count($urlInfoArray)-1];

		if($ext != ".gif" && $ext != ".jpg" && $ext != ".bmp") { $ext = ".jpg"; }

		$savePath = "Upload/idCard/".date('Y-m',time());
		// $savePath = "Upload/goods/2015-11/";
		$savePathFile = iconv("UTF-8", "GBK", $savePath);
        if (!file_exists($savePathFile)){
            mkdir($savePathFile,0777,true);
        }

		if($filename == "") { $filename = $savePath.'/'.$imgNameAndExt; }  //以时间另起名，在此可指定相对目录 ，未指定则表示同php脚本执行的当前目录

		//以流的形式保存图片

		$write_fd = @fopen($filename,"a");
		$img = $this->getImageFileStreamWithUrl($url);
		if(empty($img)){
			return 0;
		}
		@fwrite($write_fd, $img);  //将采集来的远程数据写入本地文件

		@fclose($write_fd);

		// 原图
		// 远程写法
		// $imgUrl ='http://'.$_SERVER['SERVER_NAME'].'/'.$filename;
		// 存本地写法
		$imgUrl = $filename;
        //获取图像信息
        $info = getimagesize('./'.$filename);

        //检测图像合法性
        if(false === $info || (IMAGETYPE_GIF === $info[2] && empty($info['bits']))){
        	return;
        }
	
		$rs['imgUrl'] = $imgUrl;
		// $rs['thumbImgUrl'] = 'http://'.$_SERVER['SERVER_NAME'].'/'.$newsavename;
		return($rs);  //返回文件名
		// return($imgUrl);  //返回文件名

	}

	//远程获取图片
	public function getImageFileStreamWithUrl($url){

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);

		curl_setopt($curl, CURLOPT_HEADER, false);

		//curl_setopt($curl, CURLOPT_REFERER,$url);

		curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; SeaPort/1.2; Windows NT 5.1; SV1; InfoPath.2)");  //模拟浏览器访问

		curl_setopt($curl, CURLOPT_COOKIEJAR, ‘cookie.txt’);

		curl_setopt($curl, CURLOPT_COOKIEFILE, ‘cookie.txt’);

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);

		$values = curl_exec($curl);

		curl_close($curl);

		return($values);

	}
};
?>
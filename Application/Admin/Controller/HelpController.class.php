<?php
 namespace Admin\Controller;
/**
 * 微帮控制器
 */
class HelpController extends BaseController{
	/******************微帮分类**************/
	//分页查询分类列表
	public function helpCate(){
		$this->isLogin();
        $this->checkPrivelege('wblx_00');
		$m = D('Admin/Help');
		$page = $m->getHelpCateList();
		$pager = new \Think\Page($page['total'],$page['pageSize'],I());
    	$page['pager'] = $pager->show();
		$this->assign('Page',$page);
		$this->display('help/cateList');
	}
    //跳转到增加或修改分类页
    public function toEdit(){
        $this->isLogin();
        $m = D('Admin/HelpCate');
        $id = I('id',0);
        $object = array();
        if($id>0){

        }else{
        	$this->checkPrivelege('wblx_01');
        	$object = $m->getModel();
        }
        ;
        $this->assign('object',$object);
        $this->display('help/cateEdit');
    }
	//新增操作
	public function edit(){
		$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblx_01');
		$m = D('Admin/Help');
    	$rs = array();
    	$rs = $m->insert();
    	$this->ajaxReturn($rs);
	}
	//修改微帮类型名称
	public function editCateName(){
		$this->isAjaxLogin;
        $this->checkAjaxPrivelege('wblx_02');
		$m = D('Admin/Help');
		$rs = $m->editCateName();
		$this->ajaxReturn($rs);
	}
	//修改微帮类型排序
	public function editCateSort(){
		$this->isAjaxLogin;
        $this->checkAjaxPrivelege('wblx_02');
		$m = D('Admin/Help');
		$rs = $m->editCateSort();
		$this->ajaxReturn($rs);
	}
	//删除微帮类型
	public function del(){
		$this->isAjaxLogin();
		$this->checkAjaxPrivelege('wblx_03');
		$m = D('Admin/Help');
    	$rs = $m->del();
    	$this->ajaxReturn($rs);
	}
	/******************微帮列表**************/
	public function helpList(){
		$this->isLogin();
        $this->checkPrivelege('wblb_00');
		$m = D('Help');
		$page = $m->queryByPage();
		$pager = new \Think\Page($page['total'],$page['pageSize'],I());
    	$page['pager'] = $pager->show();
    	$this->assign('Page',$page);
    	$this->assign('title',I('title'));
    	$this->assign('content',I('content'));
    	$this->assign('userName',I('userName'));
    	$this->assign('isShow',I('isShow',-1));
        $this->assign('cid',I('cid',-1));
		$this->view->display('help/index');
	}

	 //修改微帮显示状态
	 public function editisShow(){
	 	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblb_02');
	 	$m = D('Help');
		$rs = $m->editisShow();
		$this->ajaxReturn($rs);
	 }
	//查看微帮详情
    public function toDetail(){
    	$this->isLogin();
        $this->checkPrivelege('wblb_00');
    	$m = D('Help');
        $object = $m->toDetail();
        $this->assign('object',$object);
        $this->assign('referer',$_SERVER['HTTP_REFERER']);
        $this->display('help/detail');
    }
    public function delHelp(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblb_03');
    	$m = D('Admin/Help');
    	$rs = $m->delHelp();
    	$this->ajaxReturn($rs);
    }
    //批量删除微帮
    public function  delSelectHelp(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblb_03');
        $res=D('Help')->delSelectHelp();
        $this->ajaxReturn($res);
    }
    
    //批量隐藏微帮
    public function  hideSelectHelp(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblb_02');
        $res=D('Help')->hideSelectHelp();
        $this->ajaxReturn($res);
    }
    //批量显示微帮
    public function  showSelectHelp(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('wblb_02');
        $res=D('Help')->showSelectHelp();
        $this->ajaxReturn($res);
    }
    /********************投诉列表*************************/
    //投诉列表
    public function suggest(){
        $this->isLogin();
        $this->checkPrivelege('wbts_00');
        $su = D('Help');
        $page = $su->getSuggest();
        $pager = new \Think\Page($page['total'],$page['pageSize']);// 实例化分页类 传入总记录数和每页显示的记录数
        $page['pager'] = $pager->show();
        $this->assign('Page',$page);
        $this->assign('list',$data['list']);
        $this->assign('page',$data['show']);
        $this->view->display('help/suggest');
    }
    /**
     * 根据评论ID删除该评分
     */
    public function deleteSuggest()
    {
        $this->isLogin();
        $this->checkAjaxPrivelege('wbts_01');
        $su = D('Help');
        $res = $su->deleteSuggest();
        $this->ajaxReturn($res);
    }
};
?>
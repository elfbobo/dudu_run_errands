<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/1/20
 * Time: 19:48
 */

namespace Admin\Controller;


class ComplainController extends  BaseController
{
    /**
     * 查询记录
     */
    public function index()
    {
        $this->isLogin();
        $complain = D('Complain');
        $data = $complain->queryByPage();
        if($data['root']){
            $this->assign('data',$data);
        }else{
            $data['error'] = '没有用户的投诉!';
            $this->assign('data',$data);
        }
        $this->assign("orderNo",I('post.orderNo'));
        $this->assign("startTime",I('post.startTime'));
        $this->assign("endTime",I('post.endTime'));
        $this->assign("umark","complain");
        $this->view->display('orders/complain');
    }

    /**
     * 根据订单id修改处理状态
     */
    public function editiIsShow(){
        $this->isAjaxLogin();
        //$this->checkAjaxPrivelege('wzlb_02');
        $m = D('Admin/Complain');
        $rs = $m->editiIsShow();
        $this->ajaxReturn($rs);
    }
    /**
     * 批量删除
     */
    public function deletes()
    {
        $this->isLogin();
        $disposable = D("Complain");
        $id = trim(I('post.id','','htmlspecialchars'),',');
        $info =array();
        $disposable->BatchDelete($id)? $info['status'] = 1 :$info['status'] = 0;
        $this->ajaxReturn($info);
    }

    /**
     *根据id删除
     */
    public function del()
    {
        $this->isAjaxLogin();
        $disposable = D("Complain");
        $id = trim(I('post.id','','htmlspecialchars'),',');
        $info =array();
        $disposable->where("id = {$id}")->delete()? $info['status'] = 1 :$info['status'] = 0;
        $this->ajaxReturn($info);
    }

    /**
     * 导出数据
     */
    public function outExcel()
    {
        $this->isLogin();
        $complainModel = D('Complain');
        $data = json_decode($_GET['id'],true);
        $exportStatus = $data['exportStatus'];
        if($exportStatus == 0){
            //所选
            $id = trim($data['id'],',');
        }
        if($exportStatus ==1){
            //全部
            $arr = $complainModel->field('id')->select();
            $map = array();
            foreach($arr as $v){
                $map[] =$v['id'];
            }
            $id = implode(',',$map);
        }
        if($exportStatus ==2){
            //查询
            $id = implode(',',unserialize(cookie('searchesCondition')));
        }
        $array = array('订单号','手机号码','投诉内容','投诉时间');
        $list = array();
        $list = $complainModel->selectBySelecId($id);
        $list = $this->handle($list);
        $this->out($list,$array);
    }

    /**
     * 把数据表里的数据处理并返回
     * @param $arr 从数据表里取得数据
     * @return array
     */
    public function handle($arr)
    {
        $list = array();
        foreach($arr as $key=>$val)
        {
            $list[$key] = $val;
            $list[$key]['time'] = date('Y-m-d H:i:s',$val['time']);
        }

        return $list;
    }
    /**
     * 生成A-Z之间的所有英文字母
     * @return array
     */
    public function merge()
    {
        $arr = range('A','Z');
        return $arr;
    }
    /**
     * 生成Excel导出数据
     * @param $list 字段内容
     * @param $array 字段名
     * @param $txstatus 提现状态
     */
    public function out($list,$array)
    {
        vendor('PHPExcel');
        vendor('PHPExcel.PHPExcel');
        $objPHPExcel = new \PHPExcel();
        $objProps = $objPHPExcel->getProperties();
        $objPHPExcel->setActiveSheetIndex(0);
        $objActSheet = $objPHPExcel->getActiveSheet();
        $objActSheet->setTitle('Sheet1');
        $objActSheet->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//设置excel文件默认水平垂直方向居中
        $objActSheet->getDefaultStyle()->getFont()->setSize(14)->setName("微软雅黑");//设置默认字体大小和格式
        $objActSheet->getDefaultRowDimension()->setRowHeight(30);//设置默认行高
        $objActSheet->getColumnDimension('C')->setWidth(20);//设置宽度
        $objActSheet->getColumnDimension('K')->setWidth(20);
        $arr = $this->merge();
        for($i=1;$i<=count($arr);$i++) {

            $objActSheet->setCellValue($arr[$i - 1].'1', $array[$i - 1]);
        }

        $count = 1;
        $num = 0;
        foreach($list as $key=>$val)
        {
            foreach($val as $k=>$v){
                $num++;
                $number = $arr[$num - 1];
                $font = $count+1;
                $objActSheet->setCellValue($number.$font,$v);
            }
            $num = 0;
            $count++;
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();
        $filename = '订单投诉';

        header("Content-Type: application/vnd.ms-excel;");
        header("Content-Disposition:attachment;filename={$filename}".date('Y-m-d',mktime()).".xls");
        header("Pragma:no-cache");
        header("Expires:0");
        $objWriter->save('php://output');
    }

    /**
     * 查询店铺举报信息
     */
    public function getReport()
    {
        $this->isLogin();
        $complain = D('Complain');
        $data = $complain->getReport();
        if($data['root']){
            $this->assign('data',$data);
        }else{
            $data['error'] = '没有举报信息!';
            $this->assign('data',$data);
        }
        $this->assign("startTime",I('post.startTime'));
        $this->assign("endTime",I('post.endTime'));
        $this->assign("shopName",I('post.shopName'));
        $this->assign("userName",I('post.userName'));
        $this->assign("umark","complain");
        $this->view->display('shops/report');
    }
    /**
     *根据id删除举报
     */
    public function delReport()
    {
        $this->isAjaxLogin();
        $disposable = M("Report");
        $id = trim(I('post.id','','htmlspecialchars'),',');
        $info =array();
        $disposable->where("id = {$id}")->delete()? $info['status'] = 1 :$info['status'] = 0;
        $this->ajaxReturn($info);
    }
    /**
     * 批量删除举报
     */
    public function deletesReport()
    {
        $this->isLogin();
        $disposable = D("Report");
        $id = trim(I('post.id','','htmlspecialchars'),',');
        $info =array();
        $disposable->BatchDelete($id)? $info['status'] = 1 :$info['status'] = 0;
        $this->ajaxReturn($info);
    }
}
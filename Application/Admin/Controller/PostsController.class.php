<?php
 namespace Admin\Controller;
/*
*论坛帖子控制器
*/
class PostsController extends BaseController{
	/****************帖子分类列表*****************/
    public function addCate(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzfl_01');
        $info=D('Posts')->addCate();
        $this->ajaxReturn($info);
    }
    //帖子分类列表
    public function postCate(){
    	$this->isLogin();
        $this->checkPrivelege('tzfl_00');
        $this->info=D('Posts')->getCate();
        $this->display('/posts/cate');
    }   
    public function sortHandle(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzfl_02');
        $info=D('Posts')->sortHandle();
        $this->ajaxReturn($info);
    }  
    public function delCate(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzfl_03');
        $info=D('Posts')->delCate();
        $this->ajaxReturn($info);
    }  
    public  function isShow(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzfl_02');
        $info=D('Posts')->showHandle();
        $this->ajaxReturn($info);
    }
    //跳转到增加分类页
    public function toAddCate(){
        $this->isLogin();
        $this->checkPrivelege('tzfl_01');
        $m = D('Admin/PostsCate');
        $object = array();
        $object = $m->getModel();
        $this->assign('object',$object);
        $this->display('posts/addCate');
    }

    
	/****************帖子列表*****************/
	/**
	 * 查看帖子列表
	 */
	public function postList(){
		$this->isLogin();
        $this->checkPrivelege('tzlb_00');
		$m = D('Posts');
		$page = $m->queryByPage();
		$pager = new \Think\Page($page['total'],$page['pageSize'],I());
    	$page['pager'] = $pager->show();
    	$this->assign('Page',$page);
    	$this->assign('title',I('title'));
    	$this->assign('content',I('content'));
    	$this->assign('loginName',I('loginName'));
    	$this->assign('status',I('status',-1));
        $this->assign('cid',I('cid',-1));
		$this->view->display('/posts/index');
	}
	/**
	 * 修改帖子状态
	 */
	 public function editStatus(){
	 	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzlb_01');
	 	$m = D('Posts');
		$rs = $m->editStatus();
		$this->ajaxReturn($rs);
	 }
	//删除单个帖子
    public function delPosts(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzlb_03');
    	$postsModel = D('Posts');
        $rs = $postsModel->delPosts();
        $this->ajaxReturn($rs);
    }
    //批量删除帖子
    public function  delSelectPosts(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzlb_03');
        $res=D('Posts')->delSelectPosts();
        $this->ajaxReturn($res);
    }
    
    //批量隐藏帖子
    public function  hideSelectPosts(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzlb_01');
        $res=D('Posts')->hideSelectPosts();
        $this->ajaxReturn($res);
    }
    //批量显示帖子
    public function  showSelectPosts(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzlb_01');
        $res=D('Posts')->showSelectPosts();
        $this->ajaxReturn($res);
    }

	/******************帖子详情****************/
	//查看帖子详情
    public function toDetail(){
    	$this->isLogin();
        $this->checkPrivelege('tzlb_02');
    	$postsModel = D('Posts');
        $object = $postsModel->toDetail();
        $pager = new \Think\Page($object['comm']['total'],$object['comm']['pageSize'],I());
    	$object['comm']['pager'] = $pager->show();
        $this->assign('object',$object);
        $this->assign('referer',$_SERVER['HTTP_REFERER']);
        $this->display('/posts/detail');
    }
    //修改评论状态
    public function showHideComment(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_01');
        $rs=D('Posts')->showHideComment();
        $this->ajaxReturn($rs);
    }
    //删除单个评论
    public function delComment(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_02');
        $res=D('Posts')->delComment();
        $this->ajaxReturn($res);
    }
    //批量删除评论
    public function  delSelectComment(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_02');
        $res=D('Posts')->delSelectComment();
        $this->ajaxReturn($res);
    }
    //批量隐藏评论
    public function  hideSelectComment(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_01');
        $res=D('Posts')->hideSelectComment();
        $this->ajaxReturn($res);
    }
    //批量显示评论
    public function  showSelectComment(){
    	$this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_01');
        $res=D('Posts')->showSelectComment();
        $this->ajaxReturn($res);
    }

    /******************评论管理********************/
    //评论列表
    public function postComment(){
        $this->isLogin();
        $this->checkPrivelege('tzpl_00');
        $postsModel = D('Posts');
        $page = $postsModel->Comment();
        $pager = new \Think\Page($page['total'],$page['pageSize'],I());
        $page['pager'] = $pager->show();
        $this->assign('info',$page);
        $this->assign('title',I('title'));
        $this->assign('content',I('content'));
        $this->assign('userName',I('userName'));
        $this->assign('status',I('status',-1));
        $this->display('posts/comment');
    }  
    //修改评论显示状态
    public function editCommentStatus(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzpl_01');
        $res=D('Posts')->editCommentStatus();
        $this->ajaxReturn($res);
    }
    /******************举报管理****************/
    //举报列表
    public function postReport(){
        $this->isLogin();
        $this->checkPrivelege('tzjb_00');
        $postsModel = D('Posts');
        $page = $postsModel->Report();
        $pager = new \Think\Page($page['total'],$page['pageSize'],I());
        $page['pager'] = $pager->show();
        $this->assign('info',$page);
        $this->assign('title',I('title'));
        $this->assign('pcontent',I('pcontent'));
        $this->assign('userName',I('userName'));
        $this->assign('status',I('status',-1));
        $this->assign('icontent',I('icontent',-1));
        $this->display('posts/report');
    }
    //修改举报处理状态
    public function editReportStatus(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzjb_01');
        $res=D('Posts')->editReportStatus();
        $this->ajaxReturn($res);
    }
    //删除举报
    public function delInform(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzjb_02');
        $res=D('Posts')->delInform();
        $this->ajaxReturn($res);
    }
    //批量删除举报
    public function  delSelectInform(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzjb_02');
        $res=D('Posts')->delSelectInform();
        $this->ajaxReturn($res);
    }
    //批量隐藏举报
    public function  hideSelectInform(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzjb_01');
        $res=D('Posts')->hideSelectInform();
        $this->ajaxReturn($res);
    }
    //批量显示举报
    public function  showSelectInform(){
        $this->isAjaxLogin();
        $this->checkAjaxPrivelege('tzjb_01');
        $res=D('Posts')->showSelectInform();
        $this->ajaxReturn($res);
    }
};
?>
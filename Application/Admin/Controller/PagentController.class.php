<?php
namespace Admin\Controller;
use Think\Model;

class PagentController extends BaseController {
	
	public function index(){
		$m = M('area_agent');
		$truename = I('get.truename');
		$userphone = I('get.userphone');
		$province_id = I('get.province');
		$city_id = I('get.city');
		$area_id = I('get.area');
		$map = array();
		if ($truename != '') $map['trueName'] = $truename;
		if ($userphone != '') $map['loginName'] = $userphone;
		if ($province_id != 0) $map['province'] = $province_id;
		if ($city_id != 0) $map['city'] = $city_id;
		if ($area_id != 0) $map['area'] = $area_id;
		
		$page = new \Think\Page($m->where($map)->count('id'),C('PAGE_SIZE'));
		$offset = $page->firstRow.','.$page->listRows;
		$lists = $m->where($map)->limit($offset)->order('time desc')->select();
		$model = new Model(null,null,C('DB_CONFIG2'));
		$province = $model->table(DB_PREFIX.'region')->where(['pid' => 1])->select();
		foreach ($lists as $key => $value){
			$lists[$key]['province_name'] = $model->table(DB_PREFIX.'region')->where(array('id' => $value['province']))->getField('name');
			$lists[$key]['city_name'] = $model->table(DB_PREFIX.'region')->where(array('id' => $value['city']))->getField('name');
			$lists[$key]['area_name'] = $model->table(DB_PREFIX.'region')->where(array('id' => $value['area']))->getField('name');
		}
		$city = '';
		if ($province_id){
			$city = $model->table(DB_PREFIX.'region')->where(['pid' => $province_id])->select();
		}
		if (!empty($city) && $city_id != 0){
			$area = $model->table(DB_PREFIX.'region')->where(['pid' => $city_id])->select();
		}
		$this->lists = $lists;
		$this->page = $page->show();
		$this->truename = $truename;
		$this->userphone = $userphone;
		$this->province = $province;
		$this->city = $city;
		$this->area = $area;
		$this->province_id = $province_id;
		$this->city_id = $city_id;
		$this->area_id = $area_id;
		$this->display();
	}
	
	public function get_child_address(){
		if (!IS_AJAX) $this->error('操作失败');
		$model = new Model(null,null,C('DB_CONFIG2'));
		$data = $model->table(DB_PREFIX.'region')->where(['pid' => I('pid')])->select();
		$html = '';
		// 1省，2市，3区
		$flag = true;
		foreach ($data as $key => $value){
			$html .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
		}
		$this->ajaxReturn($html);
	}
	
	public function toEdit(){
		$model = new Model(null,null,C('DB_CONFIG2'));
		$this->object = M('area_agent')->where(array('id' => intval(I('id'))))->find();
		$province = $model->table(DB_PREFIX.'region')->where(['pid' => 1])->select();
		$city = $model->table(DB_PREFIX.'region')->where(['pid' => $this->object['province']])->select();
		$area = $model->table(DB_PREFIX.'region')->where(['pid' => $this->object['city']])->select();
		$this->province = $province;
		$this->city = $city;
		$this->area = $area;
		if (empty($this->object)) $this->error('操作失败');
		$this->display('add');
	}
	
	public function add(){
		$model = new Model(null,null,C('DB_CONFIG2'));
		$province = $model->table(DB_PREFIX.'region')->where(['pid' => 1])->select();
		$this->province = $province;
		$this->display();
	}
	
	public function edit(){
		if (IS_AJAX){
			$data = array(
				'province' => I('province'),
				'city' => I('city'),
				'area' => I('area'),
				'loginName' => I('loginName'),
				'trueName' => I('trueName'),
				'loginPwd' => I('loginPwd'),
				'time' => time()
			);
			if (intval(I('post.id')) > 0){
				$data['id'] = intval(I('post.id'));
				$user = M('area_agent')->where(array('id' => array('NEQ',$data['id']),'loginName' => $data['loginName']))->find();
				if (!empty($user)){
					$this->ajaxReturn(array('status' => 0,'msg' => '账号已存在'));
				}
				if (!empty($data['loginPwd'])){
					$data['loginPwd'] = md5($data['loginPwd']);
				}else{
					unset($data['loginPwd']);
				}
				if (M('area_agent')->save($data)) $this->ajaxReturn(array('status' => 1));
			}else{
				$user = M('area_agent')->where(array('loginName' => $data['loginName']))->find();
				if (!empty($user)){
					$this->ajaxReturn(array('status' => 0,'msg' => '账号已存在'));
				}
				$data['loginPwd'] = md5($data['loginPwd']);
				if (M('area_agent')->add($data)){
					$this->ajaxReturn(array('status' => 1));
				}
			}
			$this->ajaxReturn(array('status' => 0,'msg' => '操作失败'));
		}
	}
	
	public function deleteArea(){
		$id = intval(I('post.id'));
		if (M('area_agent')->where(['id' => $id])->delete()){
			$this->ajaxReturn(array('status' => 1));
		}
		$this->ajaxReturn(array('status' => 0,'msg' => '操作失败'));
	}
	
	public function infolist(){
		
		if (isset($this->admin['type'])){
			$model = new Model(null,null,C('DB_CONFIG2'));
			$province = $model->table(DB_PREFIX.'region')->where(['id' => $this->admin['province']])->find();
			$city = $model->table(DB_PREFIX.'region')->where(['id' => $this->admin['city']])->find();
			$area = $model->table(DB_PREFIX.'region')->where(['id' => $this->admin['area']])->find();
			$this->assign('address',$province['name'].$city['name'].$area['name']);
		}
		$this->display();
	}
	
	public function gitmoney(){
		$this->edit = true;
		$this->display();
	}
	
	public function agentUnpwd(){
		if (IS_AJAX){
			$info = M('area_agent')->where(array('id' => $this->admin['id']))->find();
			$postData = I('post.');
			if ($info['loginPwd'] != md5($postData['loginPwd'])) $this->ajaxReturn(array('status' => 0,'msg' => '原密码错误'));
			if (empty($postData['ReloginPwd']) || mb_strlen($postData['ReloginPwd'],'utf-8') < 6) $this->ajaxReturn(array('status' => 0,'msg' => '新密码不能为空或小于6位'));
			if (M('area_agent')->where(array('id' => $info['id']))->setField('loginPwd',md5($postData['ReloginPwd']))){
				$this->ajaxReturn(array('status' => 1,'msg' => '修改密码成功'));
			}else{
				$this->ajaxReturn(array('status' => 0,'msg' => '修改密码失败'));
			}
		}else{
			$this->object = M('area_agent')->where(array('id' => $this->admin['id']))->find();
			$this->display('modify');
		}
	}
	
	public function agentApply(){
		$this->display();
	}
	
	public function applyRecord(){
		
		$this->display('gitmoney');
	}
	
}
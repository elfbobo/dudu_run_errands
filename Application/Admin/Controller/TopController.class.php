<?php
 namespace Admin\Controller;
/**
 * 
 */
class TopController extends BaseController{

    public function __construct(){

        parent::__construct();
    }

    public function applyIndex(){
        $m = D('Admin/Top');


        $applyData=$m->applyList();

        $this->assign('applyMin',$applyData['applyMin']);
        unset($applyData['applyMin']);
        $this->assign('applyLog',json_encode($applyData));
        $this->display("Top/applyList");


    }
    
    public function applyItem(){
        $m = D('Admin/Top');


        $res=$m->applyItem();
        
        $this->assign('obj',$res);
        $this->assign('referer',$_SERVER['HTTP_REFERER']);
        $this->display("Top/applyItem");
    }

    public function applyEdit(){
        $m = D('Admin/Top');

        $data=$m->applyCheckEdit();
        $this->ajaxReturn($data);
    }
    public function ajax(){

        $res = D("Admin/Top")->ajax();

        $this->ajaxReturn($res);
    }

};
?>

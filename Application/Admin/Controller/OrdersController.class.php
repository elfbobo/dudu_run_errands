<?php
 namespace Admin\Controller;
use Think\Model;

/**
 * 订单控制器
 */
class OrdersController extends BaseController{
	/**
	 * 分页查询
	 */
	public function index(){
		$this->isLogin();
		$this->checkAjaxPrivelege('ddlb_00');
		//获取地区信息
		$m = D('Admin/Areas');
		$this->assign('areaList',$m->queryShowByList(0));
		
		$m = D('Admin/Orders');
    	$page = $m->queryByPage(0);
    	$pager = new \Think\Page($page['total'],$page['pageSize']);
    	$page['pager'] = $pager->show();
    	$this->assign('Page',$page);
    	$this->assign('shopName',I('shopName'));
    	$this->assign('orderNo',I('orderNo'));
    	$this->assign('areaId1',I('areaId1',0));
    	$this->assign('areaId2',I('areaId2',0));
    	$this->assign('areaId3',I('areaId3',0));
    	$this->assign('orderStatus',I('orderStatus',-9999));
    	$this->assign('isGroup',I('isGroup',0));
		$this->assign('isSeckill',I('isSeckill',0));
        $this->display("/orders/list");
	}

	/**
	 * 平台订单
	 */

	public function PtOrders()
	{
		$this->isLogin();
		$this->checkAjaxPrivelege('ddlb_09');
		//获取地区信息
		$m = D('Admin/Areas');
		$this->assign('areaList',$m->queryShowByList(0));
		
		$m = D('Admin/Orders');
    	$page = $m->queryByPage(1);
    	$pager = new \Think\Page($page['total'],$page['pageSize']);
    	$page['pager'] = $pager->show();
    	$this->assign('Page',$page);
    	$this->assign('shopName',I('shopName'));
    	$this->assign('orderNo',I('orderNo'));
    	$this->assign('areaId1',I('areaId1',0));
    	$this->assign('areaId2',I('areaId2',0));
    	$this->assign('areaId3',I('areaId3',0));
    	$this->assign('orderStatus',I('orderStatus',-9999));
    	$this->assign('isGroup',I('isGroup',0));
		$this->assign('isSeckill',I('isSeckill',0));
        $this->display("/orders/ptlist");
	}

    /**
	 * 退款分页查询
	 */
	public function queryRefundByPage(){
		$this->isLogin();
		$this->checkAjaxPrivelege('tk_00');
		//获取地区信息
		$m = D('Admin/Areas');
		$this->assign('areaList',$m->queryShowByList(0));
		$m = D('Admin/Orders');
    	$page = $m->queryRefundByPage();
    	$pager = new \Think\Page($page['total'],$page['pageSize']);
    	$pager->setConfig('header','件商品');
    	$page['pager'] = $pager->show();
    	$this->assign('Page',$page);
    	$this->assign('shopName',I('shopName'));
    	$this->assign('orderNo',I('orderNo'));
    	$this->assign('isRefund',I('isRefund',-1));
    	$this->assign('areaId1',I('areaId1',0));
    	$this->assign('areaId2',I('areaId2',0));
    	$this->assign('areaId3',I('areaId3',0));
        $this->display("/orders/list_refund");
	}
	/**
	 * 查看订单详情
	 */
	public function toView(){
		$this->isLogin();
		$this->checkPrivelege('ddlb_00');
		$m = D('Admin/Orders');
		if(I('id')>0){
			$object = $m->getDetail();

			$this->assign('object',$object);
		}
		$this->assign('referer',$_SERVER['HTTP_REFERER']);
		$this->display("/orders/view");
	}
    /**
	 * 查看订单详情
	 */
	public function toRefundView(){
		$this->isLogin();
		$this->checkPrivelege('tk_00');
		$m = D('Admin/Orders');
		if(I('id')>0){
			$object = $m->getDetail();
			$this->assign('object',$object);
		}
		$this->assign('referer',$_SERVER['HTTP_REFERER']);
		$this->display("/orders/view");
	}
	/**
	 * 跳到退款页面
	 */
	public function toRefund(){
		$this->isLogin();
		$this->checkPrivelege('tk_04');
		$m = D('Admin/Orders');
	    if(I('id')>0){
			$object = $m->get();

			$this->assign('object',$object);
		}
		$this->display("/orders/refund");
	}
	/**
	 * 退款
	 */
    public function refund(){
		$this->isLogin();
		$this->checkAjaxPrivelege('tk_04');
		$m = D('Admin/Orders');
		$rs = $m->refund();
		$this->ajaxReturn($rs);
	}
	
	
	//用户发布================================
	private $model = null;
	private $connect = null;
	private function db_connect(){
		$this->model = new Model();
		$this->connect = $this->model->db(1,'DB_CONFIG2');
	}
	
	//用户发布订单列表
	public function user_list(){
		$this->db_connect();
		//p($this->connect->table(DB_PREFIX.'admin')->select());
		//echo $this->connect->getLastSql();
		$this->orderNo = I('get.orderNo');
		$this->pay_status = I('get.pay_status');
		$this->order_status = I('get.order_status');
		$this->username = I('get.username');
		$where = '1';
		if ($this->orderNo != '') $where .= " and s.id='{$this->orderNo}'";
		if (isset($_REQUEST['pay_status']) && $this->pay_status != '-9999') $where .= " and s.pay_status='{$this->pay_status}'";
		if (isset($_REQUEST['order_status']) && $this->order_status != '-9999') $where .= " and s.order_status='{$this->order_status}'";
		if ($this->username != '') $where .= " and u.userPhone='{$this->username}'";
		$totalRows = $this->connect->table(DB_PREFIX.'send_order')->count('id');
		$page = new \Think\Page($totalRows);
		$offset = $page->firstRow.','.$page->listRows;
		$lists = $this->connect->table(DB_PREFIX.'send_order as s')->join(C('db_name').'.'.C('db_prefix').'users u ON s.uid=u.userId')->where($where)->field("s.*,u.userName,u.userPhone,u.loginName")->limit($offset)->select();
// 		echo $this->connect->getLastSql();
		foreach ($lists as $key => $value) {
			$lists[$key]['content'] = json_decode($value['content'],true);
		}
		$this->assign('lists',$lists);
		$this->assign('page',$page);
		$this->display();
	}
	
	public function user_order_view(){
		//lo.logType=99
		$this->db_connect();
		$id = I('id');
		if (!$id) $this->error('非法操作');
		$data = $this->connect->table(DB_PREFIX.'send_order')->find();
// 		$sql = "select lo.*,u.loginName,u.userType from __PREFIX__log_orders lo
// 		         left join __PREFIX__users u on lo.logUserId = u.userId and u.userType!=0 and s.userId=u.userId
// 		         where lo.logType=0 and lo.orderId=".$id;
// 		$log = M()->query($sql);
		$data['content'] = json_decode($data['content'],true);
		$this->assign('object',$data);
		$this->deliver_order = $this->connect->table(DB_PREFIX.'delivery_order')->where(array(
			'orderId' => $data['id'],'user_send' => 1
		))->find();
		if (!empty($this->deliver_order)){
			$this->user = $this->connect->table(DB_PREFIX.'user')->where(['id' => $this->deliver_order['userId']])->find();
		}
		$this->display();
	}
	
	//用户发布退款列表
	public function tk_user(){
		$this->db_connect();
		$this->orderNo = I('get.orderNo');
		
		$this->order_status = I('get.order_status');
		$this->username = I('get.username');
		$where = 's.pay_status=2';
		if ($this->orderNo != '') $where .= " and s.id='{$this->orderNo}'";
	
		if (isset($_REQUEST['order_status']) && $this->order_status != '-9999') $where .= " and s.order_status='{$this->order_status}'";
		if ($this->username != '') $where .= " and u.userPhone='{$this->username}'";
		$totalRows = $this->connect->table(DB_PREFIX.'send_order')->count('id');
		$page = new \Think\Page($totalRows);
		$offset = $page->firstRow.','.$page->listRows;
		$lists = $this->connect->table(DB_PREFIX.'send_order as s')->join(C('db_name').'.'.C('db_prefix').'users u ON s.uid=u.userId')->where($where)->field("s.*,u.userName,u.userPhone,u.loginName")->limit($offset)->select();

		$this->assign('lists',$lists);
		$this->assign('page',$page);
		$this->display();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
};
?>
<?php
 namespace Admin\Model;
/**
 * 提现
 */
class TopModel extends BaseModel {



    public function __construct(){
        parent::__construct();
       

    }

    public function applyList(){
        $data=array();

//        $tempdata=M('apply')->select();

        $sql = "select a.*,u.userName  from oto_apply as a left JOIN  oto_users as u on u.userId=a.userId ORDER BY time DESC";

        $tempdata =  M()->query($sql);

        // dump($tempdata);

        foreach($tempdata as $key=>$value){


//            $value['action']="<button class='btn btn-info' id=".$value['userId'].">".'通过'."</button>".'&nbsp;&nbsp;&nbsp;'."<button class='btn btn-success' id=".$value['userId'].">".'处理'."</button>";
            $value['action']=$this->applycheckAction($value['status'],$value['id']);
            $data[$key]['action']=$value['action'];
            $value['item']=$this->applycheckAction(4,$value['id']);
            $data[$key]['item']=$this->applycheckAction(4,$value['id']);

            $value['time']=date("Y-m-d",$value['time']);
            $value['status']=$this->applycheckStatus($value['status']);
            $data[$key]['time']=$value['time'];
            $data[$key]['status']=$value['status'];
            $value['applyType'] = $this->applyTypeAction($value['applyType']);
            $data[$key]['applyType'] = $value['applyType'];

            $data[$key] = $value;
            // dump($value['time']);


        }
        $data['applyMin'] = M("sys_configs")->where(array('fieldCode'=>'applyMin'))->getField('fieldValue');

        // dump($data);


        return $data;


    }


    public function applyCheckStatus($status){

        switch ($status) {
            case 0:
                $status = '待处理';
                break;

            case 1:
                $status =  '处理中';
                break;
            case 2:
                $status =  '通过';
                break;

            case 3:
                $status =  '不通过';
                break;
            default:

                break;


        }

        return $status;


    }


    public function applyTypeAction($status){
        switch ($status) {
            case 0:
                $status = '买家提现';
                break;
            case 1:
                $status =  '商家提现';
                break;
            default:

                break;


        }

        return $status;
    }




    public function applyCheckAction($status,$id){
        $action = '';
        switch ($status){
            case 0:
                $action="<button class='btn btn-info btn-sm' onclick='changeStatus($id,1)'>".'处理'."</button>";
                break;

            case 1:
                $action="<button class='btn btn-success btn-sm' onclick=changeStatus($id,2)>".'完成'."</button>".'&nbsp;&nbsp;&nbsp;'."<button class='btn btn-danger btn-sm' onclick='changeStatus($id,3)'>".'不通过'."</button>";
                break;

            case 2:
                $action="<button class='btn btn-success btn-sm' >".'完成'."</button>";

                break;

            case 3:
                $action="<button class='btn btn-danger btn-sm'>".'不通过'."</button>";
                break;
            case 4:
                $action="<a href=/Admin/Top/applyItem/id/$id class='btn btn-info btn-sm'>".'详情'."</a>";
                break;
            default:

                break;




        }



        return $action;

    }

    public function applyItem(){
        $id = (int)I('id');
        if(!$id){
            return false;
        }

//       $res = M('apply')->field('id,userId,applyType,tel,applyPrice,bankUserName,bankName,bankAccess,status,bankNum,remark,time')->where(array('id'=>$id))->find();
        $sql = "select a.*,u.userName from oto_apply as a left JOIN  oto_users as u on u.userId=a.userId where a.id=$id limit 1";

            $res = $this->queryRow($sql);




        return $res;
    }

    public function applyCheckEdit(){
        $data = false;
        $AGENTAPPLY = M('apply');
        $AGENTAPPLYLOG = M('applyLog');
        $ADMIN = session('WST_STAFF');

        $id = (int)I('post.id');
        $status = (int)I('post.status');

        $tempdata = $AGENTAPPLY->where(array('id' => $id))->data(array('status'=>$status))->save();
        $applyData = $AGENTAPPLY->where(array('id' => $id))->find();
        if ($tempdata) {

            $addData['action'] = $this->applyCheckStatus($status);
            $addData['applyOrderId'] = $id;
            $addData['adminId'] = $ADMIN['staffId'];
            $addData['adminName'] = $ADMIN['loginName'];
            $addData['ip'] = $ADMIN['lastIP'];
            $addData['userName'] = $applyData['loginName'];
            $addData['userId'] = $applyData['userId'];
            $addData['roleName'] = $ADMIN['roleName'];
            $addData['userType'] = $applyData['userType'];
            $addData['addtime'] = time();
            $data = $AGENTAPPLYLOG->add($addData);
        }
        if($tempdata and $status==3 and $applyData['applyType']==0){
            $setBalance=M("users")->where(array('userId'=>$applyData['userId']))->setInc('userMoney',$applyData['applyPrice']);
            $newMoney= M('users')->where(array('userId'=>$applyData['userId']))->getField('userMoney');
            $this->moneyRecord(4,$applyData['applyPrice'],'',1,$applyData['userId'],$newMoney,'买家提现失败:退回余额',3);
        }
        if($tempdata and $status==3 and $applyData['applyType']==1){
            $setBalance=M("shops")->where(array('userId'=>$applyData['userId']))->setInc('bizMoney',$applyData['applyPrice']);
            $newMoney= M('shops')->where(array('userId'=>$applyData['userId']))->getField('bizMoney');
            $this->moneyRecord(7,$applyData['applyPrice'],'',1,$applyData['userId'],$newMoney,'商家提现失败:退回余额',3);
        }
        
        return $data;

    }



    public function ajax()
    {
        $actionType = I('actionType');
        $res = false;

        switch ($actionType) {

            case 'editApplyMin':
                $res = $this->editApplyMin();
                break;
            default:

                break;


        }

        return $res;
    }


    private function editApplyMin()
    {

        $data['fieldValue'] = (int)I("post.applyMin");
        $res = M('sys_configs')->where(array('fieldCode'=>'applyMin'))->data($data)->save();

        return $res;
    }

    // 金额操作记录
    /**
     * 构造函数
     * @param $type 操作类型,1下单，2取消订单，3充值，4提现,5订单无效
     * @param $money 金额
     * @param $orderNo 订单编号或者充值ID
     * @param $IncDec 余额变动 0为减，1加
     * @param $userid 用户ID
     * @param $balance 余额
     * @param $remark 其它备注信息
     */
    private function moneyRecord($type = '', $money = 0, $orderNo = '', $IncDec = '', $userid = 0, $balance = 0,$remark='',$payWay=0) {
        $db = M ( 'money_record' );
        $data ['type'] = $type;
        $data ['money'] = $money;
        $data ['time'] = time ();
        $data ['ip'] = get_client_ip ();
        $data ['orderNo'] = $orderNo;
        $data ['IncDec'] = $IncDec;
        $data ['userid'] = $userid;
        $data ['balance'] = $balance;
        $data ['remark'] = $remark;
        $data ['payWay'] = $payWay;
        $res = $db->add ( $data );
        return $res;
    }

    public function getApplyWait(){

        $res = M("apply")->where(array('status'=>0))->count();


        return $res;
    }

    //获取待处理和处理中的提现数量
    public function queryPenddingTopNum(){
        $sql = "select count(*) counts from oto_apply as a left JOIN  oto_users as u on u.userId=a.userId where a.status in(0,1)";
        $rs =  M()->query($sql);
        $rd['num'] = $rs[0]['counts'];
        return $rd;
    }
};
?>
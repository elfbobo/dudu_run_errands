<?php 
 namespace Admin\Model;
 /**
 * 报表统计模型
 */
 class StatsModel extends BaseModel{
 	//客户统计
 	public function getUserOrder(){
 		$data = array();
 		$usersModel = M('Users');
 		$userData = $usersModel->where("userFlag = 1")->field('userId')->select();
 		//会员总数
 		$data['sumUser'] = count($userData);
 		$ordersModel = M('Orders');
 		$sumUsersOrder = 0;
 		$sumUserOrders = 0;
 		$sumOrdersMoney = 0;
 		foreach($userData as $v){
 			$orderData = $ordersModel->where('userId = '.$v['userId']." and orderFlag=1 and orderStatus = 4")->field('totalMoney')->select();
 			foreach($orderData as $vs){
 				$sumOrdersMoney += $vs['totalMoney'];
 			}
 			$sumOrders = count($orderData);
 			if(0 < $sumOrders){
 				$sumUsersOrder++;
 				$sumUserOrders += $sumOrders;
 			}
 		}
 		//有订单会员数
 		$data['sumUsersOrder'] = $sumUsersOrder;
 		//会员订单总数
 		$data['sumUserOrders'] = $sumUserOrders;
 		//$data['sumUserOrders'] = $ordersModel->where(" orderFlag=1 and orderStatus >=0")->count();
 		//会员购买率
 		$data['orderRate'] = sprintf('%.2f',$sumUsersOrder/$data['sumUser']*100);
 		//会员购买总金额(不含已删除的会员)
 		$data['sumOrdersMoney'] = $sumOrdersMoney;
 		//每会员订单数
 		$data['everyUserOrders'] = sprintf('%.2f',$sumUserOrders/$data['sumUser']);
 		//每会员购物额
 		$data['everyUserMoney'] = sprintf('%.2f',$sumOrdersMoney/$data['sumUser']);
 		return $data;
 	}
 	//订单统计
 	public function getOrders(){
 		$ordersModel = M('Orders');
 		$totalMoney =  $ordersModel->where('orderStatus >= 0 and orderFlag =1 and u.userFlag = 1')->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->sum('totalMoney');
 		return $totalMoney;
 	} 
 	//获取订单其他数据
 	public function getOrderChart(){
 		$ordersModel = M('Orders');
 		$status = (int)I('status',1);//查询方式
 		$dataType = (int)I('dataType',0);//查询数据
 		$ordersData = array();
 		$orderStatusName = array('待处理','已受理','打包中','配送中','已到货');
 		$orderDeliveryName = array('商城配送','物流配送');
 		$payTypeName = array('货到付款','支付宝','微信','余额','积分');
 		if(1 == $status){
 			//按日期查询
 			//查询日期条件
	 		$inpstart = strtotime(I('inpstart'));
	 		$inpend = I('inpend')?(strtotime(I('inpend'))+24*60*60):(time()+24*60*60); 
	 		$inpstartSql = " and unix_timestamp(o.createTime) >= $inpstart ";
	 		$inpendSql = " and unix_timestamp(o.createTime) < $inpend ";
	 		if(0 == $dataType){
	 			//订单概况
		 		for($i=0;$i<5;$i++){
		 			//5种订单状态
		 			$ordersData[$i][] = $orderStatusName[$i];	
		 			$where = 'orderStatus = '.$i.' and orderFlag = 1 ';
		 			if($inpstart!=''){$where .= $inpstartSql;}
		 			if($inpend!=''){$where .= $inpendSql;} 				
		 			$ordersData[$i][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
		 		}
		 		session('orderStatusModel',serialize($ordersData));
	 		}else if(1 == $dataType){
	 			//配送方式
		 		for($i=0;$i<2;$i++){
		 			$ordersData[$i][] = $orderDeliveryName[$i];	
		 			$where = 'orderStatus >= 0  and orderFlag = 1 and deliveryType = '.$i;
		 			if($inpstart!=''){$where .= $inpstartSql;}
		 			if($inpend!=''){$where .= $inpendSql;} 				
		 			$ordersData[$i][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
		 		}
		 		session('deliveryTypeModel',serialize($ordersData));
	 		}else if(2 == $dataType){
	 			//支付方式
		 		for($i=0;$i<5;$i++){
		 			$ordersData[$i][] = $payTypeName[$i];	
		 			$where = 'orderStatus >= 0  and orderFlag = 1 and payType = '.$i;
		 			if($inpstart!=''){$where .= $inpstartSql;}
		 			if($inpend!=''){$where .= $inpendSql;} 				
		 			$ordersData[$i][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
		 		}
		 		session('payTypeModel',serialize($ordersData));
	 		}		
 		}else if(2 == $status){
 			//按年月查询
 			$ys = array(I('ys1'),I('ys2'),I('ys3'),I('ys4'),I('ys5'));
 			$ordersData = array();
 			if(0 == $dataType){
 				//订单概况
 				for($i=0;$i<5;$i++){
		 			$categories[] = $orderStatusName[$i];	
		 			foreach($ys as $k=>$v){
		 				$where = '';
		 				$where = 'orderStatus = '.$i.' and orderFlag = 1 ';
			 			if($v != ''){
							$where .= " and date_format(o.createTime,'%Y-%m') = '".$v."'";
							$ordersData[$k]['name'] = $v;
							$ordersData[$k]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
						}		 				
		 			}
		 		}		
 			}else if(1 == $dataType){
 				//配送方式
 				for($i=0;$i<2;$i++){
		 			$categories[] = $orderDeliveryName[$i];	
		 			foreach($ys as $k=>$v){
		 				$where = '';
		 				$where = 'orderStatus >= 0 and orderFlag = 1 and deliveryType = '.$i;
			 			if($v != ''){
							$where .= " and date_format(o.createTime,'%Y-%m') = '".$v."'";
							$ordersData[$k]['name'] = $v;
							$ordersData[$k]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
						}		 				
		 			}
		 		}
 			}else if(2 == $dataType){
 				//支付方式
 				for($i=0;$i<5;$i++){
		 			$categories[] = $payTypeName[$i];	
		 			foreach($ys as $k=>$v){
		 				$where = '';
		 				$where = 'orderStatus >= 0 and orderFlag = 1 and payType = '.$i;
			 			if($v != ''){
							$where .= " and date_format(o.createTime,'%Y-%m') = '".$v."'";
							$ordersData[$k]['name'] = $v;
							$ordersData[$k]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
						}		 				
		 			}
		 		}	
 			}
 		} 	
 		$data = array();
 		$data['series'] = $ordersData;	
 		$data['categories'] = $categories;		
 		return $data;
 	}
 	//获取订单和销售额走势
 	public function getOrderTrend(){
 		$ordersModel = M('Orders');
 		$status = (int)I('status',1);//查询方式
 		$dataType = (int)I('dataType',0);//查询数据
 		$ordersData = array();
 		$categories = array();
 		if(1 == $status){
 			//年走势
 			$inpstart = I('inpstart');
	 		$inpend = I('inpend');
	 		$gapYear = $inpend - $inpstart;
	 		for($i=0;$i<=$gapYear;$i++){
	 			$categories[] = $inpstart +$i;
	 		}
 			if(0 == $dataType){
 				//订单走势
				foreach($categories as $k=>$v){
					$where = "o.orderFlag = 1 and o.orderStatus = 4 and date_format(o.createTime,'%Y') = ".$v;
					$ordersData[0]['name'] = '订单数（个）';
					$ordersData[0]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
					session('orderModel',serialize($ordersData[0]));
				}
 			}else if(1 == $dataType){
 				//销售额走势
				foreach($categories as $k=>$v){
					$where = "o.orderFlag = 1 and o.orderStatus = 4 and date_format(o.createTime,'%Y') = ".$v;
					$ordersData[0]['name'] = '销售额（元）';
					$ordersData[0]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->sum('totalMoney');
					session('moneyModel',serialize($ordersData[0]));
				}
 			}
 			session('categoriesModel',serialize($categories));		
 		}else if(2 == $status){
 			//月走势
 			$inpstartMonth = strtotime(I('inpstartMonth'));
 			$inpendMonth = strtotime(I('inpendMonth'));
 			$gapMonth = array();
 			$i = 0;
 			while($inpstartMonth <= $inpendMonth){
 				$gapMonth[$i] = trim(date('Y-m',$inpstartMonth),' ');
 				$inpstartMonth += strtotime('+1 month',$inpstartMonth)-$inpstartMonth;
 				$i++;
 			}
 			$categories = $gapMonth;
 			if(0 == $dataType){
 				//订单走势	
 				foreach($categories as $k=>$v){
 					$where = "o.orderFlag = 1 and o.orderStatus = 4 and date_format(o.createTime,'%Y-%m') = '".$v."'";
 					$ordersData[0]['name'] = '订单数（个）';
 					$ordersData[0]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->count();
 					session('orderModel',serialize($ordersData[0]));
 				}
 			}else if(1 == $dataType){
 				//销售额走势
 				foreach($categories as $k=>$v){
					$where = "o.orderFlag = 1 and o.orderStatus = 4 and date_format(o.createTime,'%Y-%m') = '".$v."'";
					$ordersData[0]['name'] = '销售额（元）';
					$ordersData[0]['data'][] = (int)$ordersModel->where($where)->join('as o left join oto_shops as s on s.shopId = o.shopId left join oto_users as u on o.userId = u.userId')->sum('totalMoney');
					session('moneyModel',serialize($ordersData[0]));
				}
 			}
 			session('categoriesModel',serialize($categories));
 		} 	
 		$data = array();
 		$data['series'] = $ordersData;	
 		$data['categories'] = $categories;		
 		return $data;
 	}
 }
 ?>
 
<?php
 namespace Admin\Model;
/**
 * 微帮模型
 */
class HelpModel extends BaseModel {
	/****************微帮分类*****************/
    //分页查询分类列表 
    public function getHelpCateList(){
        $sql = "select * from __PREFIX__help_cate";
        $sql .= " order by cateSort asc ";
        $page = $this->pageQuery($sql);
        return $page;
    }
    //新增分类 
    public function insert(){
        $rd = array('status'=>-1);
        $cateName = I('cateName');
        $m = M('HelpCate');
        if(!$cateName){
            $rd['status'] = -2;return;
        }
        $data['cateName']=$cateName;
        $data['cateSort']=I('cateSort',0);
        $res=$m->add($data);
        if($res!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
    //修改微帮类型名称~
    public function editCateName(){
        $rd = array('status'=>-1);
        $cateName = I('cateName');
        $m = M('HelpCate');
        if(!$cateName){
            $rd['status'] = -2;return;
        }
        $data['cateName']=$cateName;
        $data['id'] = I('id');
        $res=$m->save($data);
        if($res!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
    //修改微帮类型排序
    public function editCateSort(){
        $rd = array('status'=>-1);
        $cateSort = I('cateSort',0);
        $m = M('HelpCate');
        $data['cateSort']=$cateSort;
        $data['id'] = I('id');
        $res=$m->save($data);
        if($res!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
    //删除微帮类型
    public function del(){
        $rd = array('status'=>-1);
        $id = I('id',0);
        $m = M('HelpCate');
        $res = $m->delete($id);
        if($res!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
    /****************微帮列表*****************/
    /**
     * 分页查看微帮列表
     */
     public function queryByPage(){
        $title = I('title');
        $content = I('content');
        $userName = I('userName');
        $isShow = I('isShow',-1);
        $cid = I('cid',-1);
        $sql = "select h.*,hc.cateName,u.userName from __PREFIX__help as h left join __PREFIX__help_cate as hc on h.cid = hc.id left join __PREFIX__users as u on h.userId = u.userId where isDel = 0";
        if($title!=''){$sql .= " and h.title like '%$title%'";}
        if($content!=''){$sql .= " and h.content like '%$content%'";}
        if($userName!=''){$sql .= " and (u.userName like '%$userName%' or u.userName like '%$userName%')";}
        if($isShow>=0){$sql .= " and h.isShow = $isShow";}
        if($cid>=0){$sql .= " and hc.id = $cid";}
        $sql .=" order by id desc";
        $page = $this->pageQuery($sql);
        //获取所有分类
        $m = M('HelpCate');
        $page['helpCate'] = $m->order('cateSort asc')->select();
        return $page;
     }
     //修改微帮显示状态
    public function editisShow(){
        $rd = array('status'=>-1);
        if(I('id',0)==0)return $rd;
        $isShow = (int)I('isShow');
        $id = (int)I('id');
        $this->isShow = ($isShow==1)?1:0;
        $rs = $this->where("id = {$id}")->save();
        if(false !== $rs){
            $rd['status']= 1;
        }
        return $rd;
    } 
    //查看微帮详情
    public function toDetail(){
        $id=I('id');
        $sql = "select h.*,hc.cateName,u.userName from __PREFIX__help as h left join __PREFIX__help_cate as hc on h.cid = hc.id left join __PREFIX__users as u on h.userId = u.userId where h.isDel = 0 and h.id = $id";
        $rd=$this->queryRow($sql);
        //分隔图片
        if($rd['img'] != ''){
            $img = explode('|',$rd['img']);
            $rd['img'] = $img;
        }
        return $rd;
    }
    //删除微帮
    public function delHelp(){
        $rd = array('status'=>-1);
        $id = I('id',0);
        $res = $this->delete($id);
        if($res!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
    //批量删除帖子
    public  function delSelectHelp(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=$this->where("id in({$id})")->setField(array('isDel'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    //批量隐藏帖子
    public  function hideSelectHelp(){
        $id = self::formatIn(",", I('id',0));
        $res=$this->where("id in({$id})")->setField(array('isShow'=>0));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    //批量显示帖子
    public  function showSelectHelp(){
        $id = self::formatIn(",", I('id',0));
        if(!$id){
            return  array('status'=>-1);
        }
        $res=$this->where("id in({$id})")->setField(array('isShow'=>1));
        if($res!=false){
            return  array('status'=>0);
        }else{
            return  array('status'=>-1);
        }
    }
    /*******************************投诉列表*******************/
    /**
     * 获取所有的投诉
     * @return mixed array
     */
    public function getSuggest()
    {
        $sql = "select s.*,u.userName,us.userName object  from __PREFIX__suggest as s left join __PREFIX__users as u on s.userId = u.userId left join __PREFIX__users as us on us.userId = s.object where complaintType = 1 and isDel = 0";
        $info = $this->pageQuery($sql);
        return $info;
    }
    /**
     * 根据评论ID删除该投诉
     */
    public function deleteSuggest()
    {
        $rd = array('status',-1);
        $m = M('Suggest');
        $id = (int)I('id');
        $data = array();
        $data['id'] = $id;
        $data['isDel'] = 1;
        $rs = $m->save($data);
        if($rs!=false){
            $rd['status'] = 1;
        }
        return $rd;
    }
};
?>
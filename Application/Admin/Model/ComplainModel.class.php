<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/1/20
 * Time: 19:48
 */

namespace Admin\Model;


class ComplainModel extends BaseModel
{
    /**
     * 查看所有投诉记录
     * @return mixed array
     */
    public function queryByPage()
    {
        //查询条件
        $orderNo = I('post.orderNo');
        $starttime = strtotime(I('post.startTime'));
        $endtime = strtotime(I('post.endTime'));
        $isHandle = I('post.isHandle');
        /*
        if($isHandle == -1){
            $isHandle = '';
        }
        */
        //if($isHandle!=''){$sql.=" and c.isHandle = {$isHandle}";}
        $sql = "select c.*,o.orderNo from __PREFIX__complain as c left join __PREFIX__orders as o on c.orderId = o.orderId where 1 ";
        if($orderNo!=''){$sql.=" and o.orderNo = {$orderNo}";}
        if($starttime!=''){$sql.=" and c.time > {$starttime}";}
        if($endtime!=''){$sql.=" and c.time < {$endtime}";}
        $sql .= " order by c.id desc";
        $data = $this->pageQuery($sql);
        $arr = $this->query($sql);
        $map = array();
        foreach($arr as $v){
            $map[] =$v['id'];
        }
        //将查询到的id存储到cookie中，方便导出数据
        cookie('searchesCondition',serialize($map));
        /*
        if($isHandle==''){
            $data['isHandle'] = -1;
        }else{
            $data['isHandle'] = $isHandle;
        }
        */
        return $data;
    }

    /**
     * ï¿½ï¿½Ê¾ï¿½ï¿½ï¿½ï¿½ï¿½Ç·ï¿½ï¿½ï¿½Ê¾/ï¿½ï¿½ï¿½ï¿½
     */
    public function editiIsShow(){
        $rd = array('status'=>-1);
        if(I('id',0)==0)return $rd;
        $m = M('Complain');
        $m->isHandle = ((int)I('isHandle')==1)?1:0;
        $rs = $m->where("id =".(int)I('id',0))->save();
        if(false !== $rs){
            $rd['status']= 1;
        }
        return $rd;
    }
    /**
*批量删除
     */
    public function BatchDelete($id)
    {
        $map['id'] = array('in',$id);
        return $this->where($map)->delete();
    }

    /**
     *根据投诉id查询
     * @return mixed array
     */
    public function selectBySelecId($id)
    {
       $map['id'] = array('in',$id);
       return $this->where($map)->join("as c left join oto_orders as o on c.orderId=o.orderId")->field("o.orderNo,c.phone,c.content,c.time")->order('c.time desc')->select();  
    }

    /**
     * 获取待处理的投诉数量
     */
    public function queryPenddingComplainNum(){
        $rd = array('status'=>-1);
        $sql="select count(*) counts from __PREFIX__complain where isHandle = 0";
        $rs = $this->query($sql);
        $rd['num'] = $rs[0]['counts'];
        return $rd;
    }

    /**
     * 查看所有举报记录
     * @return mixed array
     */
    public function getReport()
    {
        $starttime = strtotime(I('post.startTime'));
        $endtime = strtotime(I('post.endTime'));
        $isHandle = I('post.isHandle');
        $shopName = I('post.shopName');
        $userName = I('post.userName');
        $sql = "select c.*,u.userName,s.shopName from __PREFIX__report as c, __PREFIX__users as u,__PREFIX__shops as s where c.userId = u.userId and c.shopId = s.shopId";
        if($shopName!=''){$sql.=" and s.shopName like '%".$shopName."%'";}
        if($userName!=''){$sql.=" and u.userName like '%".$userName."%'";}
        if($starttime!=''){$sql.=" and c.time > {$starttime}";}
        if($endtime!=''){$sql.=" and c.time < {$endtime}";}
        $sql .= " order by c.id desc";
        $data = $this->pageQuery($sql);
        $arr = $this->query($sql);
        $map = array();
        foreach($arr as $v){
            $map[] =$v['id'];
        }
        cookie('searchesCondition',serialize($map));
        return $data;
    }

}
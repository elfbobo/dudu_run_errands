<?php
namespace Home\Model;
/**
 * 用户账户类
 */
class UsersAccountModel extends BaseModel {
	/**
	* 获取用户余额
	*/
	public function getYue($obj){
		$usersModel = M('Users');
		$data = $usersModel->where('userId = '.$obj['userId'])->getField('userMoney');;
		return $data;
	}
	/**
	* 获取用户收支明细
	*/
	public function getPayRecord($obj){
		$type = I('mark');
		$lastTime = I('time');
		if($lastTime==1){
		    $time = strtotime(date('Y-m-d'));
		}elseif($lastTime ==2){
		    $time = time()-7*24*3600;
		}elseif($lastTime == 3){
		    $time = time()-15*24*3600;
		}elseif($lastTime == 4){
		    $time = time()-30*24*3600;
		}elseif($lastTime == 5){
		    $time = time()-182*24*3600;
		}elseif($lastTime == 6){
		    $time = time()-365*24*3600;
		}

		$userId = $obj['userId'];
		$pcurr = (int)I("pcurr");
		//$sql = "select * from __PREFIX__pay_record where userId = ".$userId." and time > ".$time;
		$sql = "select * from __PREFIX__money_record where userId = ".$userId;
		if($obj['pay']!=0){
		    $sql .= " and IncDec = ".$obj['pay'];
		}
		if($type>0){$sql .= " and type = ".$type;}
		if($lastTime>0){$sql .= " and time > ".$time;}
		$sql .= " order by id desc";
		return $this->pageQuery($sql,$pcurr,15);
	}
	/*
	*获取用户积分
	*/
	public function getIntegral($obj){
		$userModel = M('Users');
		$data = $userModel->where('userId = '.$obj['userId'])->getField('userScore');
		return $data;
	}
	/*
    *获取积分兑换记录
    */
    public function exchangeRecord($obj){
        $lastTime = $obj['lastTime'];
        $time = time()-30*24*3600;
        $userId = $obj['userId'];
        $pcurr = (int)I("pcurr");
        $sql = "select sr.*,o.winningCode from __PREFIX__score_record as sr left join __PREFIX__orders as o on o.orderNo = sr.orderNo where sr.userid = ".$userId." and time > ".$time;
        if($obj['pay']!=0){
            $sql .= " and IncDec = ".$obj['pay'];
        }
        $sql .= " order by time desc";
        return $this->pageQuery($sql,$pcurr,15);	
    }
}
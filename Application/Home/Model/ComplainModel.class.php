<?php
 namespace Home\Model;
/**
 * 商品评价服务类
 */
class ComplainModel extends BaseModel {
	 /**
	  * 买家增加投诉
	  */
     public function addComplain($obj){
        $rd = array('status'=>-1);
        $m = M('Complain');
        $data = array();
        $data['shopId'] = $obj['shopId'];
        $data['orderId'] = $obj['orderId'];
        $data['content'] = $obj['content'];
        $data['phone'] = $obj['phone'];
        $data['time'] = time();
        $data['userId'] = $obj['userId'];
        $rs = $m->add($data);
        if($rs){
        	$rd['status'] = 1;
        }		
		return $rd;
	 }
    /**
     * 商家查看所有用户投诉信息
     * @param $id int 商家ID
     * @return mixed array
     */
    public function queryByPage($shopId)
    {
        //查询条件
        $orderNo = I('post.orderNo');
        $starttime = strtotime(I('post.startTime'));
        $endtime = strtotime(I('post.endTime'));
        $isHandle = I('post.isHandle');
        /*
        if($isHandle == -1){
            $isHandle = '';
        }
        */
        //if($isHandle!=''){$sql.=" and c.isHandle = {$isHandle}";}
        $sql = "select c.*,o.orderNo from __PREFIX__complain as c left join __PREFIX__orders as o on c.orderId = o.orderId where c.shopId = ".$shopId." AND c.isFlag = 1";
        if($orderNo!=''){$sql.=" and o.orderNo = {$orderNo}";}
        if($starttime!=''){$sql.=" and c.time > {$starttime}";}
        if($endtime!=''){$sql.=" and c.time < {$endtime}";}
        $sql .= " order by c.id desc";
        $data = $this->pageQuery($sql);
        /*
        if($isHandle==''){
            $data['isHandle'] = -1;
        }else{
            $data['isHandle'] = $isHandle;
        }
        */
        return $data;
    }
    /**
     * 商家删除投诉信息
     * @param $id int 投诉信息ID
     * @param $user_id int 店家ID
     * @return mixed bool
     */
    public function _deleteById()
    {
        $rd = array('status'=>-1);
        $data['id'] = (int)I('id');
        $data['shopId'] = (int)I('shopId');
        $data['isFlag'] = 0;
        $rs = $this->save($data);
        if($rs){
            $rd['status'] = 1;
        }
        return $rd;
    }
    /**
      * 买家增加举报
    */
    public function addReport($obj){
        $rd = array('status'=>-1);
        $m = M('Report');
        $data = array();
        $data['shopId'] = $obj['shopId'];
        $data['content'] = $obj['content'];
        $data['time'] = time();
        $data['userId'] = $obj['userId'];
        $rs = $m->add($data);
        if($rs){
            $rd['status'] = 1;
        }       
        return $rd;
    }
}
?>
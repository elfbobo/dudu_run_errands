<?php
 namespace Home\Model;
/**
 * 商品晒单类
 */
class ShareModel extends BaseModel {
 	/**
	  * 增加晒单分享
	  */
 	public function addShare($obj,$path){
 		$rs['status'] = -1;
 		$m = M('Share');
    	$data = array();
		$data['goodsId'] = $obj['goodsId'];
		$data['shopId'] = $obj['shopId'];
		$data['orderId'] = $obj['orderId'];
		$data['userId'] = $obj['userId'];
		$data['shareTitle'] = I('shareTitle');
		$data['shareContent'] = I('shareContent');
		$data['shareTime'] = date('Y-m-d H:i:s');
		$shareImg = explode('#@#',I('shopAds'));
		for($i=1;$i<=4;$i++){
			$data['shareImg'.$i] = $shareImg[$i-1];
		}
		$data['isBest'] = 1;
		$data['remark'] = 1;
		$rd = $m->add($data);
		if($rd){
			$rs['status'] = 1;
		}
		return $rs;
 	}

 	 /**
	  *商家商品晒单分页列表
	  */
     public function queryByPage($shopId){
        $m = M('share');
        $shopCatId1 = (int)I('shopCatId1',0);
		$shopCatId2 = (int)I('shopCatId2',0);
		$goodsName = I('goodsName');
        $pcurr = (int)I("pcurr",0);
	 	$sql = "select gp.*,g.goodsName,g.goodsThums,u.loginName from 
	 	           __PREFIX__share gp ,__PREFIX__goods g, __PREFIX__users u
	 	           where gp.userId=u.userId and gp.goodsId=g.goodsId and gp.shopId=".$shopId."";
		if($shopCatId1>0)$sql.=" and shopCatId1=".$shopCatId1;
		if($shopCatId2>0)$sql.=" and shopCatId2=".$shopCatId2;
		if($goodsName!='')$sql.=" and (goodsName like '%".$goodsName."%' or goodsSn like '%".$goodsName."%')";
		$sql.=" order by id desc";
	 	$pages = $this->pageQuery($sql,$pcurr);	
		return $pages;
	 }
	/**
	 *商家审核晒单
	 */
    public function changeShareShow(){
	    $m = M('Share');
	    $id = (int)I('id');
	    $isShow = (int)I('isShow',0);
	    $rs = $m->where('id = '.$id)->setField('isShow',$isShow);
	    return $rs;
	}

	/**
	 *获取晒单详情
	 */
	public function getShare($obj){
		$m = M('Share');
		$userId = $obj['userId'];
		$shopId = $obj['shopId'];
		$orderId = $obj['orderId'];
		$goodsId = $obj['goodsId'];
		$rs = $m->where('userId = '.$userId.' and shopId = '.$shopId.' and orderId = '.$orderId.' and goodsId = '.$goodsId)->find();
		return $rs;
	}
	/*
	*买家查看晒单列表
	*/
	public function getShareList($obj){
		$m = M('share');
		$userId = $obj['userId'];
		$pcurr = (int)I("pcurr",0);
		$sql = "select gp.*,g.goodsName,g.goodsThums,u.loginName from 
	 	           __PREFIX__share gp ,__PREFIX__goods g, __PREFIX__users u
	 	           where gp.userId=u.userId and gp.goodsId=g.goodsId and gp.userId=".$userId;
	 	$sql.=" order by id desc";
	 	$pages = $this->pageQuery($sql,$pcurr);
	 	return $pages;
	}
}
<?php
namespace Home\Model;

class YungoModel extends BaseModel {
	/*************************一元云购首页******************************/
	/**
	 * 获取最新揭晓
	 */
	public function getNewAwary(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.q_end_time,u.userName,go.q_user_code,go.canyurenshu,go.thumb,u.userPhoto,u.userId  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on gc.catId = go.mall_cateid left join __PREFIX__users as u on u.userId = go.q_uid where go.q_end_time > 0 and gc.catFlag = 1 order by go.q_end_time desc";
		$rs = $m->query($sql);
		//获取用户购买次数
		foreach ($rs as $k => $v) {
			$sql = "select sum(gonumber) as gonumber from __PREFIX__member_go_record where uid = ".$v['userId']." and shopid = ".$v['id'];
			$rs[$k]['gonumber'] = $m->query($sql)[0]['gonumber'];
		}
		return $rs;
	}
	//获取人气商品
	public function getHotGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		//计算参与人数比例
		foreach($rs as $k=>$v){
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$rs[$k]['scale'] = $scale; 
		}
		return $rs;
	}
	//获取所有分类及商品
	public function getAllGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb,gc.catName,gc.cateImg,gc.catId from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		//分类
		$cateArray = array();
		foreach($rs as $k=>$v){
			//计算参与人数比例
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$v['scale'] = $scale;
			$cateArray[$v['catName']][] = $v;
		}
		return $cateArray;
	}
	//获取最新上架
	public function getNewGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.thumb,go.zongrenshu from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		return $rs;
	}

	/*******************************所有商品********************************/
	public function getGoodsList($obj){
		$communityId = I("communityId");
		$c1Id = (int)I("c1Id",0);
		$c2Id = (int)I("c2Id");
		$c3Id = (int)I("c3Id");
		$pcurr = (int)I("pcurr");
		$msort = (int)I("msort",1);//排序标识
		$prices = I("prices");
		if($prices != ""){
			$pricelist = explode("_",$prices);
		}
		$brandId = I("brandId",0);

		$keyWords = urldecode(I("keyWords"));
		$words = array();
		if($keyWords!=""){
			$words = explode(" ",$keyWords);
		}
		$sql = "SELECT  go.id,go.title,go.thumb,go.money,go.canyurenshu,go.zongrenshu,go.shenyurenshu,go.yunjiage,go.renqi
				FROM __PREFIX__shoplist go, __PREFIX__shops p ";
		if($brandId>0){
			$sql .=" , __PREFIX__brands bd ";
		}
		$sql .= "where go.shop_id = p.shopId AND go.shenyurenshu > 0 AND go.q_end_time is null";
		if($brandId>0){
			$sql .=" AND bd.brandId=go.brandid AND go.brandid = $brandId ";
		}
		if($c1Id>0){
			$sql .= " AND go.mall_cateid = $c1Id";
		}
		if($c2Id>0){
			$sql .= " AND g.mall_cateid2 = $c2Id";
		}
		if($c3Id>0){
			$sql .= " AND g.mall_cateid3 = $c3Id";
		}
		if(!empty($words)){
			$sarr = array();
			foreach ($words as $key => $word) {
				if($word!=""){
					$sarr[] = "go.title LIKE '%$word%'";
				}
			}
			$sql .= " AND (".implode(" or ", $sarr).")";
		}
	    if($prices != "" && $pricelist[0]>=0 && $pricelist[1]>=0){
			$sql .= " AND (g.shopPrice BETWEEN  ".(int)$pricelist[0]." AND ".(int)$pricelist[1].") ";
		}
		$sql .= " GROUP BY go.id";
		if($msort==1){//最新
			$sql .= " ORDER BY time desc";
		}else if($msort==3){//即将揭晓
			$sql .= " ORDER BY go.shenyurenshu asc ";
		}else if($msort==4){//人气
			$sql .= " ORDER BY go.renqi DESC ";
		}else if($msort==5){//剩余人次
			$sql .= " ORDER BY go.shenyurenshu asc ";
		}else if($msort==6){//价值
			$sql .= " ORDER BY go.money desc";
		}else if($msort==7){//价值
			$sql .= " ORDER BY go.money asc";
		}

		$pages = $this->pageQuery($sql,$pcurr,30);
		foreach($pages['root'] as $k=>$v){
			$scale = $v['canyurenshu']/$v['zongrenshu']*100;
			$rs['root'][$k]['scale'] = $scale; 
		}
		$brands = array();
		$sql = "SELECT b.brandId, b.brandName FROM __PREFIX__brands b, __PREFIX__goods_cat_brands cb WHERE b.brandId = cb.brandId AND b.brandFlag=1 ";
		if($c1Id>0){
			$sql .= " AND cb.catId = $c1Id";
		}
		$sql .= " GROUP BY b.brandId";
		$blist = $this->query($sql);
		for($i=0;$i<count($blist);$i++){
			$brand = $blist[$i];
			$brands[$brand["brandId"]] = array('brandId'=>$brand["brandId"],'brandName'=>$brand["brandName"]);
		}
		$rs["brands"] = $brands;
		$rs["pages"] = $pages;
		$gcats["mall_cateid"] = $c1Id;
		$gcats["mall_cateid2"] = $c2Id;
		$gcats["mall_cateid3"] = $c3Id;
		$rs["goodsNav"] = self::getgoodsNav();
		return $rs;
	}
	//获取所有商品分类
	public function getgoodsNav(){
		$m = M('Shoplist');
		$sql = "select gc.catName,gc.cateImg,gc.catId from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.shenyurenshu > 0 and gc.catFlag = 1 group by gc.catId";
		$rs = $m->query($sql);
		return $rs;
	}

	/********************************获取商品详情******************************/
	//获取商品详情
	public function getGoodsDetails(){
		$id = intval(I('gid'));
		$goods = array();
		//获取商品信息
		$sql = "select go.* from __PREFIX__shoplist as go where go.q_end_time is null and go.shenyurenshu > 0 and go.id = ".$id;
		$goods['details'] = $this->queryRow($sql);
		//计算参与人数比例
		$goods['details']['scale'] = $goods['details']['canyurenshu']/$goods['details']['zongrenshu']*100;
		//获取商品图册
		if (!empty($goods['details']['picarr'])){
			$arr = explode(',', $goods['details']['picarr']);
			if (!empty($arr)){
				$gallery = array();
				foreach ($arr as $key => $value){
					$imgs = explode('@', $value);
					@$gallery[] = array('goodsThumbs' => $imgs[1],'goodsImg' => $imgs[0]);
				}
			}
		}
		$goods['details']['gallery'] = $gallery;
		//获取分类信息
		$data = array();
		foreach($goods['details'] as $k=>$v){
			if($k == 'mall_cateid' || $k == 'mall_cateid2' || $k == 'mall_cateid3'){
				$sql = "select catId,catName from __PREFIX__goods_cats where catId = ".$v;
				$data[$k] = $this->queryRow($sql);
			}
		}
		$goods['cat'] = $data;
		//获取其他期数
		if($goods['details']['qishu'] > 1){
			$sql = "select id,qishu from __PREFIX__shoplist where sid = ".$goods['details']['sid']." and q_end_time is not null order by qishu desc";
			$goods['qishu'] = $this->query($sql);
		}
		//查询上期的获奖者信息
		$goods['record']=$this->queryRow("select * from `__PREFIX__member_go_record` where `shopid`='".$goods['qishu'][0]['id']."' AND `shopqishu`='".$goods['qishu'][0]['qishu']."' and huode!=0");
		$m = M('Shoplist');
		$goods['record']['q_end_time'] = $m->where('id = '.$goods['qishu'][0]['id'])->getField('q_end_time');
		return $goods;
	}
	/**************************一元云购购物清单*****************************/
	//获取清单商品列表
	public function getCartList(){
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		$cartGoodsList = array();
		foreach($cartList as $k=>$v){
			$sql = "select go.id,go.title,go.money,go.yunjiage,go.canyurenshu,go.zongrenshu,go.thumb,go.qishu  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catId where go.id = ".$k." and gc.catFlag = 1";
			$rs = $this->queryRow($sql);
			$rs['num'] = $v['num'];
			$cartGoodsList[] = $rs;
		}
		//计算剩余人次及商品总金额
		$moneyCount = 0;
		foreach($cartGoodsList as $k=>$v){
			$shenyu = $v['zongrenshu']-$v['canyurenshu'];
			$cartGoodsList[$k]['shenyu'] = $shenyu;
			$moneyCount += $v['yunjiage']*$v['num'];
		}
		$cartGoodsList['MoneyCount'] = sprintf('%.2f',$moneyCount);
		return $cartGoodsList;
	}
	//获取推荐商品
	public function getPosGoods(){
		$m = M('Shoplist');
		$sql = "select go.id,go.title,go.money,go.q_user_code,go.canyurenshu,go.zongrenshu,go.thumb  from __PREFIX__shoplist as go left join __PREFIX__goods_cats as gc on go.mall_cateid = gc.catIdwhere go.q_end_time is null and go.shenyurenshu >0 and gc.catFlag = 1 order by go.id desc";
		$rs = $m->query($sql);
		return $rs;
	}
	//修改商品数量
	public function changeNum(){
		$rd = array('status'=>-1);
		$id = intval(I('gid',0));
		$num = intval(I('num',0));
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		if($id > 0){
			if(is_array($cartList)){
				foreach($cartList as $k=>$v){
					if($k == $id){
						//增加
						if($num == 1){
							$cartList[$k]['num']++;
						//减少
						}else if($num == -1){
							$cartList[$k]['num']--;
						}
						$rd['status'] = 1;
						cookie('Cartlist',json_encode($cartList));
					}
				}
			}
		}
		return $rd;
	}
	//删除选中清单商品
	public function delSelete(){
		$rd = array('status'=>-1);
		$ids = I('ids');
		$cartList = json_decode(stripslashes(cookie('Cartlist')),true);
		if(!empty($ids)){
			if(is_array($cartList)){
				foreach($cartList as $k=>$v){
					if(in_array($k,$ids)){
						$rd['status'] = 1;		
					}else{
						$Cartlist[$k]['num'] = $v['num'];
					}
				}
				cookie('Cartlist',json_encode($CartList));
			}
		}
		return $rd;
	}

	/***************************************一元云购在线支付*****************************/
	//获取即将揭晓商品
	public function getRightNow(){
		$m = M('Shoplist');
		$sql = 'select id,title,thumb,money,zongrenshu,canyurenshu,qishu,yunjiage from __PREFIX__shoplist where shenyurenshu <> 0 order by canyurenshu desc limit 4';
		$rightNowGoods = $m->query($sql);
		return $rightNowGoods;
	}
}
<?php
namespace Home\Controller;
/**
 * 首页控制器
 */
class IndexController extends BaseController {
	/**
	 * 获取首页信息
	 * 
	 */
    public function index(){
      $areas= D('Home/Areas');
      $areaId2 = $this->getDefaultCity();
      $areaList = $areas->getDistricts($areaId2);
      $mshops = D('Home/Shops');
      $obj = array();
      if((int)cookie("bstreesAreaId3")){
        $obj["areaId3"] = (int)cookie("bstreesAreaId3");
      }else{
        $obj["areaId3"] = ((int)I('areaId3')>0)?(int)I('areaId3'):$areaList[0]['areaId'];
        cookie("bstreesAreaId3",$obj["areaId3"]);
      }
      //广告
      $ads = D('Home/Ads');
      $ads = $ads->getAds($areaId2,-3);
      //商品分类
      $gCatsModel = D('Home/GoodsCats');
      $goodsCat1 = $gCatsModel->queryByList();
      foreach($goodsCat1 as $k=>$v){
        $goodsCat1[$k]['catName'] = mb_substr($v['catName'],0,4,'utf-8');
      }
      $this->assign('goodsCat1',$goodsCat1);
      $this->assign('areaId3',$obj["areaId3"]);
      $this->assign('keyWords',I("keyWords"));
      $this->assign('ads',$ads);
      $this->assign('areaList',$areaList);
      $this->display("Shops/shop_street");
    }
    
    public function test(){
    	$this->display();
    }
    
    /**
     * 广告记数
     */
    public function access(){
    	$ads = D('Home/Ads');
    	$ads->statistics((int)I('id'));
    }
    /**
     * 切换城市
     */
    public function changeCity(){
    	$m = D('Home/Areas');
    	$areaId2 = $this->getDefaultCity();
    	$provinceList = $m->getProvinceList();
    	$cityList = $m->getCityGroupByKey();
    	$area = $m->getArea($areaId2);
    	$this->assign('provinceList',$provinceList);
    	$this->assign('cityList',$cityList);
    	$this->assign('area',$area);
    	$this->assign('areaId2',$areaId2);
    	$this->display();
    }
    /**
     * 跳到用户注册协议
     */
    public function toUserProtocol(){
    	$this->display("Users/user_protocol");
    }
    
    /**
     * 修改切换城市ID
     */
    public function reChangeCity(){
    	$this->getDefaultCity();
    }

    public function tests()
    {
      $m = M('user',C("DB_PREFIX2"),'DB_CONFIG2');
      $data = $m->find();
      print_r($data);
    }
}
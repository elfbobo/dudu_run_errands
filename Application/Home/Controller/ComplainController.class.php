<?php
namespace Home\Controller;
/**
 * 订单投诉控制器
 */
class ComplainController extends BaseController {
    /**
     * 买家添加投诉
     */
    public function addComplain(){
        $this->isUserAjaxLogin();
        $USER = session('WST_USER');
        $complainModel = D('Complain');
        $obj['userId'] = $USER['userId'];
        $obj['orderId'] = I('orderId');
        $obj['content'] = I('content');
        $obj['shopId'] = I('shopId');
        $obj['phone'] = I('phone');
        $rd = $complainModel->addComplain($obj);
        $this->ajaxReturn($rd);
    }
    /**
     * 商家查看所有用户投诉举报信息
     */
    public function index()
    {
        $this->isLogin();
        $complain = D('Complain');
        $USER = session('WST_USER');
        $shopId = $USER['shopId'];
        $data = $complain->queryByPage($shopId);
        if($data['root']){
            $this->assign('data',$data);
        }else{
            $data['error'] = '没有用户的投诉!';
            $this->assign('data',$data);
        }
        $this->assign("startTime",I('post.startTime'));
        $this->assign("endTime",I('post.endTime'));
        $this->assign("umark","complain");
        $this->view->display('Shops/complain/index');
    }
    /**
     * 商家删除投诉信息
     */
    public function delete()
    {
        $this->isShopLogin();
        $complain = D('Complain');
        $rd = $complain->_deleteById();
        $this->ajaxReturn($rd);
    }
    /*
    *买家举报店铺
    */
    public function addReport(){
        $this->isUserAjaxLogin();
        $USER = session('WST_USER');
        $complainModel = D('Complain');
        $obj['userId'] = $USER['userId'];
        $obj['content'] = I('content');
        $obj['shopId'] = I('shopId');
        $rd = $complainModel->addReport($obj);
        $this->ajaxReturn($rd);
    }

}
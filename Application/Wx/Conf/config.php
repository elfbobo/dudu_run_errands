<?php
return array(

    //'配置项'=>'配置值'
    'SEND_MES_COUNT'=>3,//发送短信次数限制
    //'SEND_MES_URL'=>'http://www.exincn.com/sms/api/index.asp?Action=send&SmsFlag=0&UserName={$userName}&UserPass={$userPass}&Mobile={$phone}&Text={$content}',//发送短信的网址GET
    'SEND_MES_URL'=>'http://203.86.7.117:8888/sms.aspx?action=send&userid=22&account={$userName}&password={$userPass}&mobile={$phone}&content={$content}',//最新的短信平台网址
    // 'SEND_MES_USER'=>'ztdb',//短信接口用户名
    // 'SEND_MES_PWD'=>'123123',//短信接口密码
    'SEND_MES_USER'=>'rhtest',//短信接口用户名
    'SEND_MES_PWD'=>'123123',//短信接口密码
    'SEND_MES_TXT'=>'(O2O平台验证码,五分钟内有效）【O2O平台】',//短信内容
    'VIEW_PATH' => './Tpl/',
    'DEFAULT_THEME' => 'Wx',
    'TMPL_PARSE_STRING' => array(
        '__JS__'     => '/Tpl/Wx/js',
        '__CSS__'     => '/Tpl/Wx/css',
        '__IMG__'     => '/Tpl/Wx/image',
        '__WST_WEB__'     => '/',
    ),
    define('WEB_HOST', 'http://'.$_SERVER['HTTP_HOST']),
    define('TOKEN', 'weixin'),
    'TOKEN_ON' => false, // 是否开启令牌验证 默认关闭
    'TOKEN_NAME' => '__hash__', // 令牌验证的表单隐藏字段名称，默认为__hash__
    'TOKEN_TYPE' => 'md5', //令牌哈希验证规则 默认为MD5
    'TOKEN_RESET' => true, //令牌验证出错后是否重置令牌 默认为true
    //微信 支付配置
    'WxPayConf_pub'=>array(
        'APPID' => 'wx1b23dc20249ad1e7',
        'MCHID' => '1496579722',
        'KEY' => '5WhcFw5zjRSjBRgXuC4gG10sY14Cb7Qh',
        'APPSECRET' => 'c3a9a4435abdd60b2023ee4a98a39654',
        'JS_API_CALL_URL' => WEB_HOST.'/wx/wxpay/index/',
        'SSLCERT_PATH' => WEB_HOST.'/ThinkPHP/Library/Vendor/WxPayPubHelper/cacert/apiclient_cert.pem',
        'SSLKEY_PATH' => WEB_HOST.'/ThinkPHP/Library/Vendor/WxPayPubHelper/cacert/apiclient_key.pem',
        'NOTIFY_URL' =>  WEB_HOST.'/index.php/Wx/Wxpay/wxCallBack',
        'CURL_TIMEOUT' => 30
    )
);
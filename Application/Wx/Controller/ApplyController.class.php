<?php

namespace Wx\Controller;

use Think\Controller;
use Think\Model;
class ApplyController extends BaseController {

    public function __construct(){

        parent::__construct();
        parent::isLogin();

    }


    public function userApplyIndex(){
        $data = D('Wx/Apply')->userData();
        $this->assign('list',$data);
        $this->banks = M('banks')->where('bankFlag=1')->select();
        $this->display('userApplyIndex');
    }


    public function shopApplyIndex(){
        $data = D('Wx/Apply')->shopData();


        $this->assign('list',$data);
        $this->display('shopApplyIndex');

    }







}

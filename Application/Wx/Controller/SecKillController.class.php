<?php
namespace Wx\Controller;
use Think\Controller;
use Think\Model;
use Wx\Controller\BaseController;
class SecKillController extends BaseController{

    public function index(){
        $time = time();
        $db = D('Wx/Seckill');
        $nowSeckill = $db->getTimeFrame($time);
        $SeckillSetTime = $nowSeckill['seckillSetTime'];
        $dateList = array(
                array('time'=>'00:00','seckillSetTime'=>0),
                array('time'=>'08:00','seckillSetTime'=>1),
                array('time'=>'12:00','seckillSetTime'=>2),
                array('time'=>'16:00','seckillSetTime'=>3),
                 array('time'=>'20:00','seckillSetTime'=>4)
            );
        foreach ($dateList as $key=>$value){
            if($dateList[$key]['seckillSetTime']==$SeckillSetTime){
                $dateList[$key]['state']='抢购中';
            }elseif($dateList[$key]['seckillSetTime']<$SeckillSetTime){
                $dateList[$key]['state']='已结束';
            }else{
                $dateList[$key]['state']='未开始';
            }
        }
        //print_r($db->getSeckillOrder());exit;
        for ($i=0;$i<=4;$i++){
            $list[$i]['isShow']= $i==$SeckillSetTime?1:0;//导航栏选中区域显示
            $list[$i]['info']= $db->getGoodsList($i,$time,$SeckillSetTime);
        }

        //print_r($list);
        $this->assign('goodslist',$list);
        $this->assign('dateList',$dateList);//导航列表
        $this->assign('getTime',$db->getTime($SeckillSetTime));//时间倒计时
        $this->display();
    }



    public  function index2(){
        //20：00-->0:00 时间段秒杀
        $starTime=strtotime(date('Y-m-d').'20:00:00');
        $endTime=strtotime(date('Y-m-d').'23:59:59');
        $this->oneStarTime=$starTime;
        $this->ondEndTime=$endTime;
        $field="k.shopId,k.goodsId,k.seckillPrice,k.seckillMaxCount,s.goodsName,s.goodsThums,s.marketPrice,s.shopPrice,k.goodsStock";
        $map['k.goodsSeckillStatus']=1;
        $map['k.seckillStatus']=1;
        $map['s.goodsStatus']=1;
        $map['s.goodsFlag']=1;
        $map['s.isSeckill']=1;
        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
        $Seckill=M('goods_seckill');
        $info=$Seckill->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');
                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->one=$info;
        
        //00:00->08:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'00:00:00');
        $endTime=strtotime(date('Y-m-d').'08:00:00');
        $this->twoStarTime=$starTime;
        $this->twoEndTime=$endTime;
        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
        $info=$Seckill->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->two=$info;
        
        //08:00->12:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'08:00:00');
        $endTime=strtotime(date('Y-m-d').'12:00:00');
        $this->threeStarTime=$starTime;
        $this->threeEndTime=$endTime;
        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
       $info=$Seckill->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=0;
            }
        }
        $this->three=$info;

        
        //12:00->16:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'12:00:00');
        $endTime=strtotime(date('Y-m-d').'16:00:00');
        $this->fourStarTime=$starTime;
        $this->fourEndTime=$endTime;
        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
        $info=$Seckill->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');
                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);

                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');

                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            //echo '已结束';
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;


                $info[$k]['status']=0;
            }
        }
        $this->four=$info;
        
        //16:00->20:00时间段秒杀
        $starTime=strtotime(date('Y-m-d').'16:00:00');
        $endTime=strtotime(date('Y-m-d').'20:00:00');

        $this->fiveStarTime=$starTime;
        $this->fiveEndTime=$endTime;
        $map['k.seckillStartTime']=array('between',"$starTime,$endTime");
        $map['k.seckillEndTime']=array('between',"$starTime,$endTime");
        $info=$Seckill->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->select();
        if(time()>$starTime&&time()<$endTime){
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=1;
            }
        }else{
            foreach ($info as $k=>$v){
                $couMap['oto_orders.orderStatus']=array('egt',0);
                $couMap['oto_orders.isPay']=array('eq',1);
                $couMap['oto_orders.orderType']=array('eq',2);
                $SeckillStock = M('goods_seckill')->where(array('goodsId'=>$v['goodsId']))->getField('seckillStock');

                $starTime = $this->format($starTime);
                $endTime = $this->format($endTime);
                $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
                $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
                $stock=$v['goodsStock']+$count;
                $info[$k]['percent']=($count/$SeckillStock)*100;
                $info[$k]['status']=0;
            }
        }
         $this->five=$info;
        print($this->one);
        print($this->two);
        print($this->three);
        print($this->four);
        print($this->five);
        $this->display();
    }
    public function format($time){
        return  date("Y-m-d H:i:s",$time);
    }
}
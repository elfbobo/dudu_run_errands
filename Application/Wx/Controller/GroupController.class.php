<?php

namespace Wx\Controller;

use Think\Controller;
use Think\Model;
use Wx\Controller\BaseController;
class GroupController extends BaseController{
    public function index(){
        $db = D('Wx/Group');
        $grouplist = $db->getGoodslist();
        foreach ($grouplist as $key=>$value){
            $endtime[$key]=trim(date('Y,m,d,H,i,s',$value['endTime']));
        }

        //print_r($grouplist);exit;

        $this->assign('endtime',$endtime);
        $this->assign('grouplist',$grouplist);
        $this->display();
    }

    //获取数据库购物车信息
    public function getCart(){
        $map['userId']= session('oto_userId');
        $isCarExists=M('car_session')->where($map)->field('car_session,userId')->find();
        $dbCarInfo=unserialize($isCarExists['car_session']);
        print_r($dbCarInfo);
    }
    //获取session里的购物信息
    public  function getseesionCart(){
        print_r(session("WST_CART"));
    }
    //清楚购物车信息
    public  function clearCart(){
        session('sortingOrder',null);
        session("WST_CART",null);
        $where['userId'] = session('oto_userId');
        $data['car_session'] = '';
        M('car_session')->where($where)->save($data);
    }

    //获取订单seesion
    public function sortingOrder(){
        print_r(session('sortingOrder'));
    }

}

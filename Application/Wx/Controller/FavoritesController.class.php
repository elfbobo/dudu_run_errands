<?php
namespace Wx\Controller;

use Think\Controller;
use Think\Model;

class FavoritesController extends BaseController
{

    public function _initialize()
    {
        parent::isLogin();
        @header('Content-type: text/html;charset=UTF-8');
    }




    // public function index()
    // {
    //     $map['f.userId'] = session('oto_userId');
    //     $map['f.favoriteType'] = 0;
    //     $map['g.goodsStatus']=1;
    //     $map['g.goodsFlag']=1;
    //     $this->fav = M('favorites')->where($map)
    //         ->join('as f left join oto_goods as g on f.targetId=g.goodsId')
    //         ->field('goodsThums,goodsName,shopPrice,goodsId')
    //         ->SELECT();
    //     $this->display();
    // }

    public function index(){

        $user_id=session('oto_userId');
        $lat = session('lat');
        $lon = session('lon');
        //$lat=I('lat');
        //$lon=I('lon');
        // $lat=23.140103;
        // $lon=113.317069;
        $re=M('sys_configs')->where(['fieldName'=>'店铺附近范围'])->getField('fieldCode');

        $fanwei=$re?$re:20000;

        $sql="SELECT favoriteId,__PREFIX__shops.* from __PREFIX__favorites,__PREFIX__shops where __PREFIX__favorites.userId='$user_id' and `favoriteType`='1' and targetId=shopId";

        $shop_info=M()->query($sql);
        //echo M()->getLastSql();die;
        //dump($shop_info);die;
        $m = D('Search');
        $local_count=0;
        $unlocal_count=0;
        if($shop_info){

            foreach($shop_info as $k =>$v){
                $lat2=$v['latitude'];
                $lon2=$v['longitude'];
                $shop_info[$k]['juli']=$m->getDistance($lat,$lon,$lat2,$lon2);

                if($shop_info[$k]['juli']>$fanwei){
                    $shop_info[$k]['is_quyu']=0;
                    $unlocal_count++;
                } else{
                    $shop_info[$k]['is_quyu']=1;
                    $local_count++;
                }

                if($shop_info[$k]['juli']<1000){
                    $shop_info[$k]['juli2']=$shop_info[$k]['juli']."米";
                }else{
                    $shop_info[$k]['juli2']=round($shop_info[$k]['juli']/1000,1)."KM";
                }
            }


            foreach($shop_info as $i=>$v)
            {
                $shop_id=$v['shopId'];

                $sql="SELECT * from `__PREFIX__goods` where `isSale`=1 and shopId='{$shop_id}' and goodsFlag='1'";

                $shop_goods_list=M()->query($sql);//将取得的所有数据赋值给person_info数组

                //月销量
                if($shop_goods_list){
                    $shop_sum=$m->monthSale($shop_goods_list);
                }else{
                    $shop_sum=0;
                }

                $shop_info[$i]['yue_xiaoliang']=$shop_sum.'';

                //计算商店评分

                $shop_info[$i]['score']=$m->calcScore($shop_id);

            }

            $this->list=$shop_info;
        }

        $this->local=$local_count;
        $this->unlocal=$unlocal_count;
        $this->display();
    }


    /**
     * 收藏店铺
     */
    public function favorites(){
        $id = I('id');
        $flag = I('flag');
        if(!session('oto_userId')) die(-2) ;
        $map=[
            'favoriteType'=>1,
            'userId'=> session('oto_userId'),
            'targetId'=>$id
        ];
        //添加记录
        if($flag==1){
            $map['createTime']=time();
            $re = M('favorites')->data($map)->add();
            if($re) die(0);
            else die(-1);
        }else{
            $re = M('favorites')->where($map)->delete();
            //dump(M('favorites')->getLastSql()) ;die;
            if($re) die(0);
            else die(-1);
        }
    }

    /**
     * 删除店铺
     */
    public function delFav(){
        $id=I('id');
        $re = M('favorites')->where(['favoriteId'=>$id])->delete();
        if($re) $this->ajaxReturn(['status'=>1,'msg'=>'已取消收藏']);
        $this->ajaxReturn(['status'=>-1,'msg'=>'操作失败']);
    }
}
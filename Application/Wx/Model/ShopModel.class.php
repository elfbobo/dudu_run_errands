<?php
namespace Wx\Model;


class ShopModel extends BaseModel{
    //营业时间
    public function serviceTime(){
        $shopId=I('shopId');
        //$sql="select deliveryCostTime,serviceStartTime,serviceEndTime from `".$oto."_shops` where `shopId`='{$shopId}'";


        $shops_info = M('shops')
                   ->field('deliveryCostTime,serviceStartTime,serviceEndTime')
                   ->where(['shopId'=>$shopId])
                   ->find();
        $time=time();

        $start=$shops_info['serviceStartTime'];
        $end=$shops_info['serviceEndTime'];
        $deliveryTime=$shops_info['deliveryCostTime'];

        $z_start=explode('.',$start);
        $z_end=explode('.',$end);
        $a=date('Y-m-d '.$z_start[0].':'.$z_start[1].'0:00',$time);
        $b=date('Y-m-d '.$z_end[0].':'.$z_end[1].'0:00',$time);
        $c=date('Y-m-d '.$z_end[0].':00:00',$time);
        $start_time=strtotime($a);
        $end_time=strtotime($b);
        $wm=strtotime($c)+3600;
        // $a=date('Y-m-d H:i:s',$start_time);
        // $b=date('Y-m-d H:i:s',$end_time);
        $t=0;

        if($end_time==$wm-1200) $wm=$wm-1200;
        else if($end_time>$wm-1200)
        {
            $data[0]['time_list'][$t]=date('H:i',$end_time);
            $t++;
            $wm=$wm-1200;
        }
        else if($end_time==$wm-2400) $wm=$wm-2400;
        else if($end_time>$wm-2400)
        {
            $data[0]['time_list'][$t]=date('H:i',$end_time);
            $t++;
            $wm=$wm-2400;
        }
        else if($end_time==$wm-3600) $wm=$wm-3600;
        else if($end_time>$wm-3600)
        {
            $data[0]['time_list'][$t]=date('H:i',$end_time);
            $t++;
            $wm=$wm-3600;
        }

        for($i=$wm;$i>=$start_time && $i>($time+$deliveryTime*60);$i-=1200)
        {
            $data[0]['time_list'][$t]=date('H:i',$i);
            $t++;
        }
        $data[0]['time_list'][$t]=date('H:i',($time+$deliveryTime*60));
        $t++;
        for($i=0;$i<$t;$i++)
        {
            for($j=$i;$j<$t;$j++)
            {
                if($data[0]['time_list'][$i]>$data[0]['time_list'][$j])
                {
                    $tt=$data[0]['time_list'][$i];
                    $data[0]['time_list'][$i]=$data[0]['time_list'][$j];
                    $data[0]['time_list'][$j]=$tt;
                }
            }
        }
        $data[0]['time_list'][0]="尽快送达(预计".$data[0]['time_list'][0].")";
        for($i=1;$i<$t-1;$i++)
        {
            $data[0]['time_list'][$i]=$data[0]['time_list'][$i]."-".$data[0]['time_list'][$i+1];
        }
        array_splice($data[0]['time_list'], $t-1, 1);
        if($t==0) $data[0]['time_list']=[];
        $data[0]['serviceStartTime_for_num']=$start_time;
        $data[0]['serviceEndTime_for_num']=$end_time;
        $data[0]['serviceStartTime_for_str']=date('Y-m-d H:i:s',$start_time);
        $data[0]['serviceEndTime_for_str']=date('Y-m-d H:i:s',$end_time);
        $data[0]['time_for_num']=$time;
        $data[0]['time_for_str']=date('Y-m-d H:i:s',$time);
        return $data;
    }

    //新店来袭
    public function newShops(){
        $lat = session('lat');
        $lon = session('lon');
        //$lat=I('lat');
        //$lon=I('lon');
        // $lat=23.1200491;
        // $lon=113.30764968;
        $sys_configs=M('sys_configs')->where("fieldName='店铺附近范围'")->getField('fieldCode');
        $fanwei = $sys_configs?$sys_configs:200000;

        $time=time();
        $time=$time-3600*24*30;
        $time2=strtotime(date('Y-m-d 00:00:00',$time));
        $sql="select s.shopId,s.shopImg,s.shopName,s.isBondShow,s.bondStartTime,s.bondEndTime,s.bond,s.deliveryStartMoney,s.deliveryCostTime,s.deliveryFreeMoney,s.deliveryStartMoney,s.deliveryMoney,s.latitude,s.longitude from `__PREFIX__shops` as s where `shopFlag`='1' and `shopStatus`='1' and createTime>='$time2' order by createTime DESC  ";

        $shop_info=M()->query($sql);
        //有商店存在
        if($shop_info){
            $m = D('Search');
            for($i=0;$i<count($shop_info);$i++){
                $lat2=$shop_info[$i]['latitude'];
                $lon2=$shop_info[$i]['longitude'];
                $shop_info[$i]['juli']=$m->getDistance($lat,$lon,$lat2,$lon2);
                if($shop_info[$i]['juli']<1000){
                    $shop_info[$i]['juli2']=$shop_info[$i]['juli']."米";
                }else {
                    $shop_info[$i]['juli2']=round($shop_info[$i]['juli']/1000,1)."KM";
                }
                $shop_info[$i]['juli']=$shop_info[$i]['juli'].'';
                if($shop_info[$i]['juli']>$fanwei){
                    array_splice($shop_info, $i, 1);
                    $i--;
                }
            }


            $shop_goods_list=[];


            foreach($shop_info as $k=>$v){
                //查询商店商品
                $shop_id=$v['shopId'];

                $sql="SELECT goodsId from `__PREFIX__goods` where `isSale`=1 and shopId='{$shop_id}' and goodsFlag='1'";

                $shop_goods_list = M()->query($sql);

                //月销量
                if($shop_goods_list){
                    $shop_sum=$m->monthSale($shop_goods_list);

                }else{
                    $shop_sum=0;
                }

                $shop_info[$k]['yue_xiaoliang']=$shop_sum.'';

                //计算商店评分
                $shop_info[$k]['score']=$m->calcScore($shop_id);

            }

            return $shop_info;

        }else{
            return;
        }
    }

    //获取店铺评论
    public function getComment(){
        $shop_id = I('id');
        $m = M('goods_appraises');
        $map=[
            'shopId'=>$shop_id,
            'isDel' =>0,
            'g.isShow'=>1
        ];
        if(I('goodsId')){
            $map=[
                'goodsId'=>I('goodsId'),
                'isDel' =>0,
                'g.isShow'=>1
            ];
        }

        $goods_comment = $m
                       ->alias('g')
                       ->field('g.*, u.userName')
                       ->join('__USERS__  as u on g.userId=u.userId')
                       ->where($map)
                       ->order('createTime DESC')
                       ->select();
        $goodsScore = 0;
        $timeScore = 0;
        $serviceScore = 0;

        $count = count($goods_comment);
        if(!$count) return ['status'=>-1, 'msg'=>'暂无评论'];
        $list = [];
        $list['satisfied']=0;
        $list['unSatisfied']=0;
        foreach($goods_comment as $k => $v){
            $serviceScore+=$v['serviceScore'];
            $timeScore+=$v['timeScore'];
            $goodsScore+=$v['goodsScore'];
            //$goods_comment[$k]['createTime']=date('Y-m-d H:i:s',$v['createTime']);
            if($v['serviceScore']>=3){
                $goods_comment[$k]['isOk']=1;
                $list['satisfied']++;
            }else{
                $goods_comment[$k]['isOk']=0;
                $list['unSatisfied']++;
            }
            //匿名评论
            if($v['anonymity']==1){
                $goods_comment[$k]['userName']='匿名用户';
            }

        }

        $goods = $goodsScore/$count;
        $time = $timeScore/$count;
        $service = $serviceScore/$count;


        $list['root'] = $goods_comment;
        $list['goodsScore'] = round($goods,1);
        $list['timeScore'] = round($time,1);
        $list['serviceScore'] = round($service,1);
        $list['score']=round(($goods +$time+$service)/3, 1);
        $list['count'] = $count;
        // $satisfied['count'] = count($satisfied);
        // $unSatisfied['count'] = count($unSatisfied);
// dump($list);die;
        return $list;
    }

    public function getShopGoods(){

        $get_num=I('get_num');
        $yiji_id=I('yiji_id');
        $erji_id=I('erji_id');
        $shop_id=I('shop_id');
        $m=M();

        //查询商品
        $m_goods=M('goods');
        $map = [
            'shopCatId1'=>$yiji_id,
            'isSale'    =>1,
            'shopId'    =>$shop_id ,
            'goodsFlag' =>1,
            'goodsStock'=>['gt',0] ,
            'goodsStatus'=>1
        ];
        if($get_num==2) $map['shopCatId2']=$erji_id;

        $goods_list = $m_goods
                ->field('goodsId,goodsImg,goodsName,shopPrice,goodsStock,brandId,goodsCatId1,goodsCatId2,goodsCatId3,shopCatId1,shopCatId2,goodsThums,shopId')
                ->where($map)
                ->select();


        if(isset($goods_list)){
            $time_star=time();
            $time_end=$time_star-3600*24*30;
            $time_star1=date('Y-m-d H:i:s',$time_end);
            $time_end1=date('Y-m-d H:i:s',$time_star);
            //
            //for($i=0;$i<count($goods_list);$i++)
            $m_goods_attr = M('goods_attributes');
            foreach($goods_list as $k=>$v){
                $goods_id=$v['goodsId'];
                // $sql2="SELECT * from `__PREFIX__goods_attributes`,`__PREFIX__attributes` where __PREFIX__goods_attributes.goodsId='$goods_id' and isPriceAttr=1 and __PREFIX__goods_attributes.attrId=__PREFIX__attributes.attrId order by __PREFIX__attributes.attrId ASC";
                // $shuxing_info=$m->query($sql2);
                $shuxing_info = $m_goods_attr->where(['goodsId'=>$goods_id])->getField('id');

                if(!isset($shuxing_info)){
                    $goods_list[$k]['is_shuxing']=0;
                }else{
                    $goods_list[$k]['is_shuxing']=1;
                }

                $sql="SELECT * from `__PREFIX__orders`,`__PREFIX__order_goods` where __PREFIX__orders.orderId=__PREFIX__order_goods.orderId and __PREFIX__order_goods.goodsId='{$goods_id}' and createTime>='$time_star1' and createTime<='$time_end1' and orderStatus='4'";
                $order_list=$m->query($sql);
                $sum=0;

                if(isset($order_list)){

                    foreach($order_list as $value)
                    {
                        $sum+=$value['goodsNums'];
                    }
                }
                $goods_list[$k]['yue_xiaoliang']=$sum;

                $sum=0;
                $sql="SELECT * from __PREFIX__goods_appraises where goodsId='{$goods_id}' and isDel='0' ";
                $appraises=$m->query($sql);

                if(isset($appraises))
                {
                    $sum=count($appraises);
                }

                $goods_list[$k]['goods_appraises']=$sum;

                //file_put_contents("tsxx.txt", "\r\n"."是否有属性:".$goods_list[$i]['is_shuxing']."\r\n", FILE_APPEND);
            }
            return ($goods_list);
        }
        return [];
    }


}
<?php
 namespace Wx\Model;
/**
 * 秒杀类
 */
use Think\Model;
class SeckillModel extends Model {

    /*根据不同抢购时间获取抢购商品列表
     * type：0|00:00,1|08:00,2|12:00,3|16:00,4|20:00
     * time：时间戳
     * seckillSetTime：与type比较，相同就等于马上抢购，否则就未开始或已结束
     * */
    public function getGoodsList($type,$time,$seckillSetTime){
        //seckillSetTime:秒杀时间点
        $map['k.seckillSetTime'] = $type;
        $map['k.goodsSeckillStatus']=1;
        $map['k.seckillStatus']=1;//秒杀状态：0表示未开始1表示进行中2表示结束
        $map['s.goodsStatus']=1;//商品状态：0为待审核1为已通过
        $map['s.goodsFlag']=1;
        $map['s.isSeckill']=1;//0表示不是秒杀1表示是秒杀商品
        $map['k.seckillStartTime']= array('lt',$time);
        $map['k.seckillEndTime']= array('gt',$time);
        $db=M('goods_seckill');
        $info=$db->join('as k left join oto_goods as s on s.goodsId=k.goodsId')->where($map)->order('seckillStock desc')->select();
        if(!empty($info)){
            foreach ($info as $key=>$value){
                if($type==$seckillSetTime){
                    $info[$key]['state']='马上抢';
                    $info[$key]['disable']=1;
                }elseif($type<$seckillSetTime){
                    $info[$key]['state']='已结束';
                    $info[$key]['disable']=0;
                }else{
                    $info[$key]['state']='未开始';
                    $info[$key]['disable']=0;
                }

                //库存判断
                if($info[$key]['seckillStock']<=0){
                    $info[$key]['state']='已经抢完';
                    $info[$key]['disable']=0;
                }

            }
        }else{
            $info = array();
        }

        return $info;
    }
    //获取抢购时间内，已支付的订单秒杀商品
    public function getOrderStock($goodsId){
        $date = $this->getTimeFrame(time());
        $couMap['oto_orders.orderStatus']=array('egt',0);
        $couMap['oto_orders.isPay']=array('eq',1);//已支付
        $couMap['oto_orders.orderType']=array('eq',2);//1普通2秒杀3团购4积分5抽奖
        $couMap['order_goods.goodsId']=array('eq',$goodsId);//当前商品ID
        $starTime = $date['begin_date'];
        $endTime  = $date['end_date'];
        $couMap['oto_orders.createTime']=array('between',"$starTime,$endTime");
        $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
        $count = !empty($count)?$count:0;
        return $count;
    }

    //获取当前抢购时间范围
    public function getTimeFrame($time){
        $nowDate = date('Y-m-d',$time);
        $nowTime0 = $nowDate.' 00:00:00';
        $nowTime0 = strtotime($nowTime0);
        $nowTime1 = $nowDate.' 08:00:00';
        $nowTime1 = strtotime($nowTime1);
        $nowTime2 = $nowDate.' 12:00:00';
        $nowTime2 = strtotime($nowTime2);
        $nowTime3 = $nowDate.' 16:00:00';
        $nowTime3 = strtotime($nowTime3);
        $nowTime4 = $nowDate.' 20:00:00';
        $nowTime4 = strtotime($nowTime4);
        $nextTime = mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        if($nowTime0<$time && $time<$nowTime1){
            //00:00-08:00
            $array['begin']=$nowTime0;
            $array['end']  =$nowTime1;
            $array['seckillSetTime']=0;
        }else if($nowTime1<$time && $time<$nowTime2){
            //08:00-12:00
            $array['begin']=$nowTime1;
            $array['end']  =$nowTime2;
            $array['seckillSetTime']=1;
        }else if($nowTime2<$time && $time<$nowTime3){
            //12:00-16:00
            $array['begin']=$nowTime2;
            $array['end']  =$nowTime3;
            $array['seckillSetTime']=2;
        }else if($nowTime3<$time && $time<$nowTime4){
            //16:00-20:00
            $array['begin']=$nowTime3;
            $array['end']  =$nowTime4;
            $array['seckillSetTime']=3;
        }else if($nowTime4<$time && $time<$nextTime){
            //20:00-00:00
            $array['begin']=$nowTime4;
            $array['end']  =$nextTime;
            $array['seckillSetTime']=4;
        }
        $array['begin_date'] = date('Y-m-d H:i:s',$array['begin']);
        $array['end_date'] = date('Y-m-d H:i:s',$array['end']);
        return  $array;
    }



    //用于倒计时:格式为2017,07,07,08,00,00
    public function getTime($seckillSetTime=0){
        if($seckillSetTime==4){
            $seckillSetTime=0;
        }else{
            $seckillSetTime++;
        }
        switch ($seckillSetTime){
            case 0:$time='23,59,59';break;
            case 1:$time='08,00,00';break;
            case 2:$time='12,00,00';break;
            case 3:$time='16,00,00';break;
            case 4:$time='20,00,00';break;
        }
        $nowDate = date('Y,m,d,',time()).$time;
        return $nowDate;
    }

    public function test(){
        echo 1;
    }

    public function format($time){
        return  date("Y-m-d H:i:s",$time);
    }
};
?>
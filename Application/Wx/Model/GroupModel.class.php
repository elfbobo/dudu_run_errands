<?php
 namespace Wx\Model;
/**
 * 团购类
 */
use Think\Model;
class GroupModel extends Model {


    //获取团购商品
    public function  getGoodslist(){
        $time = time();
        $field = 'g.goodsName,g.goodsThums,g.goodsStock,gg.*';
        $where = 'g.goodsStatus=1 AND g.goodsFlag = 1 and g.isSale=1 and g.isGroup=1 and gg.goodsGroupStatus=1 and startTime<'.$time.' and endTime>'.$time;
        $db=M('goods_group as gg');
        $grouplist = $db->join('oto_goods as g on gg.goodsId=g.goodsId')->field($field)->where($where)->order('g.goodsSTock desc')->select();
        foreach ($grouplist as $key=>$value){
            $startDate = date('Y-m-d H:i:s',$value['startTime']);
            $endDate   = date('Y-m-d H:i:s',$value['endTime']);
            $orderCount = $this->getOrderGroup($value['goodsId'],$startDate,$endDate,$value['id']);
            $grouplist[$key]['startDate'] = $startDate;
            $grouplist[$key]['endDate'] =  $endDate;
            $grouplist[$key]['orderCount']=$orderCount;//获取团购订单数量
            $message = '马上抢购';
            if($orderCount>$value['groupMaxCount']){
                $message='已结束';
            }
            $grouplist[$key]['message']=$message;
        }
        return $grouplist;
    }
    //获取团购订单数量：
    public function getOrderGroup($goodsId,$startDate,$endDate,$goodsGroupId=0){
        //$couMap['oto_orders.orderStatus']=array('egt',0);
        //$couMap['oto_orders.isPay']=array('eq',1);//已支付
        $couMap['oto_orders.orderType']=array('eq',3);//1普通2秒杀3团购4积分5抽奖
        $couMap['oto_order_goods.goodsId']=array('eq',$goodsId);//当前商品ID
        $couMap['oto_orders.createTime']=array('between',"$startDate,$endDate");
        $couMap['oto_order_goods.goodsGroupId']=array('eq',$goodsGroupId);
        $couMap['oto_orders.userId']=session('oto_userId');
        $count=M('order_goods')->join('oto_orders on oto_orders.orderId=oto_order_goods.orderId')->where($couMap)->sum('oto_order_goods.goodsNums');
        //echo M()->getlastsql();die;
        $count = !empty($count)?$count:0;
        //echo M()->getlastsql();
        return $count;
    }

};
?>
<?php
namespace Wx\Model;

use Think\Model;

class CommentModel extends Model{
    //订单评价
    public function orderEvaluate(){

    }
    //批量处理商品评论
    public function addEvaluateHandle()
    {

        $service = I('service');
        $send = I('send');
        $content = I('content');
        $orderId = I('orderId');
        $gId = I('gId'); //商品Id
        $gScore = I('gScore'); //商品评分
        $gContent = I('gContent'); //商品评价内容
        $niMing = I('niMing'); //商品评价是否匿名
        $DB = M('goods_appraises');

        if($DB->where(['orderId'=>$orderId])->getField('orderId')){
            return ['status'=>-2,'msg'=>'已经评论过了'];
        }

        $info=M('orders')->where(array('orderId'=>$orderId))->field('shopId')->find();


        $DB -> startTrans();
        $check=1;
        foreach($gId as $k=>$v){
            $data = [];
            $data['shopId'] = $info['shopId'];
            $data['orderId'] = $orderId;
            //$data['userId'] = $info['userId'];
            $data['goodsId'] = $gId[$k];
            $data['serviceScore'] = $service;
            $data['timeScore'] = $send;
            $data['content'] = $gContent[$k];
            $data['userId'] = session('oto_userId');
            $data['goodsScore'] = $gScore[$k];
            $data['createTime'] = date('Y-m-d H:i:s', time());
            $data['anonymity'] = $niMing[$k];
            $A = $DB->data($data)->add();
            if(!$A){
                $DB-> rollback();
                $check = 0;
            }
        }

        if($check){
            $DB->commit();
            return ['status'=>1,'msg'=>'评论成功'];
        }

        return ['status'=>-1,'msg'=>'评论失败'];



        //$isExisA=$DB->where(array('orderId'=>$orderId,'goodsId'=>$goodsId))->find();




        // if ($img) {
        //     $sDB = M('share');
        //     $isExisB=$sDB->where(array('goodsId'=>$goodsId,'orderId'=>$orderId))->find();
        //     if($isExisB){
        //         $this->ajaxReturn(array( 'status' => - 2 ));
        //         return;
        //     }
        //     $newImg = explode('|', $img);
        //     // 有图片晒单和评价2表都是要写数据
        //     $sdata['userId']=$info['userId'];
        //     $sdata['shopId']=$info['shopId'];
        //     $sdata['goodsId']=$goodsId;
        //     $sdata['orderId']=$orderId;
        //     $sdata['shareContent']=$content;
        //     $sdata['shareTitle']=$content;
        //     $sdata['star']=$star;
        //     $sdata['shareTime']= date('Y-m-d H:i:s', time());
        //     $sdata['anonymity']= $anonymity;
        //     $sdata['shareImg1']= $newImg[0];
        //     $sdata['shareEnvy']= ' ' ;
        //     $sdata['shareImg2']= $newImg[1];
        //     $sdata['shareImg3']= $newImg[2];
        //     $sdata['shareImg4']= $newImg[3];
        //     $A=false;
        //     $B=false;
        //     if($DB->create($data)){
        //             $A=$DB->add();
        //      }
        //     if( $sDB->create($sdata)){
        //         $B=$sDB->add();
        //     }
        //     if ($A&&$B) {
        //             //$this->ajaxReturn(array('status' =>0 ));
        //         } else {
        //             //$this->ajaxReturn(array( 'status' => - 1 ));
        //         }
        // } else {
        //     if($isExisA){
        //         //$this->ajaxReturn(array( 'status' => - 2 ));
        //         return;
        //     }
        //     if ($DB->create($data)) {
        //         $A = $DB->add();
        //         if ($A) {
        //             $this->ajaxReturn(array('status' =>0 ));
        //         } else {
        //             $this->ajaxReturn(array( 'status' => - 1 ));
        //         }
        //     } else {
        //         $this->ajaxReturn(array('status' => - 1));
        //     }
            // 没有图片只写评价表
        //}
    }


}
<?php
 namespace Wx\Model;
/**
 * 优惠券类
 */
use Think\Model;
class CouponModel extends Model {

    // 只检查页面优惠券是否过期或领完
    public function overdue(){
        $where['is_effect']=array('in','1,2');//'有效性 0:无效 1:有效 2:领完',
        $data=M('youhui')->field('id,end_time,total_num,user_count')->where($where)->select();
        $newtime=time();
        $count=count($data);
        for ($i=0; $i < $count; $i++) {
            //判断是否领完
            $sy=$data[$i]['total_num']-$data[$i]['user_count'];
            if ($sy==0) {
                M('youhui')->execute("update __PREFIX__youhui set is_effect='2' where id=".$data[$i]['id']);//主表更新为已领完
            }
            //判断是否过期
            if ($newtime>$data[$i]['end_time']) {
                M('youhui_user_link')->execute("update __PREFIX__youhui_user_link set u_is_effect='0' where youhui_id=".$data[$i]['id']);//用户领取表更新为已过期
                M('youhui')->execute("update __PREFIX__youhui set is_effect='0' where id=".$data[$i]['id']);//主表更新为无效
            }
        }
    }
    public function test(){
        echo 1;
    }

};
?>
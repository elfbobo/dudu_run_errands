<?php
namespace Wx\Model;
use Think\Model;
/**
 *
 */
class ApplyModel extends Model {

    public function __construct(){

        parent::__construct();



    }


    public function userData(){

        $map = array(
            'userId'=>session('oto_userId'),
            'applyType'=>0,
            //'time'=>['between',[strtotime('-30 days'),time()]]
            );
        $res['userMoney'] = M("users")->where(array('userId'=>session('oto_userId')))->getField('userMoney');
        $res['applyList'] = M("apply")->field('id,applyPrice,status,time')->where($map)->select();
        $this->applyFormat($res['applyList']);
        return $res;
    }


    public function shopData(){



        $res['bizMoney'] = M("shops")->where(array('userId'=>session('oto_userId')))->getField('bizMoney');

        $res['applyList']= M("apply")->field('id,applyPrice,status,time')->where(array('userId'=>session('oto_userId'),'applyType'=>1))->select();

        $this->applyFormat($res['applyList']);


        return $res;
    }
    private function applyFormat(&$data){

        foreach($data as $key=>$value){

            $temp = $this->checkStatus($value['status']);
            $data[$key]['status'] = $temp['status'];
            $data[$key]['class'] = $temp['class'];
        }





        return $data;
    }


    //申请提现-日志
    private function checkStatus($status)
    {

        switch ($status) {
            case 0:
                $status = '待处理';
                $class  = 'green';
                break;

            case 1:
                $status =  '处理中';
                $class  = 'orange';
                break;
            case 2:
                $status =  '已完成';
                $class  = 'gray';
                break;

            case 3:
                $status =  '不通过';
                $class  = 'red';
                break;
            default:

                break;


        }

        return ['status'=>$status,'class'=>$class];
    }





}
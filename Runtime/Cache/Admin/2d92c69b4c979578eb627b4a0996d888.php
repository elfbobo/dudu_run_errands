<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta http-equiv="Expires" content="0">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Cache-control" content="no-cache">
		<meta http-equiv="Cache" content="no-cache">
		<?php if(isset($WST_STAFF['type'])): ?><title>欢迎光临代理后台</title>
		<?php else: ?>
        <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title><?php endif; ?>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="/Tpl/Admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="/Tpl/Admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <script src="/Public/js/jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="/Public/js/html5shiv.min.js"></script>
          <script src="/Public/js/respond.min.js"></script>
        <![endif]-->
        
        <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="/Tpl/Admin/js/jquery-ui.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="/Tpl/Admin/js/AdminLTE/app.js" type="text/javascript"></script>
        <script src="/Public/js/common.js"></script>
        <script src="/Public/plugins/plugins/plugins.js"></script>
    </head>
        <script>
	      $(function () {
	    	  $('#pageContent').height(WST.pageHeight()-98);
	    	  getTask();
	      });
	      $(window).resize(function() {
	    	  $('#pageContent').height(WST.pageHeight()-98);
	      });
	      function logout(){
	    	  Plugins.confirm({ title:'信息提示',content:'您确定要退出系统吗?',okText:'确定',cancelText:'取消',okFun:function(){
	   		     Plugins.closeWindow();
	   		     Plugins.waitTips({title:'信息提示',content:'正在退出系统...'});
	   		     $.post("<?php echo U('Admin/Index/logout');?>",{},function(data,textStatus){
	   		    	  location.reload();
	   		     });
	          }});
	      }
	      function getTask(){
	    	  $.post("<?php echo U('Admin/Index/getTask');?>",{},function(data,textStatus){
	  			    var json = WST.toJson(data);
                    if(json.status==1){
                        /*商品管理*/
                        if(json.goodsNum>0){
                            $('#goodsTips').html(json.goodsNum).show();
                        }else{
                            $('#goodsTips').hide();
                        }
                        if(json.goodsSum>0){
                            $('#goodssTips').html(json.goodsSum).show();
                        }else{
                            $('#goodssTips').hide();
                        }
                        /*店铺管理*/
                        if(json.sshopsNum>0){
                            $('#sshopsTips').html(json.sshopsNum).show();
                        }else{
                            $('#sshopsTips').hide();
                        }
                        if(json.shopsNum>0){
                            $('#shopsTips').html(json.shopsNum).show();
                        }else{
                            $('#shopsTips').hide();
                        }

                        /*订单管理*/
                        if(json.refundNum>0){
                            $('#refundTips').html(json.refundNum).show();
                        }else{
                            $('#refundTips').hide();
                        }
                        if(json.complainNum>0){
                            $('#complainTips').html(json.complainNum).show();
                        }else{
                            $('#complainTips').hide();
                        }
                        if(json.goodsSunNum>0){
                            $('#goodsSunTips').html(json.goodsSunNum).show();   
                             $('#orderTips').html(json.goodsSunNum).show();                        
                        }else{
                            $('#goodsSunTips').hide();
                        }
                        if(json.ordersNum>0){
                            $('#ordersTips').html(json.ordersNum).show();   
                             $('#ordersTips').html(json.ordersNum).show();                        
                        }else{
                            $('#ordersTips').hide();
                        }
                        /*促销管理*/
                        if(json.cuxiaoNum>0){
                            $('#cuxiaoTips').html(json.cuxiaoNum).show();   
                        }else{
                            $('#cuxiaoTips').hide();
                        }
                        if(json.groupNum>0){
                            $('#groupTips').html(json.groupNum).show();
                        }else{
                            $('#groupTips').hide();
                        }
                        if(json.seckillNum>0){
                            $('#seckillTips').html(json.seckillNum).show();
                        }else{
                            $('#seckillTips').hide();
                        }
                        if(json.auctionNum>0){
                            $('#auctionTips').html(json.auctionNum).show();
                        }else{
                            $('#auctionTips').hide();
                        }
                        //提现管理
                        if(json.topNum>0){
                             $('#topTips').html(json.topNum).show();
                        }else{
                            $('#topTips').hide();
                        }
                        if(json.topListNum>0){
                             $('#topListTips').html(json.topListNum).show();
                        }else{
                            $('#topListTips').hide();
                        }
                        //优惠管理
                        if(json.discountNum>0){
                            $('#discountTips').html(json.discountNum).show();
                        }else{
                            $('#discountTips').hide();
                        }
                        if(json.youhuiNum>0){
                            $('#youhuiTips').html(json.youhuiNum).show();
                        }else{
                            $('#youhuiTips').hide();
                        }
                        if(json.ggkNum>0){
                            $('#ggkTips').html(json.ggkNum).show();
                        }else{
                            $('#ggkTips').hide();
                        }
                        //积分商城
                        if(json.integralNum>0){
                            $('#integralTips').html(json.integralNum).show();
                        }else{
                            $('#integralTips').hide();
                        }
                        if(json.integralOrdersNum>0){
                            $('#integralOrdersTips').html(json.integralOrdersNum).show();
                        }else{
                            $('#integralOrdersTips').hide();
                        }
                        if(json.lotteryOrdersNum>0){
                            $('#lotteryIntegralOrdersTips').html(json.lotteryOrdersNum).show();
                        }else{
                            $('#lotteryIntegralOrdersTips').hide();
                        }
                        //圈子管理
                        if(json.circleNum>0){
                            $('#circleTips').html(json.circleNum).show();
                        }else{
                            $('#circleTips').hide();
                        }
                        if(json.circleListNum>0){
                            $('#circleListTips').html(json.circleListNum).show();
                        }else{
                            $('#circleListTips').hide();
                        }
                        setTimeout("getTask();",10000);
                    }
	    	  });
	      }
	      function cleanCache(){
	    	  Plugins.waitTips({title:'信息提示',content:'正在清除缓存，请稍后...'});
	    	  $.post("<?php echo U('Admin/Index/cleanAllCache');?>",{},function(data,textStatus){
	    		  var json = WST.toJson(data);
	    		  if(json.status==1)Plugins.setWaitTipsMsg({content:'缓存清除成功!',timeout:1000});
	    	  });
	      }
	    </script>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
        <?php if(isset($WST_STAFF['type'])): ?><a href="index.html" class="logo">欢迎光临代理后台</a>
        <?php else: ?>
        	<a href="index.html" class="logo">在线商城后台管理</a><?php endif; ?>
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                    	 <?php if(!isset($WST_STAFF['type'])): ?><li class="dropdown user user-menu">
                            <a href="<?php echo WSTDomain();?>" target='_blank'>
                                <i class="glyphicon glyphicon-home"></i>
                                <span>前台&nbsp;</span>
                            </a>
                        </li>
                        <li class="dropdown user user-menu">
                            <a href="javascript:cleanCache()">
                                <i class="glyphicon glyphicon glyphicon-refresh"></i>
                                <span>清除缓存</span>
                            </a>
                        </li><?php endif; ?>
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <?php if(isset($WST_STAFF['type'])): ?><span><?php echo session('WST_STAFF.loginName');?>&nbsp;<i class="caret"></i></span>
                                <?php else: ?>
                                	<span><?php echo session('WST_STAFF.staffName');?>&nbsp;<i class="caret"></i></span><?php endif; ?>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <?php if(!isset($WST_STAFF['type'])): ?><li class="user-header bg-light-blue">
                                    <img src="/<?php echo session('WST_STAFF.staffPhoto');?>" class="img-circle" alt="<?php echo session('WST_STAFF.roleName');?>" />
                                    <p>
                                        <?php echo session('WST_STAFF.staffName');?> - <?php echo session('WST_STAFF.roleName');?>
                                        <small>职员编号：<?php echo ($WST_STAFF["staffNo"]); ?></small>
                                    </p>
                                </li><?php endif; ?>
                                <!-- Menu Body -->
                                <li class="user-body" style='display:none'>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Followers</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Sales</a>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <a href="#">Friends</a>
                                    </div>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                <?php if(!isset($WST_STAFF['type'])): ?><div class="pull-left">
                                        <a href="<?php echo U('Admin/Staffs/toEditPass');?>" target='pageContent' class="btn btn-default btn-flat">修改密码</a>
                                    </div><?php endif; ?>
                                    <div class="pull-right">
                                        <a href="javascript:logout()" class="btn btn-default btn-flat">退出系统</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <?php if(!isset($WST_STAFF['type'])): ?><div class="user-panel">
                    
                        <div class="pull-left image">
                            <img src="/<?php echo session('WST_STAFF.staffPhoto');?>" class="img-circle" alt="<?php echo session('WST_STAFF.staffName');?>" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo session('WST_STAFF.staffName');?></p>
                        </div>
                        
                    </div><?php endif; ?>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <?php if(isset($WST_STAFF['type'])): ?><li><a href="<?php echo U('Admin/Pagent/infolist');?>" target='pageContent'>区域代理</a></li><?php endif; ?>
                        <?php if(in_array('spgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>商品管理</span>
                                <small id='goodsTips' style='display:none' class="badge pull-right bg-green">0</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('splb_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Goods/index');?>" target='pageContent'>商品列表</a></li>
					            <?php } ?>
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Goods/queryPenddingByPage');?>" target='pageContent'>商品审核<small id='goodssTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
					            <?php } ?>
                                <?php if(in_array('sppl_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/GoodsAppraises/index');?>" target='pageContent'>商品评价</a></li>
					            <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(in_array('ddgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>订单管理</span>
                                <small id='ordersTips' style='display:none' class="badge pull-right bg-green">0</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('ddlb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/index');?>" target='pageContent' >商家订单列表</a></li>
                                <?php } ?>
                                <?php if(isset($abc) && in_array('ddlb_09',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/PtOrders');?>" target='pageContent' >商家平台订单列表</a></li>
                                <?php } ?>
                                <?php if(in_array('tk_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/queryRefundByPage');?>" target='pageContent' >商家退款列表<small id='refundTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                                
                                
                                <?php if(in_array('ddlb_user_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/user_list');?>" target='pageContent' >用户发布订单列表</a></li>
                                <?php } ?>
                                <?php if(in_array('tk_user_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/tk_user');?>" target='pageContent' >用户发布退款列表<small id='refundTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                                
                                <?php if(in_array('ts_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Complain/index');?>" target='pageContent'>订单投诉<small id='complainTips' style='display:none' class="badge pull-right bg-green">0</small>
                                </a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
<!--
                        <?php if(in_array('dpgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>一元云购</span>
                            </a>
                            <ul class="treeview-menu">
                            	<li><a href="<?php echo U('Admin/yunorder/goods');?>" target='pageContent' >商品管理</a></li>
                                <li><a href="<?php echo U('Admin/yunorder/index');?>" target='pageContent' >订单列表</a></li>
                                <li><a href="<?php echo U('Admin/yunorder/zzj');?>" target='pageContent' >中奖订单</a></li>
								<li><a href="<?php echo U('Admin/yunorder/ordermoney');?>" target='pageContent' >订单金额</a></li>
                            </ul>
                        </li>
-->
                        <li class="treeview">
                            <a href="#">
                                <span>提现管理</span>
                                <small id='topTips' style='display:none' class="badge pull-right bg-green">3</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('dplb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Top/applyIndex');?>" target='pageContent' >提现列表<small id='topListTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(in_array('dlgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>代理管理</span>
                                
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('spfl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Pagent/index');?>" target='pageContent'>区域代理</a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Pagent/gitmoney');?>" target='pageContent' >代理提现</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>


                        <?php if(in_array('dpgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>店铺管理</span>
                                <small id='shopsTips' style='display:none' class="badge pull-right bg-green">3</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('spfl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/GoodsCats/index');?>" target='pageContent'>店铺分类</a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Shops/toEdit');?>" target='pageContent' >添加店铺</a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Shops/index');?>" target='pageContent' >店铺列表</a></li>
                                <?php } ?>
                                <?php if(in_array('dpsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Shops/queryPeddingByPage');?>" target='pageContent' >店铺审核<small id='sshopsTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(in_array('hygl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>会员管理</span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('hydj_00',$WST_STAFF['grant'])){ ?>
                                <li style="display: none;"><a href="<?php echo U('Admin/UserRanks/index');?>" target='pageContent' >会员等级</a></li>
                                <?php } ?>
                                <?php if(in_array('hylb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Users/index');?>" target='pageContent' >会员列表</a></li>
                                <?php } ?>
                                <?php if(in_array('hylb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Users/toEdit/');?>" target='pageContent'>添加会员</a></li>
                                <?php } ?>
                                <?php if(in_array('hyzh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Users/queryAccountByPage');?>" target='pageContent' >账号管理</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(isset($abc) && in_array('dpgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>积分商城管理</span>
                                <small id='integralTips' style='display:none' class="badge pull-right bg-green">3</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('dplb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralGoods/cats');?>" target='pageContent' >商品分类</a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralGoods/toEdit');?>" target='pageContent' >添加商品</a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralGoods/index');?>" target='pageContent' >商品列表</a></li>
                                <?php } ?>
                                <?php if(in_array('dpsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralOrders/index');?>" target='pageContent' >兑换订单<small id='integralOrdersTips' style='display:none' class="badge pull-right bg-green">3</small></a></li>
                                <?php } ?>
                                <?php if(in_array('dpsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralOrders/lotteryIndex');?>" target='pageContent' >抽奖订单<small id='lotteryIntegralOrdersTips' style='display:none' class="badge pull-right bg-green">3</small></a></li>
                                <?php } ?>
                                <?php if(in_array('dplb_01',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/IntegralGoods/toLottery');?>" target='pageContent' >抽奖说明</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(isset($abc) && in_array('dpgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>促销活动管理</span>
                                <small id='cuxiaoTips' style='display:none' class="badge pull-right bg-green">0</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Group/index',array('group'=>1));?>" target='pageContent'>团购列表</a></li>
                                <?php } ?>
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Group/queryPenddingByPage');?>" target='pageContent'>团购审核<small id='groupTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/index',array('isGroup'=>1));?>" target='pageContent'>团购订单</a></li>
                                <?php } ?>

                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Seckill/index',array('group'=>1));?>" target='pageContent'>秒杀列表</a></li>
                                <?php } ?>
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Seckill/queryPenddingByPage');?>" target='pageContent'>秒杀审核<small id='seckillTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                                <?php if(in_array('spsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Orders/index',array('isSeckill'=>1));?>" target='pageContent'>秒杀订单</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

<?php if(in_array('tjbb_00',$WST_STAFF['grant'])){ ?> 


                        <li class="treeview">
                            <a href="pages/mailbox.html">
                                <span> 统计报表</span>
                            </a>
                            <ul class="treeview-menu">
                               <?php if(in_array('tjkf_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjkf');?>" target='pageContent' >客户统计</a></li>
                               <?php } ?>

                               <?php if(in_array('tjgk_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjgk');?>" target='pageContent' >销售概况</a></li>
                               <?php } ?>
                                <?php if(in_array('tjmx_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjmx');?>" target='pageContent' >销售明细</a></li>
                               <?php } ?>
                              <?php if(in_array('tjwater_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/water');?>" target='pageContent' >平台流水</a></li>
                               <?php } ?>
<!--
                               <?php if(in_array('tjdd_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjdd');?>" target='pageContent' >订单统计</a></li>
                               <?php } ?>
                                 <?php if(in_array('tjph_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjph');?>" target='pageContent' >销售排行</a></li>
                               <?php } ?>
                                <?php if(in_array('tjhy_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Stats/tjhy');?>" target='pageContent' >会员排行</a></li>
                               <?php } ?> -->
                            </ul>
                        </li>
                        <?php } ?>
                         
                        <?php if(in_array('dpgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>优惠活动管理</span>
                                <small id='discountTips' style='display:none' class="badge pull-right bg-green">3</small>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('cmgl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Youhui/index');?>" target='pageContent' >优惠券列表</a></li>
                                <?php } ?>
                                <?php if(in_array('cmgl_03',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Youhui/shopup_log');?>" target='pageContent' >优惠券审核列表<small id='youhuiTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                                <?php if(in_array('cmgl_08',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Youhui/userlist');?>" target='pageContent' >优惠券已领取列表</a></li>
                                <?php } ?>
                                <?php if(in_array('ggkz_01',$WST_STAFF['grant'])){ ?>
                                <li style="display: none;"><a href="<?php echo U('Admin/Ggk/indexlist');?>" target='pageContent' >刮刮卡活动列表</a></li>
                                <?php } ?>
                                <?php if(in_array('ggks_05',$WST_STAFF['grant'])){ ?>
                                <li style="display: none;"><a href="<?php echo U('Admin/Ggk/checklist');?>" target='pageContent' >刮刮卡活动审核列表<small id='ggkTips' style='display:none' class="badge pull-right bg-green">0</small></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(isset($abc) && in_array('wbgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>微帮管理</span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('wblx_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Help/helpCate');?>" target='pageContent' >微帮类型</a></li>
                                <?php } ?>
                                <?php if(in_array('wblb_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Help/helpList');?>" target='pageContent' >微帮列表</a></li>
                                <?php } ?>
                                <?php if(in_array('wbts_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Help/suggest');?>" target='pageContent' >微帮投诉</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(isset($abc) && in_array('dqgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="pages/calendar.html">
                                <span>地区管理</span>
                            </a>
                            <ul class="treeview-menu">
                               <?php if(in_array('dqlb_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Areas/index');?>" target='pageContent' >地区管理</a></li>
                               <?php } ?>
                               <?php if(in_array('sqlb_00',$WST_STAFF['grant'])){ ?>
					           <li><a href="<?php echo U('Admin/Communitys/index');?>" target='pageContent' >社区列表</a></li>
					           <?php } ?>
                               <?php if(in_array('sqlb_01',$WST_STAFF['grant'])){ ?>
					           <li><a href="<?php echo U('Admin/Communitys/toEdit');?>" target='pageContent' >添加社区</a></li>
					           <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(in_array('wzgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="pages/mailbox.html">
                                <span> 文章管理</span>
                            </a>
                            <ul class="treeview-menu">
                               <?php if(in_array('wzlb_00',$WST_STAFF['grant'])){ ?>
                               <li><a href="<?php echo U('Admin/Articles/index');?>" target='pageContent' >文章列表</a></li>
                               <?php } ?>
                               <?php if(in_array('wzfl_00',$WST_STAFF['grant'])){ ?>
					           <li><a href="<?php echo U('Admin/ArticleCats/index');?>" target='pageContent' >文章分类</a></li>
					           <?php } ?>
                               <?php if(in_array('wzlb_01',$WST_STAFF['grant'])){ ?>
					           <li><a href="<?php echo U('Admin/Articles/toEdit');?>" target='pageContent' >添加文章</a></li>
					           <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if(in_array('xtgl_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>系统管理</span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('jsgl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Roles/index');?>" target='pageContent' >角色管理</a></li>
                                <?php } ?>
                                <?php if(in_array('zylb_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Staffs/index');?>" target='pageContent' >职员管理</a></li>
					            <?php } ?>
                                <?php if(in_array('dlrz_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/LogLogins/index');?>" target='pageContent' >登录日志</a></li>
					            <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(in_array('scsz_00',$WST_STAFF['grant'])){ ?>
                        <li class="treeview">
                            <a href="#">
                                <span>商城设置</span>
                            </a>
                            <ul class="treeview-menu">
                                <?php if(in_array('scxx_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Index/toMallConfig');?>" target='pageContent' >商城信息</a></li>
                                <?php } ?>
                                <?php if(in_array('dhgl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Navs/index');?>" target='pageContent' >导航管理</a></li>
                                <?php } ?>
                                <?php if(in_array('yqlj_00',$WST_STAFF['grant'])){ ?>
					            <li style="display: none;"><a href="<?php echo U('Admin/Friendlinks/index');?>" target='pageContent'>友情链接</a></li>
					            <?php } ?>
                                <?php if(in_array('gggl_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Ads/index');?>" target='pageContent'>广告管理</a></li>
					            <?php } ?>
                                <?php if(in_array('gggl_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Suggest/index');?>" target='pageContent'>用户建议</a></li>
                                <?php } ?>
                                <?php if(in_array('yhgl_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Banks/index');?>" target='pageContent'>银行管理</a></li>
					            <?php } ?>
					            <?php if(in_array('zfgl_00',$WST_STAFF['grant'])){ ?>
					            <li><a href="<?php echo U('Admin/Payments/index');?>" target='pageContent'>支付管理</a></li>
					            <?php } ?>
                                <?php if(in_array('dpsh_00',$WST_STAFF['grant'])){ ?>
                                <li><a href="<?php echo U('Admin/Express/index');?>" target='pageContent' >物流管理</a></li>
                                <?php } ?>
					            
                            </ul>
                        </li>
                        <?php } ?>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <?php if(!isset($WST_STAFF['type'])): ?><section class="content-header">
                    <h1>
                        <small>后台管理中心</small>
                    </h1>
                </section><?php endif; ?>
                <!-- Main content -->
                <section class="content" style='margin:0px;padding:0px;'>
                <?php if(isset($WST_STAFF['type'])): ?><iframe id='pageContent' name='pageContent' src="<?php echo U('Pagent/infolist');?>" width='100%' height='100%' frameborder="0"></iframe>
                <?php else: ?>
                	<iframe id='pageContent' name='pageContent' src="<?php echo U('Admin/Index/toMain');?>" width='100%' height='100%' frameborder="0"></iframe><?php endif; ?>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
    </body>
</html>